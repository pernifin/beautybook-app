package com.beautybook.android;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.actionsheet.ActionSheetPackage;
import io.invertase.firebase.crashlytics.ReactNativeFirebaseCrashlyticsPackage;
import org.umhan35.RNSearchBarPackage;
import io.invertase.firebase.app.ReactNativeFirebaseAppPackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;
import com.apptentive.android.sdk.reactlibrary.RNApptentivePackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.react.rnspinkit.RNSpinkitPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.swmansion.reanimated.ReanimatedPackage;
import com.lugg.ReactNativeConfig.ReactNativeConfigPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.delightfulstudio.wheelpicker.WheelPickerPackage;
import com.reactcommunity.rnlocalize.RNLocalizePackage;
import com.reactcommunity.rndatetimepicker.RNDateTimePickerPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.swmansion.rnscreens.RNScreensPackage;
import com.horcrux.svg.SvgPackage;
import org.reactnative.camera.RNCameraPackage;
import com.reactnativecommunity.rnpermissions.RNPermissionsPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {
  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new ActionSheetPackage(),
            new ReactNativeFirebaseCrashlyticsPackage(),
            new RNSearchBarPackage(),
            new ReactNativeFirebaseAppPackage(),
            new SplashScreenReactPackage(),
            new ReactNativePushNotificationPackage(),
            new RNApptentivePackage(),
            new PickerPackage(),
          new RNSpinkitPackage(),
          new LinearGradientPackage(),
          new VectorIconsPackage(),
          new WheelPickerPackage(),
          new RNLocalizePackage(),
          new RNDateTimePickerPackage(),
          new ReactNativeConfigPackage(),
          new AsyncStoragePackage(),
          new RNScreensPackage(),
          new SvgPackage(),
          new RNCameraPackage(),
          new RNPermissionsPackage(),
          new RNGestureHandlerPackage(),
          new ReanimatedPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
