import { without, sortBy } from 'lodash';
import {
  LOAD_CLIENT_DATA,
  FINISH_APPOINTMENT,
  SET_PAST_APPOINTMENTS,
  SAVE_CLIENT_APPOINTMENT,
  CANCEL_CLIENT_APPOINTMENT
} from '../actions';

const initialState = {
  upcoming: [],
  past: []
};

export default function appointmentsReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_CLIENT_DATA:
      return {
        ...state,
        upcoming: action.appointments
      };

    case FINISH_APPOINTMENT:
      return {
        past: state.past,
        upcoming: sortBy(state.upcoming.concat(action.appointment), a => new Date(a.datetime))
      };

    case SET_PAST_APPOINTMENTS:
      return {
        ...state,
        past: action.appointments
      };

    case SAVE_CLIENT_APPOINTMENT:
      const index = state.upcoming.findIndex(a => a.id === action.appointment.id);

      return {
        ...state,
        upcoming: [...state.upcoming.slice(0, index), action.appointment, ...state.upcoming.slice(index + 1)]
      };

    case CANCEL_CLIENT_APPOINTMENT:
      return {
        ...state,
        upcoming: without(state.upcoming, action.appointment)
      };

    default:
      return state;
  }
}