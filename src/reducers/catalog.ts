import { SET_CATALOG_CATEGORIES, SET_CATALOG_SERVICES } from '../actions';

const initialState = {
  categories: [],
  services: {}
};

export default function catalogReducer(state = initialState, action) {
  switch (action.type) {
    case SET_CATALOG_CATEGORIES:
      return {
        ...state,
        categories: action.categories
      };

    case SET_CATALOG_SERVICES:
      return {
        ...state,
        services: {
          ...state.services,
          [action.category]: action.services
        }
      };

    default:
      return state;
  }
};