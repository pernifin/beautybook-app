import { combineReducers } from 'redux'
import system from './system';
import profile from './profile';
import calendar from './calendar';
import schedule from './schedule';
import masterCode from './masterCode';
import services from './services';
import catalog from './catalog';
import masters from './masters';
import appointmentWizard from './appointmentWizard';
import appointments from './appointments';
import { SET_ERROR, RESET_ERROR, SET_WARNING, RESET_WARNING, QUERYING } from '../actions';

import t from '../Lang';

function errorMessage(state = null, { type, error }) {
  if (type === SET_ERROR) {
    return t(error);
  } else if (type === RESET_ERROR) {
    return null;
  } else {
    return state;
  }
}

function warningMessage(state = null, { type, warning }) {
  if (type === SET_WARNING) {
    return t(warning);
  } else if (type === RESET_WARNING) {
    return null;
  } else {
    return state;
  }
}

function querying(state = false, { type, active }) {
  if (type === QUERYING) {
    return active;
  } else {
    return state;
  }
}

export default combineReducers({
  errorMessage,
  warningMessage,
  querying,
  
  system,
  profile,

  calendar,
  schedule,
  masterCode,
  services,
  catalog,

  masters,
  appointmentWizard,
  appointments
});
