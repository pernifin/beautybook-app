import { SET_MASTER_CODE } from '../actions';

export default function masterCodeReducer(state = null, action) {
  switch (action.type) {
    case SET_MASTER_CODE:
      return action.code;

    default:
      return state;
  }
};
