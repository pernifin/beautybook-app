import { range, sum, pickBy, mapValues } from 'lodash';
import {
  START_APPOINTMENT_WIZARD,
  SET_MASTER_SERVICES,
  SELECT_APPOINTMENT_SERVICES,
  SET_MASTER_TIMESLOTS,
  SELECT_TIME_RANGE,
  SELECT_TIME_SLOT
} from '../actions';

const initialState = {
  masterId: null,
  services: [],
  selectedServices: [],
  duration: 0,
  price: 0,
  allTimeSlots: {},
  timeSlots: {},
  ranges: [],
  selectedRange: 0,
  selectedTime: null
};

function getRanges(timeRange: { timeFrom: number, timeTo: number }, numRanges: number) {
  const step = Math.floor((timeRange.timeTo - timeRange.timeFrom) / 180) * 60;

  return range(numRanges).map(i => ({
    timeFrom: timeRange.timeFrom + step * i,
    timeTo: i === 2 ? timeRange.timeTo : timeRange.timeFrom + step * (i + 1)
  }));
}

function filterSlots(timeSlots, { timeFrom, timeTo }) {
  return pickBy(
    mapValues(timeSlots, daySlots => daySlots.filter(time => time >= timeFrom && time < timeTo)),
    daySlots => daySlots.length
  );
}

export default function appointmentWizardReducer(state = initialState, action) {
  switch (action.type) {
    case START_APPOINTMENT_WIZARD:
      return {
        ...initialState,
        masterId: action.masterId
      };

    case SET_MASTER_SERVICES:
      return {
        ...state,
        services: action.services
      };

    case SELECT_APPOINTMENT_SERVICES:
      return {
        ...state,
        selectedServices: action.services,
        duration: sum(action.services.map(s => s.duration)),
        price: sum(action.services.map(s => s.price))
      };

    case SET_MASTER_TIMESLOTS:
      const ranges = getRanges(action.timeRange, 3);

      return {
        ...state,
        allTimeSlots: action.availability,
        timeSlots: filterSlots(action.availability, ranges[0]),
        ranges,
        selectedRange: 0,
        selectedTime: null
      };

    case SELECT_TIME_RANGE:
      return {
        ...state,
        timeSlots: filterSlots(state.allTimeSlots, state.ranges[action.range]),
        selectedRange: action.range
      };

    case SELECT_TIME_SLOT:
      return {
        ...state,
        selectedTime: action.timeSlot
      };

    default:
      return state;
  }
}
