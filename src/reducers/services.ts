import { LOAD_MASTER_DATA, SAVE_SERVICE, DELETE_SERVICE } from '../actions';

export default function servicesReducer(state = [], action) {
  switch (action.type) {
    case LOAD_MASTER_DATA:
      return [
        ...action.services
      ];

    case SAVE_SERVICE:
      return state.filter(s => s.id !== action.service.id).concat(action.service);

    case DELETE_SERVICE:
      return state.filter(s => s.id !== action.serviceId);

    default:
      return state;
  }
};