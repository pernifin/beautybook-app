import { LOAD_CLIENT_DATA, ADD_MASTER } from '../actions';

export default function mastersReducer(state = [], action) {
  switch (action.type) {
    case LOAD_CLIENT_DATA:
      return [...action.masters];

    case ADD_MASTER:
      return [
        action.master,
        ...state
      ];

    default:
      return state;
  }
}
