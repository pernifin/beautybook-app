import { SET_TOKEN, SET_USER_STATE, SIGNIN_USER, SET_INITIAL_SCREEN, USER_LOGOUT } from '../actions';

const initialState = {
  token: null,
  loggedIn: undefined,
  userId: null,
  mode: null,
  cid: null,
  initScreen: null
};

export default function systemReducer(state = initialState, action) {
  switch (action.type) {
    case SET_TOKEN:
      return {
        ...state,
        token: action.token
      };

    case SET_USER_STATE:
      return {
        ...state,
        loggedIn: action.loggedIn,
        userId: action.userId,
        mode: action.mode
      };

    case SIGNIN_USER:
      return {
        ...state,
        cid: action.cid
      };

    case SET_INITIAL_SCREEN:
      return {
        ...state,
        initScreen: action.screen && {
          routeName: action.screen,
          params: action.params
        }
      };

    case USER_LOGOUT:
      return { ...initialState };

    default:
      return state
  }
};