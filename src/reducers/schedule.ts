import moment from 'moment';
import { range, forEach } from 'lodash';

import { LOAD_MASTER_DATA, SET_DATES_SCHEDULE, SET_WEEK_SCHEDULE, SET_WORK_SETTINGS } from '../actions';

const initialState = {
  startDate: null,
  week: {},
  days: {},
  overlaps: [],
  settings: {}
};


export default function scheduleReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_MASTER_DATA:
      return {
        ...initialState,
        ...action.schedule
      };

    case SET_DATES_SCHEDULE:
    case SET_WEEK_SCHEDULE:
      const days = { ...state.days, ...action.days };
      const week = action.week || state.week;

      return {
        ...state,
        week,
        days,
        overlaps: action.overlapDates || []
      };

    case SET_WORK_SETTINGS:
      return {
        ...state,
        settings: action.settings
      };

    default:
      return state;
  }
};