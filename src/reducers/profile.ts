import { LOAD_MASTER_DATA, LOAD_CLIENT_DATA, SAVE_PROFILE, SAVE_PHONE, VERIFY_PHONE } from '../actions';

const initialState = {
  name: '',
  phone: '',
  cid: null
};

export default function profileReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_MASTER_DATA:
    case LOAD_CLIENT_DATA:
    case SAVE_PROFILE:
      return {
        ...state,
        ...action.profile
      };

    case SAVE_PHONE:
      return {
        ...state,
        cid: action.cid
      };

    case VERIFY_PHONE:
      return {
        ...state,
        phone: action.phone,
        cid: null
      };

    default:
      return state;
  }
};