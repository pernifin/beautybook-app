import moment from 'moment';
import { mapValues, omit, sortBy, range, sum, zipObject, groupBy, pickBy, fromPairs, Dictionary } from 'lodash';

import { ITimeBlock, ICalendar } from '@/common/DataTypes';

import {
  QUERYING,
  LOAD_MASTER_DATA,
  SET_CALENDAR_DATES,
  SAVE_APPOINTMENT,
  SAVE_DATE_BREAKS,
  REMOVE_CALENDAR_RECORD,
  SET_DATES_SCHEDULE,
  SET_WEEK_SCHEDULE
} from '../actions';

function buildSlots(records) {
  return sortBy(records, record => record.timeFrom).reduce((slots, record) => {
    const lastSlot = slots[slots.length - 1];

    if (lastSlot && record.timeFrom < lastSlot.timeTo) {
      lastSlot.records.push(record);
      lastSlot.timeTo = Math.max(lastSlot.timeTo, record.timeTo);
      lastSlot.workload += record.appointment ? record.timeTo - lastSlot.timeTo : 0;
    } else {
      slots.push({
        timeFrom: record.timeFrom,
        timeTo: record.timeTo,
        workload: record.appointment ? record.timeTo - record.timeFrom : 0,
        records: [record]
      });
    }

    return slots;
  }, []);
}

const buildDateState = (records) => {
  const slots = buildSlots(records);
  const workload = sum(slots.map(s => s.workload));

  return {
    loading: false,
    slots,
    workload
  };
};

const buildCalendarDates = (records) => {
  return mapValues(groupBy(records, r => r.date), buildDateState);
}

const recordsById = (records: ITimeBlock[]) => {
  return fromPairs(records.map(r => [r.id, r]));
}

const initialDateState = () => ({
  loading: true,
  slots: [],
  workload: 0
});

const getInitialState = () => {
  const from = moment().startOf('week');
  const initialDates = range(7).map(d => from.clone().add(d, 'd').format('YYYY-MM-DD'));

  return {
    records: {} as Dictionary<ITimeBlock>,
    dates: zipObject(initialDates, range(7).map(initialDateState)) as ICalendar
  };
}

export default function calendarReducer(state = getInitialState(), action) {
  let records = null;

  switch (action.type) {
    case LOAD_MASTER_DATA:
    case SET_CALENDAR_DATES:
      return {
        records: {
          ...state.records,
          ...recordsById(action.calendar)
        },
        dates: {
          ...mapValues(state.dates, date => ({ ...date, loading: false })),
          ...buildCalendarDates(action.calendar)
        }
      };

    case SAVE_APPOINTMENT:
      records = {
        ...state.records,
        [action.record.id]: action.record
      };

      return {
        records,
        dates: {
          ...state.dates,
          ...buildCalendarDates(Object.values(records))
        }
      };

    case SAVE_DATE_BREAKS:
      records = {
        ...pickBy(state.records, r => r.id !== action.oldBlockId),
        ...recordsById(action.blocks)
      };

      return {
        records,
        dates: {
          ...state.dates,
          ...buildCalendarDates(Object.values(records))
        }
      };

    case REMOVE_CALENDAR_RECORD:
      records = pickBy(state.records, r => r.id !== action.record.id);

      const dateState = buildDateState(Object.values(pickBy(records, r => r.date === action.record.date)));

      return {
        records,
        dates: {
          ...state.dates,
          [action.record.date]: dateState
        }
      };

    case SET_DATES_SCHEDULE:
      const dates = Object.keys(action.days);

      return {
        records: pickBy(state.records, r => !dates.includes(r.date)),
        dates: pickBy(state.dates, (_, date) => !dates.includes(date))
      };

    case SET_WEEK_SCHEDULE:
      return {
        records: {},
        dates: {}
      };

    case QUERYING:
      if (action.active && action.originalType === SET_CALENDAR_DATES) {
        const { from, to } = action.payload;
        const diff = moment(to).diff(from, 'days') + 1;
        const dates = range(diff).map(d => moment(from).add(d, 'days').format('YYYY-MM-DD'));
        const initialState = range(diff).map(initialDateState);

        return {
          records: state.records,
          dates: {
            ...state.dates,
            ...zipObject(dates, initialState)
          }
        };
      }

    default:
      return state;
  }
};