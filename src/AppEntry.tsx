import React from 'react';
import { YellowBox, Platform, UIManager } from 'react-native';
import Config from 'react-native-config';
import { Apptentive, ApptentiveConfiguration } from 'apptentive-react-native';
import crashlytics from '@react-native-firebase/crashlytics';
import { Provider } from 'react-redux';
import moment from 'moment';

import connectReactotron from './ReactotronConfig';
import configureStore from './store';
import { configure as configureNotifications } from './notifications';

import AppRoot from './components/AppRoot';

// import Playgroud from './Playground';

moment.updateLocale('en', {
  week: { dow: 1, doy: 4 }
});

if (Platform.OS === 'android') {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

YellowBox.ignoreWarnings([
  'Warning: componentWillMount has been renamed',
  'Warning: componentWillReceiveProps has been renamed',
  '[RCTRootView cancelTouches]',
  'Invalid props.itemStyle key'
]);

Apptentive.register(new ApptentiveConfiguration(
  Platform.OS === 'ios' ? Config.APPTENTIVE_IOS_KEY : Config.APPTENTIVE_ANDROID_KEY,
  Platform.OS === 'ios' ? Config.APPTENTIVE_IOS_SIGNATURE : Config.APPTENTIVE_ANDROID_SIGNATURE
)).catch(error => {
  console.log(error);
});

//if (__DEV__) {
  connectReactotron();
//}

const store = configureStore();

configureNotifications(store); // Need to handle exceptions somehow

if (!__DEV__) {
  ErrorUtils.setGlobalHandler((error, isFatal) => {
    crashlytics().setUserId(store.getState()?.system?.userId);
    crashlytics().recordError(error);
  });
}

export default function App() {
  return (
    <Provider store={store}>
      <AppRoot />
    </Provider>
    //<Playgroud />
  );
}
