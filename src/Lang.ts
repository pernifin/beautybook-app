import { findBestAvailableLanguage, addEventListener } from 'react-native-localize';
import i18n from 'i18n-js';
import AsyncStorage from '@react-native-community/async-storage';
import { memoize, range, capitalize } from 'lodash';
import moment from 'moment';
import 'moment/min/locales';
import { LocaleConfig } from 'react-native-calendars';

['en', 'ru', 'uk'].forEach(locale => {
  const config = moment.updateLocale(locale, {});
  LocaleConfig.locales[locale] = {
    monthNames: config.months().map(capitalize),
    monthNamesShort: config.monthsShort().map(capitalize),
    dayNames: config.weekdays(),
    dayNamesShort: config.weekdaysShort()
  };
});

class Lang {
  private active;
  private listeners: Function[] = [];

  private translations = {
    en: require('./translations/en.json'),
    ru: require('./translations/ru.json'),
    uk: require('./translations/uk.json')
  }

  private pluralizers = {
    ru: (count) => {
      switch (true) {
        case count % 10 === 1 && count % 100 !== 11:
          return ['one'];
        case [2, 3, 4].includes(count % 10) && ![12, 13, 14].includes(count % 100):
          return ['few'];
        case count % 10 === 0 || range(5, 10).includes(count % 10) || range(11, 15).includes(count % 100):
          return ['many'];
        default:
          return ['other'];
      }
    },
    uk: (count) => {
      switch (true) {
        case count % 10 === 1 && count % 100 !== 11:
          return ['one'];
        case [2, 3, 4].includes(count % 10) && ![12, 13, 14].includes(count % 100):
          return ['few'];
        case count % 10 === 0 || range(5, 10).includes(count % 10) || range(11, 15).includes(count % 100):
          return ['many'];
        default:
          return ['other'];
      }
    }
  }

  constructor() {
    addEventListener('change', this.update);
    i18n.translations = this.translations;
    Object.assign(i18n.pluralization, this.pluralizers);

    AsyncStorage.getItem('lang')
      .then(lang => this.update(lang, true));
  }

  get activeLang() {
    return this.active;
  }

  get availableLangs() {
    return Object.keys(this.translations);
  }

  update(lang?: string, silent?: boolean) {
    const { languageTag } = lang
      ? { languageTag: lang }
      : findBestAvailableLanguage(Object.keys(this.translations)) || { languageTag: 'en' };

    this.i18nT.cache.clear();
    i18n.locale = languageTag;
    moment.locale(languageTag);
    LocaleConfig.defaultLocale = languageTag;
    this.active = languageTag;

    // Remember forced lang
    if (lang) {
      AsyncStorage.setItem('lang', lang);
    }

    if (!silent) {
      this.listeners.forEach(listener => listener());
    }
  }

  addEventListener(listener: Function) {
    this.listeners.push(listener);
  }

  removeEventListener(listener: Function) {
    this.listeners = this.listeners.filter(l => l !== listener);
  }

  translate = (text: string | any, config = {}) => {
    const { key, ...options } = typeof text === 'object' ? text : { key: text, ...config };
    
    return key.charAt?.(0) === '#' ? this.i18nT(key.substr(1), options) : key;
  }

  i18nT = memoize(
    (key: string, config?: any) => i18n.t(key, config) as string,
    (key, config) => (config ? key + JSON.stringify(config) : key)
  );
}

const lang = new Lang();

export { lang as Lang };
export default lang.translate;