import AsyncStorage from '@react-native-community/async-storage';

import { Lang } from 'src/Lang'

export const SET_TOKEN = 'SET_TOKEN';

export const SET_USER_STATE = 'SET_USER_STATE';
export function setUserState(loggedIn, userId, mode) {
  return {
    type: SET_USER_STATE,
    loggedIn,
    userId,
    mode
  };
}

export function fetchUserInfo() {
  return async function (dispatch) {
    const userinfo = await AsyncStorage.getItem('userinfo');

    if (userinfo) {
      const { token, userId, mode } = JSON.parse(userinfo);
      dispatch({
        type: SET_TOKEN,
        token
      });

      dispatch(setUserState(true, userId, mode));
    } else {
      dispatch(setUserState(false, null, null));
    }
  }
}

export function skipAuth() {
  return async function (dispatch, getState) {
    const response = await dispatch({
      type: SET_USER_STATE,
      api: {
        endpoint: 'auth/skip',
        method: 'post',
        transform: ({ id, mode }) => ({
          loggedIn: true,
          userId: id,
          mode
        })
      }
    });

    if (response.success) {
      const userInfo = { token: getState().system.token, mode: response.mode };
      await AsyncStorage.setItem('userinfo', JSON.stringify(userInfo));
    }

    return response;
  }
}

export const SIGNIN_USER = 'SIGNIN_USER';
export function signUserIn(phone) {
  return {
    type: SIGNIN_USER,
    api: {
      endpoint: 'auth/signin',
      method: 'post',
      payload: { phone }
    }
  }
}

export function repeatUserCall(phone) {
  return async function (dispatch, getState) {
    return dispatch({
      type: SIGNIN_USER,
      api: {
        endpoint: 'auth/repeat',
        method: 'post',
        payload: {
          phone,
          cid: getState().system.cid
        }
      }
    });
  }
}

export function verifyUserPhone(phone, code) {
  return async function (dispatch, getState) {
    const response = await dispatch({
      type: SET_USER_STATE,
      api: {
        endpoint: 'auth/verify',
        method: 'post',
        silent: true,
        payload: {
          phone,
          code,
          cid: getState().system.cid,
          lang: Lang.activeLang
        },
        transform: ({ id, mode }) => ({
          loggedIn: true,
          userId: id,
          mode
        })
      }
    });

    if (response.success && response.mode) {
      const userInfo = {
        token: getState().system.token,
        userId: response.id,
        mode: response.mode
      };
      
      await AsyncStorage.setItem('userinfo', JSON.stringify(userInfo));
    }

    return response;
  }
}

export function createUser(name, mode) {
  return async function (dispatch, getState) {
    const response = await dispatch({
      type: SET_USER_STATE,
      api: {
        endpoint: 'profile/create',
        method: 'post',
        payload: { name, mode },
        transform: ({ id }) => ({
          loggedIn: true,
          userId: id,
          mode
        })
      }
    });

    if (response.success) {
      const { token, userId } = getState().system;
      const userInfo = { token, userId, mode };

      await AsyncStorage.setItem('userinfo', JSON.stringify(userInfo));
    }

    return response;
  }
}

export function switchMode() {
  return async function (dispatch, getState) {
    const response = await dispatch({
      type: SET_USER_STATE,
      api: {
        endpoint: 'profile/switch',
        method: 'post',
        transform: ({ mode }) => ({
          loggedIn: true,
          mode
        })
      }
    });

    if (response.success) {
      const { token, mode } = getState().system;
      await AsyncStorage.setItem('userinfo', JSON.stringify({ token, mode }));
    }

    return response;
  }
}

export const USER_LOGOUT = 'USER_LOGOUT';
export function logoutUser() {
  AsyncStorage.removeItem('userinfo');

  return {
    type: USER_LOGOUT
  };
}
