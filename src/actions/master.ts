import moment from 'moment';
import { range } from 'lodash';

export const SET_CALENDAR_DATES = 'SET_CALENDAR_DATES';
export function loadCalendarDate(selectedDate: string, wholeMonth?: boolean) {
  return async function (dispatch, getState) {
    const calendar = getState().calendar.dates;

    const from = moment(selectedDate).startOf(wholeMonth ? 'month' : 'week').format('YYYY-MM-DD');
    const to = moment(selectedDate).endOf(wholeMonth ? 'month' : 'week').format('YYYY-MM-DD');

    if (wholeMonth) {
      if (calendar[from] && calendar[to]) {
        return;
      }
    } else if (calendar[selectedDate]) {
      return;
    }

    return dispatch({
      type: SET_CALENDAR_DATES,
      api: {
        endpoint: 'master/calendar',
        payload: { from, to }
      }
    });
  }
}

export const SAVE_APPOINTMENT = 'SAVE_APPOINTMENT';
export function saveAppointment(appointment, force) {
  return {
    type: SAVE_APPOINTMENT,
    api: {
      endpoint: 'master/calendar/appointment',
      method: 'post',
      payload: { appointment, force }
    }
  };
}

export const REMOVE_CALENDAR_RECORD = 'REMOVE_CALENDAR_RECORD';
export function removeAppointment(record) {
  return {
    type: REMOVE_CALENDAR_RECORD,
    api: {
      endpoint: 'master/calendar/appointment/' + record.appointment.id,
      method: 'delete',
      transform: () => ({ record })
    }
  };
}

export const SAVE_DATE_BREAKS = 'SAVE_DATE_BREAKS';
export function saveTimeBlock(timeoff, force) {
  return {
    type: SAVE_DATE_BREAKS,
    api: {
      endpoint: 'master/calendar/timeoff',
      method: 'post',
      payload: { timeoff, force },
      transform: ({ blocks }) => ({
        oldBlockId: timeoff.id,
        blocks
      })
    }
  };
}

export function removeTimeBlock(timeBlock) {
  return {
    type: REMOVE_CALENDAR_RECORD,
    api: {
      endpoint: 'master/calendar/timeoff/' + timeBlock.id,
      method: 'delete',
      transform: () => ({ record: timeBlock })
    }
  };
}

export const SET_DATES_SCHEDULE = 'SET_DATES_SCHEDULE';
export function saveWorkdays(workData) {
  return {
    type: SET_DATES_SCHEDULE,
    api: {
      endpoint: 'master/schedule/byday',
      method: 'post',
      payload: workData
    }
  };
}

export const SET_WEEK_SCHEDULE = 'SET_WEEK_SCHEDULE';
export function saveWorkWeek(workweek) {
  return {
    type: SET_WEEK_SCHEDULE,
    api: {
      endpoint: 'master/schedule/byweek',
      method: 'post',
      payload: { workweek }
    }
  };
}

export const SET_WORK_SETTINGS = 'SET_WORK_SETTINGS';
export function saveWorkSettings(settings) {
  return {
    type: SET_WORK_SETTINGS,
    api: {
      endpoint: 'master/schedule/settings',
      method: 'post',
      payload: { settings }
    }
  };
}

export const SET_MASTER_CODE = 'SET_MASTER_CODE';
export function loadMasterCode() {
  return {
    type: SET_MASTER_CODE,
    api: {
      endpoint: 'master/code'
    }
  };
}

export const SET_CATALOG_CATEGORIES = 'SET_CATALOG_CATEGORIES';
export function fetchCatalogCategories() {
  return async function (dispatch, getState) {
    if (!getState().catalog.categories.length) {
      dispatch({
        type: SET_CATALOG_CATEGORIES,
        api: {
          endpoint: 'master/catalog/categories'
        }
      });
    }
  }
}

export const SET_CATALOG_SERVICES = 'SET_CATALOG_SERVICES';
export function fetchCatalogServices(category) {
  return async function (dispatch, getState) {
    const services = getState().catalog.services[category];

    if (!services) {
      dispatch({
        type: SET_CATALOG_SERVICES,
        api: {
          endpoint: 'master/catalog/services',
          payload: { category },
          transform: ({ services }) => ({ services, category })
        }
      });
    }
  }
}

export const SAVE_SERVICE = 'SAVE_SERVICE';
export function saveService(service) {
  return {
    type: SAVE_SERVICE,
    api: {
      endpoint: 'master/services',
      method: 'post',
      payload: { service }
    }
  };
}

export const DELETE_SERVICE = 'DELETE_SERVICE';
export function deleteService(service) {
  return {
    type: DELETE_SERVICE,
    api: {
      endpoint: 'master/services/' + service.id,
      method: 'delete',
      transform: () => ({
        serviceId: service.id
      })
    }
  };
}
