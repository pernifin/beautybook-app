export const SET_ERROR = 'SET_ERROR';
export function setError(message) {
  return {
    type: SET_ERROR,
    error: message
  };
}

export const RESET_ERROR = 'RESET_ERROR';
export function dismissError() {
  return {
    type: RESET_ERROR
  };
}

export const SET_WARNING = 'SET_WARNING';
export function setWarning(message) {
  return {
    type: SET_WARNING,
    warning: message
  };
}

export const RESET_WARNING = 'RESET_WARNING';
export function dismissWarning() {
  return {
    type: RESET_WARNING
  };
}

export const QUERYING = 'QUERYING';

export const LOAD_MASTER_DATA = 'LOAD_MASTER_DATA';
export const LOAD_CLIENT_DATA = 'LOAD_CLIENT_DATA';
export function loadInitialData(mode: string) {
  return {
    type: mode === 'master' ? LOAD_MASTER_DATA : LOAD_CLIENT_DATA,
    api: {
      endpoint: mode + '/start',
      transform: ({ data }) => data
    }
  };
}

export const ATTACH_DEVICE = 'ATTACH_DEVICE';
export function attachDevice(token: string, os: string) {
  return {
    type: ATTACH_DEVICE,
    api: {
      endpoint: '/profile/device',
      method: 'post',
      payload: { token, os }
    }
  };
}

export const SET_INITIAL_SCREEN = 'SET_INITIAL_SCREEN';
export function setInitialScreen(screen: string, params?: object) {
  return {
    type: SET_INITIAL_SCREEN,
    screen,
    params
  };
}
