import { isEqual } from 'lodash';
import { IService } from '@/common/DataTypes';

export const ADD_MASTER = 'ADD_MASTER';
export function addMaster(code) {
  return {
    type: ADD_MASTER,
    api: {
      endpoint: 'client/masters',
      method: 'post',
      payload: code,
      silent: true
    }
  };
}

export const START_APPOINTMENT_WIZARD = 'START_APPOINTMENT_WIZARD';
export const SET_MASTER_SERVICES = 'SET_MASTER_SERVICES';
export function startAppointmentWizard(masterId) {
  return async function (dispatch) {
    dispatch({
      type: START_APPOINTMENT_WIZARD,
      masterId
    });

    return await dispatch({
      type: SET_MASTER_SERVICES,
      api: {
        endpoint: 'client/masters/services',
        payload: { masterId }
      }
    });
  }
}

export const SELECT_APPOINTMENT_SERVICES = 'SELECT_APPOINTMENT_SERVICES';
export const SET_MASTER_TIMESLOTS = 'SET_MASTER_TIMESLOTS';
export function selectAppointmentServices(services: IService[]) {
  return async function (dispatch, getState) {
    const { selectedServices } = getState().appointmentWizard;
    if (isEqual(services, selectedServices)) {
      return;
    }

    dispatch({
      type: SELECT_APPOINTMENT_SERVICES,
      services
    });

    const { duration, masterId } = getState().appointmentWizard;
    return await dispatch({
      type: SET_MASTER_TIMESLOTS,
      api: {
        endpoint: 'client/masters/availability',
        payload: {
          masterId,
          duration
        }
      }
    });
  }
}

export const SELECT_TIME_RANGE = 'SELECT_TIME_RANGE';
export function selectTimeRange(range: number) {
  return {
    type: SELECT_TIME_RANGE,
    range
  };
}

export const SELECT_TIME_SLOT = 'SELECT_TIME_SLOT';
export function selectTimeSlot(timeSlot) {
  return {
    type: SELECT_TIME_SLOT,
    timeSlot
  };
}

export const FINISH_APPOINTMENT = 'FINISH_APPOINTMENT';
export function finishAppointment(note, remindIn) {
  return async function (dispatch, getState) {
    const { masterId, selectedServices, selectedTime } = getState().appointmentWizard;
    
    return await dispatch({
      type: FINISH_APPOINTMENT,
      api: {
        endpoint: 'client/appointments',
        method: 'post',
        payload: {
          masterId,
          serviceIds: selectedServices.map(s => s.id),
          time: selectedTime,
          note,
          remindIn
        }
      }
    });
  }
}

export const SET_PAST_APPOINTMENTS = 'SET_PAST_APPOINTMENTS';
export function loadPastAppointments() {
  return async function (dispatch, getState) {
    if (!getState().appointments.past.length) {
      dispatch({
        type: SET_PAST_APPOINTMENTS,
        api: {
          endpoint: 'client/appointments/past'
        }
      });
    }
  };
}

export const SAVE_CLIENT_APPOINTMENT = 'SAVE_CLIENT_APPOINTMENT';
export function saveClientAppointment(appointment) {
  return {
    type: SAVE_CLIENT_APPOINTMENT,
    api: {
      endpoint: 'client/appointments/' + appointment.id,
      method: 'post',
      payload: { appointment }
    }
  };
}

export const CANCEL_CLIENT_APPOINTMENT = 'CANCEL_CLIENT_APPOINTMENT';
export function cancelClientAppointment(appointment) {
  return {
    type: CANCEL_CLIENT_APPOINTMENT,
    api: {
      endpoint: 'client/appointments/' + appointment.id,
      method: 'delete',
      transform: () => ({ appointment })
    }
  };
}