export const SAVE_PROFILE = 'SAVE_PROFILE';
export function saveProfile(profile) {
  return {
    type: SAVE_PROFILE,
    api: {
      endpoint: 'profile',
      method: 'post',
      payload: { ...profile }
    }
  };
}

export const SET_PROFILE_PHOTO = 'SET_PROFILE_PHOTO';
export function uploadPhoto(upload) {
  return {
    type: SET_PROFILE_PHOTO,
    api: {
      endpoint: 'profile/photo',
      method: 'post',
      payload: { photo: upload },
      upload: true
    }
  };
}

export const SELECT_LANGUAGE = 'SELECT_LANGUAGE';
export function selectLanguage(lang) {
  return {
    type: SELECT_LANGUAGE,
    api: {
      endpoint: 'profile/language',
      method: 'post',
      payload: { lang }
    }
  };
}

export const SAVE_PHONE = 'SAVE_PHONE';
export function savePhone(phone: string) {
  return {
    type: SAVE_PHONE,
    api: {
      endpoint: 'profile/phone',
      method: 'post',
      payload: { phone }
    }
  };
}

export const REPEAT_CALL = 'REPEAT_CALL';
export function repeatCall() {
  return async function (dispatch, getState) {
    return dispatch({
      type: REPEAT_CALL,
      api: {
        endpoint: 'profile/phone/repeat',
        method: 'post',
        payload: {
          cid: getState().profile.cid
        }
      }
    });
  }
}

export const VERIFY_PHONE = 'VERIFY_PHONE';
export function verifyPhone(phone: string, code: string) {
  return async function (dispatch, getState) {
    return dispatch({
      type: VERIFY_PHONE,
      api: {
        endpoint: 'profile/phone/verify',
        method: 'post',
        silent: true,
        payload: {
          phone,
          code,
          cid: getState().profile.cid
        }
      }
    });
  }
}