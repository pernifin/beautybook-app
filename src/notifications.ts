import { Platform } from 'react-native';
import Config from 'react-native-config';
import PushNotification from 'react-native-push-notification';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import messaging from '@react-native-firebase/messaging';
import { RESULTS, checkNotifications } from 'react-native-permissions';
import { pickBy, mapKeys } from 'lodash';

import { attachDevice, setInitialScreen } from 'src/actions';
import reactotron from 'reactotron-react-native';

export async function configure(store) {
  PushNotification.configure({
    async onRegister(device) {
      if (device.os === 'ios') {
        device.token = await messaging().getToken();
      }

      reactotron.log(device);
      store.dispatch(attachDevice(device.token, device.os));
    },

    onNotification(notification) {
      reactotron.log(notification);

      if (notification.foreground && Platform.OS === 'android') {
        const { title, body } = notification.notification;

        PushNotification.localNotification({
          title,
          message: body
        });
      } else if (!notification.foreground) {
        const payload = mapKeys(
          pickBy(Platform.OS === 'ios' ? notification.data : notification, (val, key) => key.includes('app.')),
          (val, key) => key.replace('app.', '')
        );

        const screen = getScreenByNotificationPayload(payload);
        if (screen) {
          store.dispatch(setInitialScreen(screen, payload));
        }
      }

      notification.finish(PushNotificationIOS.FetchResult.NoData);
    },

    senderID: Config.FCM_SENDER_ID,
    requestPermissions: false
  });
}

export async function refreshToken() {
  const { status } = await checkNotifications();

  if (status === RESULTS.GRANTED) {
    PushNotification.requestPermissions();
  }
}

export async function requestPermissions() {
  await PushNotification.requestPermissions();
}

export function getScreenByNotificationPayload(payload: { [key: string]: string }) {
  switch (true) {
    case payload.appointmentId !== undefined:
      return 'AppointmentDetails';

    default:
      return null;
  }
}