import Config from 'react-native-config';
import moment, { Moment } from 'moment';
import { padStart, compact } from 'lodash';

import t from './Lang';

export function minutesToTime(minutes: number, addSeconds?: boolean) {
  return compact([
    padStart(Math.floor(minutes / 60 % 24).toString(), 2, '0'),
    padStart((minutes % 60).toString(), 2, '0'),
    addSeconds ? '00' : null
  ]).join(':');
}

export function timeToMinutes(time: string) {
  const [hours, minutes] = time.split(':');
  return Number(hours) * 60 + Number(minutes);
}

export function formatDate(date: string | Moment, withYear?: boolean) {
  return moment(date).format(`ddd, D MMM${withYear ? ' YYYY' : ''}`);
}

export function formatDatetime(datetime: string) {
  return moment(datetime).format('ddd, D MMM YYYY, HH:mm');
}

export function formatDatetimeLong(datetime: string) {
  return moment(datetime).format(`MMMM D, dddd [${t('#datetime.at')}] H:mm`);
}

export function formatDuration(duration: number) {
  const hours = Math.floor(duration / 60);
  const minutes = duration % 60;

  return compact([
    hours > 0 ? `${hours} ${t('#datetime.hours', { count: hours })}` : null,
    minutes > 0 ? `${minutes} ${t('#datetime.minutes', { count: minutes })}` : null,
    (hours || minutes ? '' : `0 ${t('#datetime.hours', { count: 0 })}`)
  ]).join(' ');
}

export function formatLongPeriod(period: number) {
  const days = Math.floor(period / 24);
  const hours = period % 24;

  return compact([
    days > 0 ? `${days} ${t('#datetime.days', { count: days })}` : null,
    hours > 0 ? `${hours} ${t('#datetime.hours', { count: hours })}` : null,
    (days || hours ? '' : `0 ${t('#datetime.days', { count: 0 })}`)
  ]).join(' ');
}

export function formatPrice(price: number) {
  return `${price || 0} ${t('#common.uah')}`;
}

export function checkPhone(phone: string) {
  return /^380\d{9}/.test(phone);
}

export function formatPhone(phone: string) {
  return phone && phone.replace(/^(\d+)(\d{2})(\d{3})(\d{2})(\d{2})$/, '+$1 ($2) $3 $4 $5');
}

export function getPhotoSource(profile) {
  if (profile.photo) {
    return {
      uri: profile.photo.startsWith('http')
        ? profile.photo
        : `${Config.STATIC_ENDPOINT}/userdata/${profile.id}/${profile.photo}`
    };
  } else {
    return require('../assets/images/profile-bg.png');
  }
}