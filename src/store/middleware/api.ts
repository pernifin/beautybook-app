import Config from 'react-native-config';
import axios from 'axios';
import { merge, forEach, get, identity } from 'lodash';

import { SET_TOKEN, QUERYING, setWarning, setError, logoutUser } from 'src/actions';

axios.defaults.baseURL = Config.API_ENDPOINT;

const statusHooks = {
  401: (data, next) => {
    next(logoutUser());
  },
  500: (data, next) => {
    next(setError(__DEV__ ? data.error : '#error.server'));
  }
};

const dataHooks = {
  'error': (next, data) => {
    next(setError(data.error))
  },
  'warning': (next, data) => {
    next(setWarning(data.warning))
  }
}

const getFormData = (payload) => {
  const formData = new FormData();
  forEach(payload, (value, key) => formData.append(key, value));
  return formData;
}

export default store => next => async action => {
  if (!action.api) {
    return next(action);
  }

  const { endpoint, method = 'get', payload = {}, transform = identity, upload = false, silent = false } = action.api;
  const token = store.getState().system.token;
  const options = {
    method: method,
    params: method.toLowerCase() === 'get' ? payload : null,
    data: method.toLowerCase() !== 'get' ? (upload ? getFormData(payload) : payload) : null,
    timeout: 10000,
    validateStatus: status => status < 500
  };

  if (upload) {
    merge(options, { headers: { 'Content-Type': 'multipart/form-data' } });
  }

  if (token) {
    merge(options, { headers: { Authorization: `Bearer ${token}` } });
  }

  delete action.api;
  next({ type: QUERYING, active: true, originalType: action.type, payload });

  try {
    const response = await axios(endpoint, options);
    next({ type: QUERYING, active: false, originalType: action.type, payload });

    const { authorization } = response.headers;
    if (authorization) {
      next({
        type: SET_TOKEN,
        token: authorization.split(' ')[1]
      });
    }

    if (!silent) {
      forEach(dataHooks, (hook, dataPath) => {
        const value = get(response.data, dataPath);
        if (value !== undefined) {
          hook(next, response.data);
        }
      });
    }

    if (statusHooks[response.status]) {
      return statusHooks[response.status](response.data, next);
    }

    if (response.data.success) {
      next({
        ...action,
        ...transform(response.data)
      });
    }

    return response.data;
  } catch (error) {
    const response = error.response;

    next({ type: QUERYING, active: false, originalType: action.type, payload });
    if (response && statusHooks[response.status]) {
      return statusHooks[response.status](response.data, next);
    }

    const message = response?.data.error || (__DEV__ ? error.message : '#error.network');
    
    return next(setError(message));
  }
};