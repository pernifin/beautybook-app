import { createStore, applyMiddleware, compose, StoreEnhancer } from 'redux';
import thunk from 'redux-thunk';
import Reactotron from 'reactotron-react-native';

import api from './middleware/api';
import rootReducer from '../reducers';

export default function configureStore() {
  const enhancers: StoreEnhancer[] = [applyMiddleware(thunk, api)];
  if (__DEV__) {
    enhancers.push(Reactotron.createEnhancer());
  }

  return createStore(
    rootReducer,
    undefined,
    compose(...enhancers)
  );
}