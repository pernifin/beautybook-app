import { Platform } from 'react-native';
import { padStart } from 'lodash';

export const color = {
  primary: '#F4726D',
  primaryRange: ['#F15459', '#F17A54'],
  accent: '#5C7CEF',
  accentRange: ['#5C7CEF', '#AFB6CD'],
  accentAlpha(opacity: number) {
    return this.accent + padStart(Math.round(opacity * 255).toString(16), 2, '0');
  },
  accentLight: '#AFB6CD',
  accentLightAlpha(opacity: number) {
    return this.accentLight + padStart(Math.round(opacity * 255).toString(16), 2, '0');
  },
  accentDark: '#0E2C96',
  darkBlue: '#455FBD',
  error: '#F25E58',
  black: '#000000',
  white: '#FFFFFF',
  inactive: '#AAAAAA',
  grey: '#444444',
  green: '#4BC889',
  yellow: '#FFB800',
  red: '#F15459'
};

const androidFonts = {
  300: 'Open Sans Light',
  400: 'Open Sans Regular',
  500: 'Open Sans SemiBold',
  600: 'Open Sans Bold',
  //700: 'Open Sans Bold'
};

export const font = (weight: number) => Platform.select({
  ios: {
    fontFamily: 'System',
    fontWeight: String(weight) as '300' | '400' | '500' | '600'
  },
  android: {
    fontFamily: androidFonts[weight]
  }
});

export const header = {
  headerStyle: {
    zIndex: 2,
    borderBottomColor: 'transparent',
    backgroundColor: Platform.OS === 'android' ? color.accent : color.white,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowColor: "#3A79C3",
    shadowOpacity: 0.3,
    shadowRadius: 5
  },
  headerTitleStyle: {
    color: Platform.OS === 'android' ? color.white : color.black
  }
};