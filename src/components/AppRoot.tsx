import React, { Component } from 'react';
import { View, StatusBar, Animated, Platform } from 'react-native';
import {
  createAppContainer,
  createSwitchNavigator,
  NavigationSwitchProp,
  NavigationContainerComponent,
  NavigationActions,
  NavigationNavigateActionPayload
} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Config from 'react-native-config';
import SplashScreen from 'react-native-splash-screen';
import { connect } from 'react-redux';
import { capitalize } from 'lodash';

import { refreshToken } from 'src/notifications';
import { Lang } from '../Lang';
import { color } from 'src/theme';

import { fetchUserInfo, loadInitialData, setError, dismissError } from 'src/actions';

import InitScreen from './InitScreen';
import AuthScreen from './auth/AuthScreen';
import MasterModeTabs from './master/MasterModeTabs';
import ClientModeTabs from './client/ClientModeTabs';
import ModalScreen from './common/modals/ModalScreen';

export const AppContainer = createAppContainer(
  createStackNavigator({
    Screens: createSwitchNavigator({
      Init: InitScreen,
      Auth: AuthScreen,
      Master: MasterModeTabs,
      Client: ClientModeTabs
    }, {
      initialRouteName: 'Init'
    }),
    Modal: ModalScreen
  }, {
    mode: 'modal',
    headerMode: 'none',
    initialRouteName: 'Screens',
    transparentCard: true,
    transitionConfig: () => ({
      transitionSpec: { timing: Animated.timing },
      screenInterpolator: () => {}
    }),
    defaultNavigationOptions: () => ({
      gesturesEnabled: false
    })
  })
);

export type Props = {
  navigation: NavigationSwitchProp;
  loggedIn: boolean;
  mode: string;
  initScreen: NavigationNavigateActionPayload;
  error: string;
  fetchUserInfo: () => Promise<any>;
  loadInitialData: (mode: string) => Promise<any>;
  setError: (message: string) => void;
  dismissError: () => void;
}

export class AppRoot extends Component<Props> {
  container = React.createRef<NavigationContainerComponent>();
  loaded = false;
  started = false;

  readonly state = { lang: Lang.activeLang };

  async componentDidMount() {
    await this.props.fetchUserInfo();
    Lang.addEventListener(this.onLangChange);
    this.loaded = true;

    this.componentDidUpdate(this.props);
  }

  componentWillUnmount() {
    Lang.removeEventListener(this.onLangChange);
  }

  async componentDidUpdate(prevProps: Props) {
    const { loggedIn, mode, initScreen } = this.props;

    if (mode && prevProps.mode !== mode) {
      this.props.loadInitialData(mode);
    }

    if (loggedIn && prevProps.loggedIn !== loggedIn) {
      refreshToken();
    }

    let screen: NavigationNavigateActionPayload = {
      routeName: this.props.loggedIn && this.props.mode ? capitalize(this.props.mode) : 'Auth',
      params: {}
    };

    if (this.props.error) {
      screen = this.getErrorScreen;
    } else if (initScreen && !this.started || prevProps.initScreen !== initScreen) {
      screen = initScreen;
    }

    if (this.loaded) {
      SplashScreen.hide();
      this.openScreen(screen);
      this.started = true;
    }
  }

  onLangChange = () => {
    this.setState({ lang: Lang.activeLang });
  }

  get getErrorScreen() {
    return {
      routeName: 'Modal',
      params: {
        message: this.props.error,
        icon: 'error',
        closeLabel: '#common.close',
        onClosePress: this.props.dismissError
      }
    };
  }

  openScreen(screen: NavigationNavigateActionPayload) {
    return this.container.current.dispatch(
      NavigationActions.navigate(screen)
    );
  }

  render() {
    return (
      <>
        <StatusBar
          backgroundColor={color.accentDark}
          barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'}
        />
        <AppContainer ref={this.container} screenProps={{ lang: this.state.lang }} />
      </>
    );
  }
}

export default connect(
  state => ({
    loggedIn: state.system.loggedIn,
    mode: state.system.mode,
    initScreen: state.system.initScreen,
    error: state.errorMessage
  }),
  {
    fetchUserInfo,
    loadInitialData,
    setError,
    dismissError
  }
)(AppRoot);
