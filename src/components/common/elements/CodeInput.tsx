import React, { Component } from 'react';
import { TextInput, View, Text, StyleSheet } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { range } from 'lodash';

import { color } from 'src/theme';
import Span from '@/common/Span';
import { SecondaryButton } from '@/common/buttons';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

export type Props = {
  onCodeInput: (code: string) => void;
  onRepeatCall: () => void;
  invalid?: boolean;
}

export type State = {
  code: string;
  invalid: boolean;
  repeat: boolean;
}

export default class CodeInput extends Component<Props, State> {
  readonly state = {
    code: '',
    invalid: false,
    repeat: false
  };

  inputRef = React.createRef<TextInput>();
  timeout: number;

  componentDidMount() {
    this.timeout = setTimeout(() => this.setState({ repeat: true }), 30000);
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  componentDidUpdate(prevProps: Props, prevState: State) {
    if (prevProps !== this.props && this.props.invalid !== this.state.invalid) {
      this.setState({ invalid: this.props.invalid });
    }

    if (prevState.code !== this.state.code) {
      this.setState({ invalid: false });
    }
  }

  focus = () => {
    this.inputRef.current.focus();
  }

  onChangeText = (code) => {
    this.setState({ code });
    if (code.length === 4) {
      this.props.onCodeInput(code);
    }
  }

  onRepeat = () => {
    this.setState({ code: '', repeat: false, invalid: false });
    this.timeout = setTimeout(() => this.setState({ repeat: true }), 30000);

    this.props.onRepeatCall();
  }

  render() {
    const { code, invalid, repeat } = this.state;

    return (
      <View>
        <TextInput
          ref={this.inputRef}
          keyboardType='number-pad'
          autoFocus
          maxLength={4}
          value={code}
          onChangeText={this.onChangeText}
          style={styles.fakeInput}
        />

        <TouchableWithoutFeedback onPress={this.focus}>
          <View style={styles.code}>
            {range(4).map(i => (
              <View key={i} style={[styles.letter, invalid && styles.error]}>
                <Span style={[styles.letterText, invalid && styles.error]}>{this.state.code[i] || ''}</Span>
              </View>
            ))}
          </View>
        </TouchableWithoutFeedback>

        <View style={styles.info}>
          {invalid && (
            <Animatable.Text animation='shake'>
              <Span style={invalid && styles.error}>#auth.invalidCode</Span>
            </Animatable.Text>
          )}
        </View>

        {repeat && <SecondaryButton label='#auth.repeatCall' onPress={this.onRepeat} />}
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  fakeInput: {
    width: 0,
    height: 0
  },
  code: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginHorizontal: '25%',
    marginTop: 20
  },
  letter: {
    width: '18%',
    height: 35,
    alignItems: 'center',
    borderBottomWidth: 2,
    borderBottomColor: color.accentLight,
  },
  letterText: {
    fontSize: 25,
    fontWeight: '600'
  },
  info: {
    alignSelf: 'center',
    marginVertical: 15,
    height: 20
  },
  error: {
    color: color.error,
    borderBottomColor: color.error
  }
});
