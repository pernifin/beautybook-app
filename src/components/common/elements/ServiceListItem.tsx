import React from 'react';
import { View, TouchableHighlight, TouchableNativeFeedback, StyleSheet, Platform } from 'react-native';
import IonIcon from 'react-native-vector-icons/Ionicons';

import { formatDuration, formatPrice } from 'src/utils';
import { color } from 'src/theme';

import { IService } from '../DataTypes';
import Span from '../Span';

export type Props = {
  service: IService;
  details?: boolean;
  selectable?: boolean;
  selected?: boolean;
  onPress?: () => void;
}

export default function ServiceListItem(props: Props) {
  const { service, selected } = props;
  const Touchable = Platform.OS === 'ios' ? TouchableHighlight : TouchableNativeFeedback;

  const renderInner = () => (
    <View style={[styles.item, selected && styles.itemSelected]}>
      <Span raw numberOfLines={1} style={[styles.title, selected && styles.titleSelected]}>{service.title}</Span>
      {props.details
        ? <Span style={styles.details}>{formatPrice(service.price)}  |  {formatDuration(service.duration)}</Span>
        : null}

      <View style={styles.mark}>
        {props.selectable
          ? (selected ? <IonIcon name='ios-checkmark' size={30} color='#fff' /> : null)
          : <IonIcon name='ios-arrow-forward' size={20} color={color.accentAlpha(0.5)} />}
      </View>
    </View>
  );

  if (Platform.OS === 'ios') {
    return (
      <TouchableHighlight
        underlayColor={color.accent}
        activeOpacity={0.8}
        onPress={props.onPress}
        style={styles.container}
      >
        {renderInner()}
      </TouchableHighlight>
    );
  } else {
    return (
      <TouchableNativeFeedback
        background={TouchableNativeFeedback.Ripple(color.accentAlpha(0.1))}
        useForeground
        onPress={props.onPress}
        style={styles.container}
      >
        {renderInner()}
      </TouchableNativeFeedback>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    //height: 60,
    //overflow: 'hidden',
    marginTop: 5,
    borderRadius: 6
  },
  item: {
    overflow: 'hidden',
    marginBottom: Platform.OS === 'android' ? 5 : 0,
    paddingLeft: 15,
    paddingRight: 25,
    paddingVertical: 10,
    borderRadius: 5,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: color.accentAlpha(0.2),
    backgroundColor: '#fff'
  },
  itemSelected: {
    backgroundColor: color.accent
  },
  title: {
    fontSize: 14,
    lineHeight: 20
  },
  titleSelected: {
    color: '#fff'
  },
  details: {
    fontSize: 12,
    lineHeight: 20,
    color: '#AFB6CD'
  },
  mark: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 8,
    justifyContent: 'center'
  }
});