import React, { ReactElement, Component } from 'react';
import { StyleSheet } from 'react-native';
import { CalendarList } from 'react-native-calendars';
import moment from 'moment';
import { findKey } from 'lodash';

export type Props = {
  startDate?: string;
  currentDate?: string;
  dayComponent: ReactElement;
  markedDates?: any;
  monthHeight: number;
  onDayPress?: (date: string) => void;
  onMonthChange?: (months: string[]) => void;
};

export default class MonthCalendar extends Component<Props> {
  calendarRef = React.createRef<CalendarList>();

  render() {
    const backMonths = moment(this.props.currentDate).endOf('month').diff(this.props.startDate, 'M');
    const onMonthChange = months => {
      this.props.onMonthChange && this.props.onMonthChange(months.map(m => moment(m.timestamp).format('YYYY-MM')));
    };

    return (
      <CalendarList
        ref={this.calendarRef}
        current={this.props.currentDate}
        firstDay={1}
        pastScrollRange={backMonths}
        futureScrollRange={3}
        calendarHeight={this.props.monthHeight}
        showScrollIndicator={true}
        markedDates={this.props.markedDates}
        dayComponent={this.props.dayComponent as any}
        onDayPress={date => this.props.onDayPress(date.dateString)}
        onVisibleMonthsChange={onMonthChange}
        theme={calendarTheme as any}
        style={styles.calendar}
      />
    );
  }
}

export const styles = StyleSheet.create({
  calendar: {
    //marginBottom: 35
  }
});

export const calendarTheme = {
  'stylesheet.calendar-list.main': {
    calendar: {
      paddingLeft: 20,
      paddingRight: 20
    }
  },
  'stylesheet.calendar.main': {
    week: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      // borderWidth: 1,
      // borderColor: 'blue'
    }
  },
  'stylesheet.calendar.header': {
    header: {
      alignItems: 'flex-start',
      paddingLeft: 0,
      marginTop: 6
    },
    monthText: {
      margin: 0,
      marginVertical: 10,
      fontSize: 22,
      fontWeight: '300',
      color: '#2d4150'
    }
  }
};