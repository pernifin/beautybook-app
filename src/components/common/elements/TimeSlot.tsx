import React, { ReactNode } from 'react';
import { TouchableOpacity, StyleSheet, ViewStyle } from 'react-native';

import { color } from 'src/theme';
import { minutesToTime } from 'src/utils';

import Span from '../Span'

export type Props = {
  time?: number;
  enabled?: boolean;
  selected?: boolean;
  onPress?: () => void;
  style?: ViewStyle | ViewStyle[];
  children?: ReactNode | ReactNode[];
}

export default function TimeSlot(props: Props) {
  const { enabled = true, selected = false } = props;

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      disabled={!enabled}
      onPress={props.onPress}
      style={[styles.container, enabled && styles.active, selected && styles.selected, StyleSheet.flatten(props.style)]}
    >
      {'time' in props 
       ? (
        <Span numberOfLines={1} style={[styles.label, enabled && styles.labelActive, selected && styles.labelSelected]}>
          {minutesToTime(props.time)}
        </Span>
       ) : (
        props.children
       )}
    </TouchableOpacity>
  );
}

export const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    alignItems: 'center',
    minWidth: 44,
    minHeight: 22,
    borderRadius: 14,
    borderWidth: 1,
    borderColor: 'transparent',
    backgroundColor: color.accentAlpha(0.1)
  },
  active: {
    borderColor: color.accentAlpha(0.3),
    backgroundColor: '#fff'
  },
  selected: {
    borderColor: color.accent,
    backgroundColor: color.accent
  },
  label: {
    textAlign: 'center',
    fontSize: 11,
    fontWeight: '500',
    lineHeight: 20,
    color: color.accentAlpha(0.4)
  },
  labelActive: {
    color: '#000',
  },
  labelSelected: {
    color: '#fff'
  }
});
