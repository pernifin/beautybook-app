import React from 'react'
import { View, StyleSheet, StyleProp, ViewStyle } from 'react-native';
import { BorderlessButton } from 'react-native-gesture-handler';
import IonIcon from 'react-native-vector-icons/Ionicons';

import { color } from 'src/theme';

import { IService } from '../DataTypes';
import Span from '../Span';

export type Props = {
  services: IService[];
  label?: boolean;
  onDeletePress?: (service: IService) => void;
  style?: StyleProp<ViewStyle>;
}

export default function SelectedServicesList(props: Props) {
  const style = StyleSheet.flatten([styles.container, props.style]);

  return (
    <View style={style}>
      {props.label && <Span style={styles.header}><Span>#services.selectedLabel</Span>:</Span>}
      {props.services.map(service => (
        <View key={service.id} style={styles.record}>
          <View style={styles.name}>
            <Span raw style={styles.nameText}>{service.title}</Span>
          </View>
          {props.onDeletePress && (
            <BorderlessButton onPress={() => props.onDeletePress(service)}>
              <IonIcon name='ios-close' size={25} style={styles.remove} />
            </BorderlessButton>
          )}
        </View>
      ))}
    </View>
  );
}

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 30,
    paddingVertical: 20
  },
  header: {
    marginBottom: 10,
    fontSize: 15,
    fontWeight: '500'
  },
  record: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5
  },
  name: {
    paddingVertical: 8,
    paddingHorizontal: 12,
    marginRight: 4,
    borderRadius: 4,
    backgroundColor: color.accent
  },
  nameText: {
    fontSize: 13,
    color: '#fff'
  },
  remove: {
    paddingHorizontal: 8
  }
});