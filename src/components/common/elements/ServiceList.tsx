import React from 'react';
import { SectionList, StyleSheet } from 'react-native';
import { groupBy, map, sortBy } from 'lodash';

import { IService } from '../DataTypes';
import ServiceListItem from './ServiceListItem';
import Span from '../Span';

export type Props = {
  services: IService[];
  selected?: IService[];
  selectable?: boolean;
  onServicePress?: (service: IService) => void;
  onDeletePress?: (service: IService) => void;
}

type Section = {
  id: string;
  title: string;
  data: IService[];
}

export default class ServiceList extends React.Component<Props> {
  static defaultProps = {
    selected: [],
    onServicePress: () => {},
    onDeletePress: () => {}
  }

  get byCategory(): Section[] {
    const { services = [] } = this.props;
    const byCategory = groupBy(services, service => service.category.id);

    const groups = map(byCategory, (services, id) => ({
      id,
      title: services[0].category.title,
      data: sortBy(services, s => s.title)
    }));

    return sortBy(groups, c => c.title);
  }

  renderHeader(section: Section) {
    return (
      <Span style={styles.header}>{section.title}</Span>
    )
  }

  renderItem(service: IService) {
    const selected = !!this.props.selected.find(selected => selected.id === service.id);

    return (
      <ServiceListItem
        service={service}
        details
        selectable={this.props.selectable}
        selected={selected}
        onPress={() => this.props.onServicePress(service)}
      />
    );
  }

  render() {
    return (
      <SectionList
        sections={this.byCategory}
        renderSectionHeader={({ section }) => this.renderHeader(section as Section)}
        renderItem={({ item }) => this.renderItem(item)}
        keyExtractor={item => item.id.toString()}
        contentContainerStyle={styles.container}
      />
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    paddingBottom: 8
  },
  header: {
    paddingTop: 30,
    paddingBottom: 10,
    fontSize: 16,
    fontWeight: '600',
    lineHeight: 20,
    backgroundColor: '#fff'
  },

  backItemContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginLeft: 50,
    backgroundColor: '#ff3b30'
  },
  deleteButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 75
  }
});