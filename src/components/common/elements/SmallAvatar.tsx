import React from 'react';
import { View, Image, StyleSheet, Platform } from 'react-native';

import { color } from 'src/theme';
import { getPhotoSource } from 'src/utils';

import { IProfile } from '../DataTypes';
import Gradient from '../containers/Gradient';
import Span from '../Span';

export type Props = {
  profile: Partial<IProfile>;
}

export default function SmallAvatar(props: Props) {
  const { profile } = props;

  if (props.profile.photo) {
    return (
      <Image source={getPhotoSource(profile)} style={styles.image} />
    );
  } else {
    return (
      <Gradient colors={color.accentRange} style={styles.image}>
        <Span style={styles.letter}>{props.profile.name?.[0]}</Span>
      </Gradient>
    );
  }
}

export const styles = StyleSheet.create({
  image: {
    width: 60,
    height: 60,
    borderRadius: 30
  },
  letter: {
    textAlign: 'center',
    fontSize: 30,
    lineHeight: Platform.OS === 'ios' ? 60 : 58,
    fontWeight: '600',
    color: color.white
  }
});
