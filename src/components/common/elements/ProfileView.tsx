import React, { Component } from 'react';
import { View, ScrollView, TouchableOpacity, Animated, Linking, Dimensions, StyleSheet } from 'react-native';

import { color } from 'src/theme';
import { formatPhone, getPhotoSource } from 'src/utils';
import { IProfile } from '../DataTypes';
import LineSeparator from '../LineSeparator';
import Span from '../Span';
import Icon from '../Icon';


export type Props = {
  profile: IProfile;
  full?: boolean;
  showActions?: boolean;
}

export type State = {
  scrollY: Animated.Value;
}

const PHOTO_HEIGHT = Dimensions.get('screen').width * 0.8;
const SHIFT_DISTANCE = PHOTO_HEIGHT;

export default class ProfileView extends Component<Props, State> {
  scrollEvent = null;
  photoHeight = null;
  overlayHeight = null;
  overlayOpacity = null;
  backgroundPosition = null;
  backgroundRadius = null;

  constructor(props: Props) {
    super(props);
    this.openCall = this.openCall.bind(this);
    this.openInstagram = this.openInstagram.bind(this);

    this.state = {
      scrollY: new Animated.Value(0)
    };

    this.scrollEvent = Animated.event(
      [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }]
    );

    this.photoHeight = this.state.scrollY.interpolate({
      inputRange: [0, SHIFT_DISTANCE],
      outputRange: [PHOTO_HEIGHT + 20, PHOTO_HEIGHT - SHIFT_DISTANCE + 20],
      extrapolateRight: 'clamp'
    });

    this.overlayHeight = Animated.subtract(SHIFT_DISTANCE + 20, this.state.scrollY);
    this.overlayOpacity = this.state.scrollY.interpolate({
      inputRange: [SHIFT_DISTANCE / 3, SHIFT_DISTANCE - 20],
      outputRange: [0, 1]
    });

    this.backgroundPosition = this.state.scrollY.interpolate({
      inputRange: [0, SHIFT_DISTANCE],
      outputRange: [PHOTO_HEIGHT, PHOTO_HEIGHT - SHIFT_DISTANCE],
      extrapolateRight: 'clamp'
    });

    this.backgroundRadius = this.state.scrollY.interpolate({
      inputRange: [SHIFT_DISTANCE - 40, SHIFT_DISTANCE],
      outputRange: [20, 0],
      extrapolate: 'clamp'
    });
  }

  openCall() {
    Linking.openURL(`tel:${this.props.profile.phone}`);
  }

  async openInstagram() {
    const { instagram } = this.props.profile;
    const appLink = `instagram://user?username=${instagram}`;
    const pageLink = `https://www.instagram.com/${instagram}`;

    Linking.openURL(await Linking.canOpenURL(appLink) ? appLink : pageLink);
  }

  renderTopActions() {
    return (
      <View style={styles.actionsContainer}>
        <TouchableOpacity activeOpacity={0.6} onPress={this.openCall} style={styles.action}>
          <Icon style={styles.actionIcon} name='phone' size={15} color='#fff' />
        </TouchableOpacity>
        <TouchableOpacity activeOpacity={0.6} onPress={this.openInstagram} style={styles.action}>
          <Icon style={styles.actionIcon} name='instagram' size={15} color='#fff' />
        </TouchableOpacity>
      </View>
    );
  }

  renderBasicInfo() {
    const { profile, full, showActions } = this.props;

    return (
      <View style={styles.info}>
        <Span raw style={styles.name}>{profile.name || '#profile.anonymous'}</Span>
        {full && profile.specialization ? <Span raw>{profile.specialization}</Span> : null}

        <LineSeparator short style={styles.infoSeparator} />

        {full && profile.address ? <Span raw style={styles.address}>{profile.address}</Span> : null}
        <Span style={styles.address}>{formatPhone(profile.phone || '380000000000')}</Span>

        {full && !showActions && profile.instagram ? (
          <TouchableOpacity activeOpacity={0.6} onPress={this.openInstagram}>
            <View style={styles.instagram}>
              <Icon name='instagram' size={17} />
              <Span style={styles.instaUsername}>@{profile.instagram}</Span>
            </View>
          </TouchableOpacity>
        ) : null}
      </View>
    );
  }

  render() {
    const { profile, showActions } = this.props;

    return (
      <View style={styles.container}>
        <Animated.Image
          source={getPhotoSource(profile)}
          style={[
            styles.photo,
            { height: this.photoHeight }
          ]}
        />

        {showActions && this.renderTopActions()}

        <Animated.View style={[styles.scrollOverlay, { height: this.overlayHeight, opacity: this.overlayOpacity }]} />
        <Animated.View style={[styles.contentBackground, { top: this.backgroundPosition, borderRadius: this.backgroundRadius }]} />

        <ScrollView
          onScroll={this.scrollEvent}
          scrollEventThrottle={16}
          showsVerticalScrollIndicator={false}
          overScrollMode='never'
          style={styles.contentContainer}
          contentContainerStyle={styles.content}
        >
          {this.renderBasicInfo()}

          {this.props.children}
        </ScrollView>
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#efefef'
  },
  photo: {
    position: 'absolute',
    top: 0,
    resizeMode: 'cover',
    width: '100%',
    height: PHOTO_HEIGHT + 20,
    backgroundColor: color.darkBlue
  },
  actionsContainer: {
    position: 'absolute',
    zIndex: 4,
    top: 10,
    right: 10,
    flexDirection: 'row'
  },
  action: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 35,
    height: 35,
    paddingLeft: 1,
    marginLeft: 10,
    borderRadius: 13,
    backgroundColor: '#0004'
  },
  actionIcon: {
    lineHeight: 35
  },
  contentBackground: {
    position: 'absolute',
    zIndex: 2,
    top: PHOTO_HEIGHT,
    bottom: 0,
    width: '100%',
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    backgroundColor: '#fff'
  },
  scrollOverlay: {
    position: 'absolute',
    zIndex: 1,
    top: 0,
    width: '100%',
    backgroundColor: color.black
  },
  contentContainer: {
    zIndex: 3
  },
  content: {
    paddingTop: 25 + SHIFT_DISTANCE,
  },
  info: {
    alignItems: 'center',
    paddingHorizontal: 30,
    marginBottom: 25
  },
  name: {
    marginBottom: 5,
    fontSize: 20,
    fontWeight: '600'
  },
  infoSeparator: {
    marginVertical: 15,
    borderColor: '#b3b3b3'
  },
  address: {
    fontSize: 12,
    lineHeight: 20,
    color: '#808080'
  },
  instagram: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20
  },
  instaUsername: {
    marginLeft: 8,
    fontSize: 15,
    fontWeight: '600'
  },
  navigationList: {
    marginVertical: 25
  },
  navigationBtn: {
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 25,
    marginVertical: 0
  }
});