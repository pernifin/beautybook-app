import React, { Component } from 'react'
import { View, StyleSheet, Platform, Linking } from 'react-native';

import { color } from 'src/theme';
import { formatDatetimeLong, formatDuration, formatPrice, formatPhone } from 'src/utils';

import { IAppointment } from '../DataTypes';
import Touchable from '../shims/Touchable';
import SmallAvatar from './SmallAvatar';
import SelectedServicesList from './SelectedServicesList';
import Form from '../containers/Form';
import { DurationField, PickerField, TextInputField, StaticTextField } from '../formFields';
import Icon from '../Icon';
import Span from '../Span';

export type Props = {
  appointment: IAppointment;
  editable?: boolean;
  onUpdate?: (appointment: IAppointment) => void;
}

export default class AppointmentView extends Component<Props> {
  updateAppointment(values: Partial<IAppointment>) {
    if (this.props.onUpdate) {
      this.props.onUpdate({ ...this.props.appointment, ...values });
    }
  }

  callUser = (phone: string) => {
    Linking.openURL(`tel:${phone}`);
  }

  render() {
    const { appointment, editable = false } = this.props;
    const user = appointment.master 
      || appointment.client
      || { name: appointment.clientName, phone: appointment.clientPhone };

    return (
      <View>
        <View style={styles.header}>
          <View style={styles.user}>
            <SmallAvatar profile={user} />
            <View style={styles.userInfo}>
              <Span raw style={styles.userName}>{user.name}</Span>
              <Span raw style={styles.userPhone}>{formatPhone(user.phone)}</Span>
            </View>
            <Touchable borderless onPress={() => this.callUser(user.phone)} style={styles.callIcon}>
              <Icon name='phone' size={20} color={color.accent} />
            </Touchable>
          </View>
          <View style={styles.datetime}>
            <View style={styles.dateIcon}>
              <Icon name='nav-schedule' size={22} color={color.primary} />
            </View>
            <Span style={styles.dateValue}>{formatDatetimeLong(appointment.datetime)}</Span>
          </View>
        </View>

        <SelectedServicesList services={appointment.services} style={styles.services} />

        <Form style={styles.form}>
          <StaticTextField bold align='right' label='#label.duration' value={formatDuration(appointment.duration)} />
          <StaticTextField bold align='right' label='#label.price' value={formatPrice(appointment.price)} />
          {editable ? this.renderReminder() : null}
          {editable ? this.renderNoteEdit() : appointment.note && this.renderNoteView()}
        </Form>
      </View>
    );
  }

  renderReminder() {
    const { appointment } = this.props;

    return (
      <PickerField
        label='#label.reminder'
        align='right'
        value={appointment.remindIn ? formatDuration(appointment.remindIn) : '#appointments.noRemind'}
      >
        <DurationField
          min={60} max={480} step={60}
          value={appointment.remindIn}
          onValueChange={remindIn => this.updateAppointment({ remindIn })}
        />
      </PickerField>
    );
  }

  renderNoteEdit() {
    const { appointment } = this.props;

    return (
      <TextInputField
        label='#label.note'
        placeholder='#label.addNote'
        value={appointment.note}
        onValueChange={note => this.updateAppointment({ note })}
        multiline
      />
    );
  }

  renderNoteView() {
    return (
      <StaticTextField label='#label.note' value={this.props.appointment.note} />
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    //paddingVertical: 20
  },
  header: {
    marginHorizontal: 16,
    marginVertical: 20,
    borderRadius: Platform.OS === 'android' ? 3 : 8,
    backgroundColor: '#fff',
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 3,
    shadowOpacity: 0.2,
    elevation: 4
  },
  user: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    borderBottomWidth: 1,
    borderBottomColor: color.accentAlpha(0.1)
  },
  userInfo: {
    flex: 1,
    marginHorizontal: 15
  },
  userName: {
    fontSize: 16,
    lineHeight: 20,
    fontWeight: '600'
  },
  userPhone: {
    fontSize: 14,
    lineHeight: 20,
    color: color.accentLight
  },
  callIcon: {
    width: 30,
    height: 30,
    padding: 5
  },
  datetime: {
    flexDirection: 'row',
    height: 60,
    paddingVertical: 20,
    paddingHorizontal: 15
  },
  dateIcon: {
    height: 22,
    top: -1,
    marginRight: 12
  },
  dateValue: {
    fontSize: 16,
    lineHeight: 20,
    fontWeight: '500'
  },
  services: {
    paddingTop: 5,
    paddingBottom: 15,
    paddingHorizontal: 16
  },
  form: {
    marginBottom: 16
  }
});