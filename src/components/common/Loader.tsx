import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Spinner from 'react-native-spinkit';
import { color } from 'src/theme';

export type Props = {
  active: boolean;
}

export default function Loader(props: Props) {
  return (
    <View style={[styles.overlay, props.active && styles.active]}>
      <Spinner type='Circle' size={60} color={color.primary} />
    </View>
  );
}

const styles = StyleSheet.create({
  overlay: {
    position: 'absolute',
    zIndex: 10,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    top: 0,
    left: 0,
    width: '100%',
    height: 0,
    backgroundColor: color.accentAlpha(0.2)
  },
  active: {
    height: 'auto',
    bottom: 0
  }
})