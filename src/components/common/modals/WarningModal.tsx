import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Modal from 'react-native-modal';
import IonIcon from 'react-native-vector-icons/Ionicons';

import { MainButton, ModalSecondaryButton } from '../buttons';
import Span from '../Span';

export type Props = {
  visible: boolean;
  message: string;

  onClosePress: () => void;
  closeText: string;

  isConfirm?: boolean;
  onConfirmPress?: () => void;
  confirmText?: string;

  onModalHidden?: () => void;
}

export default class WarningModal extends Component<Props> {
  static defaultProps = {
    onModalHidden: () => null
  };

  render() {
    return (
      <Modal
        isVisible={this.props.visible}
        animationIn='fadeIn'
        animationOut='fadeOut'
        backdropTransitionOutTiming={0}
        onModalHide={this.props.onModalHidden}
      >
        <View style={styles.modalContainer}>
          <IonIcon name='ios-alert' size={40} color='#333' />
          <Span style={styles.modalText}>{this.props.message}</Span>
          {this.props.isConfirm && (
            <MainButton label={this.props.confirmText} onPress={this.props.onConfirmPress} />
          )}

          <ModalSecondaryButton label={this.props.closeText} onPress={this.props.onClosePress} />
        </View>
      </Modal>
    )
  }
}

export const styles = StyleSheet.create({
  modalContainer: {
    // height: 200,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    padding: 20,
    paddingTop: 50,
    borderRadius: 10,
    backgroundColor: '#fff'
  },
  modalText: {
    marginVertical: 30,
    fontSize: 20,
    textAlign: 'center'
  }
});