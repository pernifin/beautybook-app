import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Modal from 'react-native-modal';

import { MainButton, SecondaryButton } from '../buttons';
import Span from '../Span';

export type Props = {
  visible: boolean;
  message?: string;
  onCancel: () => void;
  cancelText: string;
  onConfirm: () => void;
  confirmText: string;
  onModalHidden?: () => void;
}

export default class ConfirmModal extends Component<Props> {
  static defaultProps = {
    onModalHidden: () => null,
    cancelText: 'Cancel',
    confirmText: 'Confirm'
  };

  render() {
    return (
      <Modal
        isVisible={this.props.visible}
        animationIn='fadeIn'
        animationOut='fadeOut'
        backdropTransitionOutTiming={0}
        onModalHide={this.props.onModalHidden}
      >
        <View style={styles.container}>
          {this.props.children}
          <Span style={styles.message}>{this.props.message}</Span>

          <MainButton label={this.props.confirmText} onPress={this.props.onConfirm} />
          <SecondaryButton label={this.props.cancelText} onPress={this.props.onCancel} />
        </View>
      </Modal>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
    padding: 20,
    borderRadius: 10,
    backgroundColor: '#fff'
  },
  message: {
    marginVertical: 30,
    fontSize: 20,
    textAlign: 'center'
  }
});