import React, { Component } from 'react';
import { View, Image, StyleSheet, Platform } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import Modal from 'react-native-modal';

import { color } from 'src/theme';

import { MainButton, SecondaryButton, ModalButtonIOS } from '../buttons';
import Span from '../Span';

const icons = {
  calendar: require('assets/images/icons/calendar.png'),
  camera: require('assets/images/icons/camera.png'),
  notification: require('assets/images/icons/notification.png'),
  library: require('assets/images/icons/library.png'),
  warning: require('assets/images/icons/warning.png'),
  error: require('assets/images/icons/error.png'),
};

type Params = {
  title?: string;
  icon?: string;
  message: string;

  closable?: boolean;
  onClosePress?(): void;
  closeLabel: string;
  onConfirmPress?(): void;
  confirmLabel?: string;
}

export type Props = {
  navigation: NavigationStackProp;
}

export default class ModalScreen extends Component<Props> {
  render() {
    const params = this.props.navigation.state.params as Params;
    if (!params.message) {
      return null;
    }

    return (
      <Modal
        isVisible
        animationIn='fadeIn'
        animationOut='fadeOut'
        backdropTransitionOutTiming={0}
      >
        <View style={styles.modalContainer}>
          {params.title && <Span style={styles.title}>{params.title}</Span>}
          {params.icon && <Image source={icons[params.icon]} style={styles.icon} />}
          <Span style={styles.message}>{params.message}</Span>

          {this.renderButtons()}
        </View>
      </Modal>
    );
  }

  renderButtons() {
    const params = this.props.navigation.state.params as Params;
    const { closable = true, closeLabel, onConfirmPress, confirmLabel } = params;

    if (Platform.OS === 'ios') {
      return (
        <View style={styles.buttons}>
          {closable ? (
            <ModalButtonIOS label={closeLabel} onPress={this.closeModal} />
          ) : null}
          {onConfirmPress && (
            <ModalButtonIOS label={confirmLabel} onPress={() => this.closeModal(true)} style={styles.secondButton} />
          )}
        </View>
      );
    } else {
      return (
        <View style={styles.buttons}>
          {onConfirmPress && <MainButton thin label={confirmLabel} onPress={() => this.closeModal(true)} />}
          {closable ? <SecondaryButton thin label={closeLabel} onPress={this.closeModal} /> : null}
        </View>
      );
    }
  }

  closeModal = (confirmed?: boolean) => {
    const { onConfirmPress, onClosePress } = this.props.navigation.state.params;
    
    this.props.navigation.goBack();
    if (confirmed && onConfirmPress) {
      onConfirmPress();
    } else if (onClosePress) {
      onClosePress();
    }
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    // height: 200,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    marginHorizontal: 20,
    paddingTop: 32,
    borderRadius: Platform.OS === 'ios' ? 12 : 4,
    backgroundColor: '#fff'
  },
  title: {
    marginBottom: 12,
    fontSize: 24,
    textAlign: 'center'
  },
  icon: {
    marginTop: 8,
    marginBottom: 16,
    width: 100,
    height: 100,
    resizeMode: 'contain'
  },
  message: {
    marginVertical: 12,
    paddingHorizontal: 16,
    fontSize: 16,
    textAlign: 'center',
    color: color.grey
  },
  buttons: {
    flexDirection: Platform.OS === 'ios' ? 'row' : 'column',
    alignSelf: 'stretch',
    marginTop: 20,
    marginBottom: Platform.OS === 'ios' ? 0 : 12,
    marginHorizontal: Platform.OS === 'ios' ? 0 : 12
  },
  secondButton: {
    borderLeftWidth: StyleSheet.hairlineWidth
  }
})