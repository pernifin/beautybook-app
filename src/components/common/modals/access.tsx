import { Platform } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { PERMISSIONS, RESULTS, check, request, checkNotifications } from 'react-native-permissions';
import PushNotification from 'react-native-push-notification';

const CAMERA_PERMISSION = Platform.select({
  ios: PERMISSIONS.IOS.CAMERA,
  android: PERMISSIONS.ANDROID.CAMERA
});

const LIBRARY_PERMISSION = Platform.select({
  ios: PERMISSIONS.IOS.PHOTO_LIBRARY,
  android: PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE
});

export async function CameraAccess(navigation: NavigationStackProp): Promise<boolean> {
  const status = await check(CAMERA_PERMISSION);

  if (status === RESULTS.DENIED) {
    return new Promise(async resolve => {
      navigation.navigate('Modal', {
        title: '#access.cameraTitle',
        message: '#access.cameraMessage',
        icon: 'camera',
        closeLabel: '#common.later',
        confirmLabel: '#common.yes',
        onConfirmPress: async () => resolve(await request(CAMERA_PERMISSION) === RESULTS.GRANTED)
      });
    });
  }

  return status === RESULTS.GRANTED;
}

export async function LibraryAccess(navigation: NavigationStackProp): Promise<boolean> {
  const status = await check(LIBRARY_PERMISSION);

  if (status === RESULTS.DENIED) {
    return new Promise(async resolve => {
      navigation.navigate('Modal', {
        title: '#access.libraryTitle',
        message: '#access.libraryMessage',
        icon: 'library',
        closeLabel: '#common.later',
        confirmLabel: '#common.yes',
        onConfirmPress: async () => resolve(await request(LIBRARY_PERMISSION) === RESULTS.GRANTED)
      });
    });
  }

  return status === RESULTS.GRANTED;
}

export async function NotificationAccess(navigation: NavigationStackProp, message: string): Promise<boolean> {
  const { status } = await checkNotifications();

  if (status === RESULTS.DENIED) {
    return new Promise(async resolve => {
      navigation.navigate('Modal', {
        title: '#access.notificationTitle',
        message: message,
        icon: 'notification',
        closeLabel: '#common.later',
        confirmLabel: '#common.yes',
        onConfirmPress: () => 
          PushNotification.requestPermissions()
            .then(() => resolve(true))
            .catch(() => resolve(false))
      });
    });
  }

  return status === RESULTS.GRANTED;
}