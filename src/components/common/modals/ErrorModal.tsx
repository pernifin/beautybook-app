import { NavigationStackProp } from 'react-navigation-stack';

export default async function ErrorModal(navigation: NavigationStackProp, message: string) {
  return new Promise(async resolve => {
    navigation.navigate('Modal', {
      title: '#error.title',
      message: message,
      icon: 'error',
      closeLabel: '#common.close'
    });
  });
}
