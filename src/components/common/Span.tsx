import React, { ReactElement } from 'react';
import { Text, TextProps, StyleProp, TextStyle, StyleSheet } from 'react-native';

import t from 'src/Lang';
import { font } from 'src/theme';

export type Props = TextProps & {
  raw?: boolean;
  // weight?: '300' | '400' | '500' | '600';
  style?: StyleProp<TextStyle>;
  children: any;
}

export default function Span(origProps: Props) {
  const { raw, /* weight = '400', */ style, children, ...props } = origProps;
  const fullStyle = StyleSheet.flatten([styles.text, /* font(+weight), */ style]);

  const content = raw
    ? children
    : children instanceof Object && children.key || typeof children === 'string'
      ? t(children)
      : children;

  return <Text style={fullStyle} {...props}>{content}</Text>;
}

export const styles = StyleSheet.create({
  text: {
    color: '#000'
  }
});