import React, { Component } from 'react';
import {
  ScrollView,
  TextInput,
  UIManager,
  Keyboard,
  Dimensions,
  findNodeHandle,
  KeyboardEvent,
  ScrollViewProps
} from 'react-native';

export type Props = ScrollViewProps & {
  extraInset?: number;
}

export type State = {
  inset: number;
}

type MeasureResult = {
  x: number;
  y: number;
  width: number;
  height: number;
}

export default class KeyboardAvoidScrollView extends Component<Props, State> {
  scrollRef = React.createRef<ScrollView>();
  scrollNode = 0;

  keyboardShowEvent = null;
  keyboardHideEvent = null;

  scrollHeight = 0;
  contentHeight = 0;
  scrollOffset = 0;
  keyboardY = Dimensions.get('window').height;

  static defaultProps = {
    extraInset: 20
  }

  readonly state = {
    inset: 0
  }

  componentDidMount() {
    this.scrollNode = findNodeHandle(this.scrollRef.current);

    this.keyboardShowEvent = Keyboard.addListener('keyboardWillShow', this.onKeyboardShow);
    this.keyboardHideEvent = Keyboard.addListener('keyboardWillHide', this.onKeyboardHide);
  }

  componentWillUnmount() {
    this.keyboardShowEvent && this.keyboardShowEvent.remove();
    this.keyboardHideEvent && this.keyboardHideEvent.remove();
  }

  onKeyboardShow = (event: KeyboardEvent) => {
    this.keyboardY = event.endCoordinates.screenY;

    this.updateInset();
    this.scrollToFocused();
  }

  onKeyboardHide = (event: KeyboardEvent) => {
    this.keyboardY = event.endCoordinates.screenY;

    this.updateInset();
    this.scrollRef.current.scrollTo({ x: 0, y: Math.min(this.scrollOffset, this.contentHeight - this.scrollHeight) });
  }

  async updateInset() {
    const { y, height } = await this.measureNode(this.scrollNode);

    this.scrollHeight = height;

    const inset = Math.max(0, height + y - this.keyboardY);
    if (this.state.inset !== inset) {
      this.setState({ inset: inset });
    }
  }

  async scrollToFocused() {
    const focusedField = TextInput.State.currentlyFocusedField();
    if (!focusedField) {
      return;
    }

    const isDescendant = await this.isDescendant(this.scrollNode, focusedField);
    if (isDescendant) {
      const { y: scrollY, height: scrollHeight } = await this.measureNode(this.scrollNode);
      const { y: fieldY, height: fieldHeight } = await this.measureNode(focusedField);
      const cutLine = Math.min(scrollY + scrollHeight, this.keyboardY);
      const shift = Math.max(0, fieldY + fieldHeight - cutLine + this.props.extraInset);
      
      this.scrollRef.current.scrollTo({ x: 0, y: shift + this.scrollOffset });
    }
  }

  measureNode(nodeId: number) {
    return new Promise<MeasureResult>(resolve => {
      UIManager.measureInWindow(nodeId, (x, y, width, height) => {
        resolve({ x, y, width, height });
      });
    });
  }

  isDescendant(parentNodeId: number, childNodeId: number) {
    return new Promise<boolean>(resolve => {
      UIManager.viewIsDescendantOf(childNodeId, parentNodeId, isDescendant => {
        resolve(isDescendant);
      });
    });
  }

  onScroll = ({ nativeEvent }) => {
    this.scrollOffset = nativeEvent.contentOffset.y;
  }

  onContentSizeChange = (contentWidth, contentHeight) => {
    this.contentHeight = contentHeight;
    this.scrollToFocused();
  }

  render() {
    const { extraInset, children, ...props } = this.props;
    return (
      <ScrollView
        ref={this.scrollRef}
        onScroll={this.onScroll}
        onContentSizeChange={this.onContentSizeChange}
        scrollEventThrottle={16}
        automaticallyAdjustContentInsets={false}
        keyboardShouldPersistTaps='handled'
        contentInset={{ bottom: this.state.inset }}
        {...props}
      >
        {children}
      </ScrollView>
    )
  }
}
