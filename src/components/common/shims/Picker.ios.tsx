import React, { Component } from 'react';
import { PickerIOS, StyleProp, TextStyle, ViewStyle } from 'react-native';
import { range, flatten } from 'lodash';

type Item<T> = {
  label: string;
  value: T;
}

export type Props<T> = {
  data: Item<T>[];
  value?: T;
  onValueChange?: (value: T) => void;
  cyclic?: boolean;
  style?: StyleProp<ViewStyle>;
  itemStyle?: StyleProp<TextStyle>;
}

export default class Picker<T> extends Component<Props<T>> {
  static defaultProps = {
    data: [{ label: '', value: '' }],
    onValueChange: () => null
  }

  constructor(props: Props<T>) {
    super(props);

    this.tail = props.cyclic ? Math.round(200 / props.data.length) : 1;
    this.head = props.cyclic ? -this.tail : 0;

    this.values = flatten(range(this.head, this.tail).map(i => {
      return props.data.map((item, index) => ({
        label: item.label,
        value: i * props.data.length + index
      }));
    }));
  }

  head = 0;
  tail = 0;
  shift = 0;
  values: Item<number>[];

  onValueChange = (value: number) => {
    const length = this.props.data.length;
    const position = value % length;
    const dataValue = this.props.data[position < 0 ? position + length : position].value;

    this.shift = Math.floor(value / length);

    if (this.shift < this.head / 2 || this.shift > this.tail / 2) {
      this.head = this.shift < this.head / 2 ? this.head * 2 : this.head;
      this.tail = this.shift > this.tail / 2 ? this.tail * 2 : this.tail;

      this.values = flatten(range(this.head, this.tail).map(i => {
        return this.props.data.map((item, index) => ({
          label: item.label,
          value: i * length + index
        }));
      }));
    }

    this.props.onValueChange(dataValue);
  }

  render() {
    const { data, value, style, itemStyle } = this.props;
    const index = data.findIndex(item => item.value === value) + data.length * this.shift;

    return (
      <PickerIOS
        selectedValue={index}
        onValueChange={this.onValueChange}
        style={style}
        itemStyle={itemStyle}
      >
        {this.values.map(item => (
          <PickerIOS.Item key={item.value} label={item.label} value={item.value} />
        ))}
      </PickerIOS>
    );
  }
}
