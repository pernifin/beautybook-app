import React from 'react';
import {
  View, TouchableOpacity, TouchableNativeFeedback, TouchableNativeFeedbackProps, TouchableOpacityProps, ViewStyle, Platform, StyleSheet
} from 'react-native';

import { color } from 'src/theme';

export type Props = (TouchableNativeFeedbackProps | TouchableOpacityProps) & {
  borderless?: boolean;
  style?: ViewStyle;
  children: any;
};

export default function Touchable(props: Props) {
  const { style = {}, borderless, children, ...allProps } = props;
  const styles = StyleSheet.flatten([
    { overflow: Platform.OS === 'android' && !borderless ? 'hidden' : 'visible' },
    style,
  ]);

  const inner = (
    <View style={styles}>
      {children}
    </View>
  );

  if (Platform.OS === 'android') {
    const background = TouchableNativeFeedback.Ripple(color.accentAlpha(0.2), borderless);
    return (
      <TouchableNativeFeedback background={background} useForeground {...allProps}>{inner}</TouchableNativeFeedback>
    );
  } else {
    return (
      <TouchableOpacity activeOpacity={0.6} {...allProps}>{inner}</TouchableOpacity>
    );
  }
}
