import React, { Component } from 'react';
import { View, TextStyle, ViewStyle, PixelRatio, StyleSheet } from 'react-native';
import { WheelPicker } from '@delightfulstudio/react-native-wheel-picker-android';
import { memoize } from 'lodash';

type Item = {
  label: string;
  value: number | string;
}

export type Props = {
  data: Item[];
  value?: string | number;
  onValueChange?: (value: string | number) => void;
  cyclic?: boolean;
  style?: ViewStyle;
  itemStyle?: TextStyle;
}

export default class Picker extends Component<Props> {
  static defaultProps = {
    data: [{ label: '', value: '' }],
    onValueChange: () => null,
    style: {},
    itemStyle: {}
  }

  isCurved = false;
  getListData = memoize((data: Item[]) => data.map(item => item.label));

  mapStyleToProps = (viewStyle: ViewStyle, itemStyle: TextStyle) => {
    return {
      backgroundColor: viewStyle.backgroundColor,
      itemTextFontFamily: itemStyle.fontFamily,
      itemTextSize: PixelRatio.getPixelSizeForLayoutSize(itemStyle.fontSize || 20),
      itemTextColor: itemStyle.color || '#555555',
      itemTextAlign: itemStyle.textAlign,
      selectedItemTextColor: '#333333'
    };
  }

  onItemSelected = ({ position }) => {
    this.props.onValueChange(this.props.data[position].value);
  }

  render() {
    const { data, value } = this.props;
    const selectedIndex = data.findIndex(item => item.value === value);

    return (
      <View style={StyleSheet.flatten(this.props.style)}>
        <WheelPicker
          data={this.getListData(data)}
          selectedItemPosition={selectedIndex}
          onItemSelected={this.onItemSelected}
          isCyclic={this.props.cyclic}
          visibleItemCount={this.isCurved ? 7 : 5}
          isCurved={this.isCurved}
          isAtmospheric
          renderIndicator={!this.isCurved}
          indicatorColor='#e0e0e0'
          indicatorSize={2}
          style={styles.wheel}
          {...this.mapStyleToProps(this.props.style, this.props.itemStyle)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wheel: {
    width: '100%',
    height: 180
  }
});