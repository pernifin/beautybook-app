import React from 'react';
import { Keyboard, KeyboardEventName, Platform } from 'react-native';

const keyboardShow = Platform.select<KeyboardEventName>({
  ios: 'keyboardWillShow',
  android: 'keyboardDidShow'
});

const keyboardHide = Platform.select<KeyboardEventName>({
  ios: 'keyboardWillHide',
  android: 'keyboardDidHide'
});

export function avoidKeyboard(Component) {
  return function (props) {
    const [visible, setVisible] = React.useState(true);

    React.useEffect(() => {
      this.keyboardShowEvent = Keyboard.addListener(keyboardShow, () => setVisible(false));
      this.keyboardHideEvent = Keyboard.addListener(keyboardHide, () => setVisible(true));

      return () => {
        this.keyboardShowEvent.remove();
        this.keyboardHideEvent.remove();
      };
    }, [true]);

    return visible ? <Component {...props} /> : null;
  }
}

export default function KeyboardAvoidContainer(props) {
  const [visible, setVisible] = React.useState(true);

  React.useEffect(() => {
    this.keyboardShowEvent = Keyboard.addListener(keyboardShow, () => setVisible(false));
    this.keyboardHideEvent = Keyboard.addListener(keyboardHide, () => setVisible(true));

    return () => {
      this.keyboardShowEvent.remove();
      this.keyboardHideEvent.remove();
    };
  }, [true]);

  return visible ? props.children : null;
}