import React, { ReactNode } from 'react';
import { ViewStyle, StyleProp, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { color } from 'src/theme';

export type Props = {
  active?: boolean;
  colors?: string[];
  style?: StyleProp<ViewStyle>;
  children?: ReactNode | ReactNode[];
}

export default function Gradient(props: Props) {
  const active = 'active' in props ? props.active : true;
  const colors = props.colors || (active ? color.primaryRange : ['transparent', 'transparent']);
  const styles = StyleSheet.flatten(props.style);

  return (
    <LinearGradient
      colors={colors}
      start={{ x: 0, y: 1 }}
      end={{ x: 1, y: 0 }}
      style={styles}
    >
      {props.children}
    </LinearGradient>
  )
}