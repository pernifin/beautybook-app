import React, { Component } from 'react';
import { View, TouchableOpacity, Animated, StyleSheet, ViewStyle, Platform } from 'react-native';

import { color } from 'src/theme';
import Gradient from './Gradient';
import Span from '../Span';

export type Props = {
  tabs: string[];
  activeIndex: number;
  enabled?: boolean;
  onTabChange(index: number): void;
  style?: ViewStyle;
}

export default class TabStrip extends Component<Props> {
  tabLayouts = [];
  activeLeft: Animated.Value;

  constructor(props: Props) {
    super(props);

    this.activeLeft = new Animated.Value(props.activeIndex);
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.activeIndex !== this.props.activeIndex) {
      Animated.timing(this.activeLeft, {
        duration: Platform.OS === 'ios' ? 300 : 200,
        toValue: this.props.activeIndex
      }).start();
    }
  }

  render() {
    const { tabs, activeIndex, enabled = true, onTabChange } = this.props;
    const width = `${100 / tabs.length}%`;
    const left = this.activeLeft.interpolate({
      inputRange: [0, 1],
      outputRange: ['0%', width]
    });

    return (
      <View style={[styles.tabs, this.props.style, !enabled && styles.tabsDisabled]}>
        <Animated.View style={[styles.activeTab, { width, left }]}>
          <Gradient style={styles.activeTabInner} />
        </Animated.View>

        {tabs.map((tab, i) => (
          <TouchableOpacity
            key={tab}
            disabled={!enabled}
            activeOpacity={activeIndex === i ? 0.7 : 0.5}
            onPress={() => enabled && onTabChange(i)}
            style={styles.tab}
          >
            <Span numberOfLines={1} style={[styles.tabLabel, activeIndex === i && styles.tabLabelActive]}>{tab}</Span>
          </TouchableOpacity>
        ))}
      </View>
    )
  }
}

export const styles = StyleSheet.create({
  tabs: {
    flexDirection: 'row',
    marginHorizontal: 16,
    marginBottom: 10,
    padding: 5,
    height: 50,
    borderRadius: 100,
    backgroundColor: color.accentAlpha(0.1)
  },
  tabsDisabled: {
    opacity: 0.5
  },
  activeTab: {
    position: 'absolute',
    overflow: 'hidden',
    top: 0,
    marginTop: 5,
    marginLeft: 5,
    height: '100%',
    borderRadius: 50
  },
  activeTabInner: {
    width: '100%',
    height: '100%'
  },
  tab: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 5,
    borderRadius: 50
  },
  tabLabel: {
    textAlign: 'center',
    fontSize: 12,
    lineHeight: 20,
    fontWeight: '600'
  },
  tabLabelActive: {
    color: '#fff'
  }
});
