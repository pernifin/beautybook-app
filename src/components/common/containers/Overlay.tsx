import React, { ReactElement } from 'react';
import { View, StyleSheet } from 'react-native';

export type Props = {
  enabled: boolean;
  activeOpacity?: number;
  children?: ReactElement;
}

export default function Overlay(props: Props) {
  return (
    <View>
      {props.children}
      <View style={[
        styles.overlay,
        { height: props.enabled ? '100%' : 0 },
        { opacity: props.activeOpacity || 0.7 }
      ]} />
    </View>
  );
}

export const styles = StyleSheet.create({
  overlay: {
    position: 'absolute',
    width: '100%',
    backgroundColor: '#fff'
  }
});