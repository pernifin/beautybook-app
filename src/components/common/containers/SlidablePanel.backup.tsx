import React, { Component, ReactElement } from 'react';
import { View, ScrollView, Animated, PanResponder, StyleSheet } from 'react-native';
import { PanGestureHandler, State as HandlerState } from 'react-native-gesture-handler';
import reactotron from 'reactotron-react-native';

export type Props = {
  expanded?: boolean;
  fixed?: boolean;
  range: { top: number, bottom: number };
  onToggle?: (state: boolean) => void;
  header?: ReactElement;
}

export type State = {
  shift: Animated.Value;
  position: Animated.Value;
  scrollPosition: Animated.Value;
  scrollable: boolean;
}

export default class SlidablePanel extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    // this.onLayout = this.onLayout.bind(this);
    this.onPanStateChange = this.onPanStateChange.bind(this);

    this.state = {
      shift: new Animated.Value(0),
      position: new Animated.Value(props.expanded ? props.range.top : props.range.bottom),
      scrollPosition: new Animated.Value(0),
      scrollable: false
    };

    this.panResponder = PanResponder.create({
      onMoveShouldSetPanResponder: this.onMoveShouldSetPanResponder,
      onPanResponderGrant: this.onPanResponderGrant,
      onPanResponderMove: Animated.event([null, { dy: this.state.shift }]),
      onPanResponderRelease: this.onPanResponderRelease,
      onPanResponderTerminate: this.onPanResponderTerminate,
      onPanResponderTerminationRequest: () => true,
      onShouldBlockNativeResponder: () => false
    });

    // this.state.scrollPosition.addListener(({ value }) => {
    //   reactotron.log(`scrollPosition: ${value}`);
    // });
  }

  panResponder = null;
  bottom = 35;
  moveThreshold = 40;
  bounce = 10;

  onMoveShouldSetPanResponder = (evt, gestureState) => {
    const should = this.withinRange(gestureState.dy) && Math.abs(gestureState.dy) >= 10 && Math.abs(gestureState.dx) < 10;

    //reactotron.log(should);

    return should;
  }

  onPanResponderGrant = () => {
    this.state.position.stopAnimation();
    //this.setState({ scrollable: false });
  }

  onPanResponderRelease = () => {
    const shiftValue = this.state.shift['__getValue']();
    const positionValue = this.state.position['__getValue']();
    let total = positionValue - shiftValue;
    const { top, bottom } = this.props.range;

    if (total > top) {
      total = top;
    } else if (total < bottom) {
      total = bottom;
    }

    this.state.shift.setValue(0);
    this.state.position.setValue(total);

    const toValue = (Math.abs(shiftValue) > this.moveThreshold) !== (shiftValue > 0) ? top : bottom;

    if (top > total && total > bottom) {
      Animated.timing(this.state.position, {
        duration: 150,
        toValue
      }).start(() => {
        this.setState({ scrollable: toValue === top });
      });
    } else {
      this.setState({ scrollable: total === top });
    }

    if (this.props.onToggle) {
      this.props.onToggle(toValue === top);
    }

    //reactotron.log(`onPanResponderRelease; shiftValue: ${shiftValue}, total: ${total}`);
  }

  onPanResponderTerminate = () => {
    //reactotron.log(`onPanResponderTerminate`);
    this.onPanResponderRelease();
  }

  withinRange = (dy: number) => {
    const { top, bottom } = this.props.range;
    const position = this.state.position['__getValue']();
    const scroll = this.state.scrollPosition['__getValue']();

    //reactotron.log(position, dy, scroll);

    return dy > 0 ? position > bottom && scroll === 0 : position < top;
  }

  componentDidUpdate(prevProps: Props, prevState: State) {
    if (prevProps.expanded !== this.props.expanded) {
      Animated.timing(this.state.position, {
        duration: 150,
        toValue: this.props.expanded ? this.props.range.top : this.props.range.bottom
      }).start();
    }

    if (prevState.scrollable !== this.state.scrollable) {
      //reactotron.log('scrollable: ' + this.state.scrollable);
    }
  }

  getViewTranslation() {
    // const config = {
    //   inputRange: [this.bottom - this.bounce, this.state.top + this.bounce],
    //   outputRange: [-this.bottom + this.bounce, -this.state.top - this.bounce],
    //   extrapolate: 'clamp' as 'clamp'
    // };
    const { top, bottom } = this.props.range;
    const config = {
      inputRange: [bottom, top],
      //outputRange: [bottom, top],
      outputRange: [-bottom, -top],
      extrapolate: 'clamp' as 'clamp'
    };

    return Animated.subtract(this.state.position, this.state.shift).interpolate(config);
    //return this.state.position.interpolate(config);
  }

  // onLayout({ nativeEvent }) {
  //   const top = Math.floor(nativeEvent.layout.height) - this.bounce;

  //   this.setState({ top });
  //   if (this.props.expanded) {
  //     this.state.position.setValue(top);
  //   }
  // }

  onPanStateChange({ nativeEvent }) {
    if (nativeEvent.state === HandlerState.ACTIVE) {
      this.state.position.stopAnimation();
    }
    
    if (nativeEvent.state === HandlerState.END) {
      const shiftValue = this.state.shift['__getValue']();
      const positionValue = this.state.position['__getValue']();
      let total = positionValue - shiftValue;
      const { top, bottom } = this.props.range;

      if (total > top) {
        total = top;
      } else if (total < bottom) {
        total = bottom;
      }

      this.state.shift.setValue(0);
      this.state.position.setValue(total);

      const toValue = (Math.abs(shiftValue) > this.moveThreshold) !== (shiftValue > 0) ? top : bottom;

      if (!this.props.fixed && top > total && total > bottom) {
        Animated.timing(this.state.position, {
          duration: 150,
          toValue
        }).start();
      }

      if (this.props.onToggle) {
        this.props.onToggle(toValue === top);
      }
    }
  }

  render() {
    const scrollEvent = Animated.event(
      [{ nativeEvent: { contentOffset: { y: this.state.scrollPosition } } }]
    );

    return (
      // <PanGestureHandler
      //   failOffsetX={[-10, 10]}
      //   activeOffsetY={[-10, 10]}
      //   onGestureEvent={event}
      //   onHandlerStateChange={this.onPanStateChange}
      // >
        <Animated.View
          {...this.panResponder.panHandlers}
          style={[
            styles.contaner,
            // { height: this.getViewTranslation() }
            { height: this.props.range.top },
            { transform: [{ translateY: this.getViewTranslation() }] }
          ]}
        >
          <View style={styles.bar} />
          {this.props.header}
          
          <ScrollView
            showsVerticalScrollIndicator={false}
            onScroll={scrollEvent}
            scrollEventThrottle={16}
            alwaysBounceVertical={false}
            scrollEnabled={this.state.scrollable}
            overScrollMode='never'
            //style={{ borderWidth: 1, borderColor: 'red' }}
          >
            {this.props.children}
          </ScrollView>
          
        </Animated.View>
      // </PanGestureHandler>
    )
  }
}

export const styles = StyleSheet.create({
  contaner: {
    position: 'absolute',
    //overflow: 'hidden',
    top: '100%',
    //top: 0,
    //bottom: 0,
    left: 16,
    right: 16,
    //paddingBottom: 10,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderWidth: StyleSheet.hairlineWidth,
    borderBottomWidth: 0,
    borderColor: '#ddd',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowRadius: 5,
    shadowOpacity: 0.1,
    elevation: 12
  },
  bar: {
    marginVertical: 18,
    alignSelf: 'center',
    width: '15%',
    height: 4,
    borderRadius: 2,
    backgroundColor: '#ddd'
  }
});
