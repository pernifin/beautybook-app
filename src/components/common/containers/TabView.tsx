import React, { Component, ReactNode } from 'react';
import { SegmentedControlIOS, TouchableOpacity, Text, ScrollView, View, StyleSheet, ViewStyle } from 'react-native';

import TabStrip from './TabStrip';

export type Props = {
  tabs: string[];
  onTabChange?: (tabIndex: number) => void;
  tabIndex?: number;
  hideTabs?: boolean;
  enabled?: boolean;
  style?: ViewStyle;
  tabsStyle?: ViewStyle;
}

export type State = {
  tabIndex: number;
  tabWidth: 0
}

export default class TabView extends Component<Props, State> {
  static defaultProps = {
    enabled: true,
    onTabChange: () => null,
    tabIndex: 0
  }

  sectionList = React.createRef<ScrollView>();

  constructor(props) {
    super(props);
    this.onLayout = this.onLayout.bind(this);
    this.onMomentumScrollEnd = this.onMomentumScrollEnd.bind(this);

    this.state = {
      tabIndex: props.tabIndex,
      tabWidth: 0
    };
  }

  componentDidUpdate(prevProps: Props, prevState: State) {
    if (prevState.tabIndex !== this.state.tabIndex) {
      this.sectionList.current.scrollTo({ x: this.state.tabWidth * this.state.tabIndex });
      this.props.onTabChange && this.props.onTabChange(this.state.tabIndex);
    }

    if (prevProps.tabIndex !== this.props.tabIndex) {
      this.setState({ tabIndex: this.props.tabIndex });
    }
  }

  onLayout({ nativeEvent }) {
    const { width } = nativeEvent.layout;
    this.setState({ tabWidth: width });

    if (this.state.tabIndex !== 0) {
      requestAnimationFrame(() => {
        this.sectionList.current.scrollTo({ x: width * this.state.tabIndex, animated: false });
      });
    }
  }

  onMomentumScrollEnd({ nativeEvent }) {
    this.setState({ tabIndex: Math.round(nativeEvent.contentOffset.x / this.state.tabWidth) });
  }

  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        {!this.props.hideTabs && (
          <TabStrip
            tabs={this.props.tabs}
            activeIndex={this.state.tabIndex}
            enabled={this.props.enabled}
            onTabChange={tabIndex => this.setState({ tabIndex })}
            style={this.props.tabsStyle}
          />
        )}

        <ScrollView
          ref={this.sectionList}
          horizontal
          pagingEnabled
          bounces={false}
          scrollEnabled={this.props.enabled}
          showsHorizontalScrollIndicator={false}
          keyboardShouldPersistTaps='handled'
          keyboardDismissMode='on-drag'
          overScrollMode='never'
          onLayout={this.onLayout}
          onMomentumScrollEnd={this.onMomentumScrollEnd}
          style={styles.content}
        >
          {this.renderChildren()}
        </ScrollView>
      </View>
    );
  }

  renderChildren() {
    const children = Array.isArray(this.props.children) ? this.props.children : [this.props.children];

    return children.map((child, i) => (
      <View key={this.props.tabs[i]} style={{ width: this.state.tabWidth }}>
        {child}
      </View>
    ));
  }
}

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  tabs: {
    flexDirection: 'row',
    marginHorizontal: 16,
    marginBottom: 10,
    padding: 5,
    borderRadius: 50,
    backgroundColor: '#eff2fd'
  },
  tab: {
    flex: 1,
    alignItems: 'center',
    height: 40,
    paddingVertical: 20,
    borderRadius: 50
  },
  tabActive: {
    backgroundColor: '#F17A54'
  },
  tabLabel: {
    fontSize: 12,
    lineHeight: 20,
    fontWeight: '600'
  },
  tabsDisabled: {

  },
  content: {
    flex: 1,
    
  }
});
