import React, { Component } from 'react';
import { View, Animated, StyleSheet } from 'react-native';
import { PanGestureHandler, State as HandlerState } from 'react-native-gesture-handler';

export type Props = {
  expanded?: boolean;
  fixed?: boolean;
  range: { top: number, bottom: number };
  onToggle?: (state: boolean) => void;
}

export type State = {
  shift: Animated.Value;
  position: Animated.Value;
}

export default class SlidablePanel extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    // this.onLayout = this.onLayout.bind(this);
    this.onPanStateChange = this.onPanStateChange.bind(this);

    this.state = {
      shift: new Animated.Value(0),
      position: new Animated.Value(props.expanded ? props.range.top : props.range.bottom)
    };
  }

  bottom = 35;
  moveThreshold = 40;
  bounce = 10;

  componentDidUpdate(prevProps: Props) {
    if (prevProps.expanded !== this.props.expanded) {
      Animated.timing(this.state.position, {
        duration: 150,
        toValue: this.props.expanded ? this.props.range.top : this.props.range.bottom
      }).start();
    }
  }

  getViewTranslation() {
    // const config = {
    //   inputRange: [this.bottom - this.bounce, this.state.top + this.bounce],
    //   outputRange: [-this.bottom + this.bounce, -this.state.top - this.bounce],
    //   extrapolate: 'clamp' as 'clamp'
    // };
    const { top, bottom } = this.props.range;
    const config = {
      inputRange: [bottom, top],
      //outputRange: [bottom, top],
      outputRange: [-bottom, -top],
      extrapolate: 'clamp' as 'clamp'
    };

    return Animated.subtract(this.state.position, this.state.shift).interpolate(config);
    //return this.state.position.interpolate(config);
  }

  // onLayout({ nativeEvent }) {
  //   const top = Math.floor(nativeEvent.layout.height) - this.bounce;

  //   this.setState({ top });
  //   if (this.props.expanded) {
  //     this.state.position.setValue(top);
  //   }
  // }

  onPanStateChange({ nativeEvent }) {
    if (nativeEvent.state === HandlerState.ACTIVE) {
      this.state.position.stopAnimation();
    }
    
    if (nativeEvent.state === HandlerState.END) {
      const shiftValue = this.state.shift['__getValue']();
      const positionValue = this.state.position['__getValue']();
      let total = positionValue - shiftValue;
      const { top, bottom } = this.props.range;

      if (total > top) {
        total = top;
      } else if (total < bottom) {
        total = bottom;
      }

      this.state.shift.setValue(0);
      this.state.position.setValue(total);

      const toValue = (Math.abs(shiftValue) > this.moveThreshold) !== (shiftValue > 0) ? top : bottom;

      if (!this.props.fixed && top > total && total > bottom) {
        Animated.timing(this.state.position, {
          duration: 150,
          toValue
        }).start();
      }

      if (this.props.onToggle) {
        this.props.onToggle(toValue === top);
      }
    }
  }

  render() {
    const event = Animated.event(
      [{ nativeEvent: { translationY: this.state.shift } }]
    );

    return (
      <PanGestureHandler
        failOffsetX={[-10, 10]}
        activeOffsetY={[-10, 10]}
        onGestureEvent={event}
        onHandlerStateChange={this.onPanStateChange}
      >
        <Animated.View
          style={[
            styles.contaner,
            // { height: this.getViewTranslation() }
            { maxHeight: this.props.range.top },
            { transform: [{ translateY: this.getViewTranslation() }] }
          ]}
        >
          <View style={styles.bar}></View>
          {this.props.children}
        </Animated.View>
      </PanGestureHandler>
    )
  }
}

export const styles = StyleSheet.create({
  contaner: {
    position: 'absolute',
    //overflow: 'hidden',
    top: '100%',
    //top: 0,
    //bottom: 0,
    left: 16,
    right: 16,
    //paddingBottom: 10,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderWidth: StyleSheet.hairlineWidth,
    borderBottomWidth: 0,
    borderColor: '#ddd',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowRadius: 5,
    shadowOpacity: 0.1,
    elevation: 12
  },
  bar: {
    marginVertical: 18,
    alignSelf: 'center',
    width: '15%',
    height: 4,
    borderRadius: 2,
    backgroundColor: '#ddd'
  }
});
