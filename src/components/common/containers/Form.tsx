import React from 'react';
import { View, StyleSheet, ViewStyle } from 'react-native';

import LineSeparator from '../LineSeparator';
import Span from '../Span';

export type Props = {
  header?: string;
  footer?: string;
  children?: any;
  style?: ViewStyle;
}

export default function Form(props: Props) {
  const children = React.Children.toArray(props.children);

  return children.length ? (
    <View style={props.style}>
      {props.header && <Span style={styles.text}>{props.header}</Span>}
      <LineSeparator />
      {children.map((child, i) => child ? [
        child,
        <LineSeparator key={i} />
      ] : null)}
      {props.footer && <Span style={styles.text}>{props.footer}</Span>}
    </View>
  ) : null;
}

const styles = StyleSheet.create({
  text: {
    paddingVertical: 5,
    paddingHorizontal: 16,
    fontSize: 12,
    color: '#666'
  }
});