import { createIconSet } from 'react-native-vector-icons';
import glyphs from 'assets/icons/glyphs.json';

const Icon = createIconSet(glyphs, 'IcoBeauty', 'IcoBeauty.ttf');

export default Icon;