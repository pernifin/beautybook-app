import React from 'react';
import { View, Platform, Dimensions, StyleSheet } from 'react-native';
import { BoxShadow, BorderShadow } from 'react-native-shadow';

const { width } = Dimensions.get('screen');
const padding = 36;

export default function Shadow() {
  const shadowOpt = {
    width: width,
    height: 40,
    color: "#3A79C3",
    border: 5,
    opacity: 0.2,
    style: {
      position: 'absolute',
      zIndex: 1,
      left: 0,
      right: 0,
      top: -40
    }
  };

  return Platform.select({
    ios: <View style={styles.shadow} />,
    android: (
      <BoxShadow setting={shadowOpt}>
        <View style={styles.inner} />
      </BoxShadow>
    )
  });
}

export const styles = StyleSheet.create({
  shadow: {
    position: 'absolute',
    zIndex: 100,
    left: 0,
    right: 0,
    top: -40,
    height: 40,
    backgroundColor: '#fff',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowColor: "#3A79C3",
    shadowOpacity: 0.3,
    shadowRadius: 5
  },
  inner: {
    position: 'relative',
    flex: 1,
    backgroundColor: '#fff'
  }
});