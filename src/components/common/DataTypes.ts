export type IDateInterval = {
  date: string;
  timeFrom: number;
  timeTo: number;
}

export type IAppointment = {
  id?: number;
  master?: IProfile;
  client?: IProfile;
  clientName: string;
  clientPhone: string;
  datetime: string;
  services: IService[];
  duration: number;
  price: number;
  note?: string;
  remindIn?: number;
}

export type ITimeBlock = IDateInterval & {
  id?: number;
  note?: string;
  appointment?: IAppointment;
}

export type ICalendarSlot = IInterval & {
  timeFrom: number;
  timeTo: number;
  workload: number;
  records: ITimeBlock[];
}

export type ICalendarDate = {
  loading: boolean;
  workload: number;
  slots: ICalendarSlot[];
}

export type ICalendar = {
  [date: string]: ICalendarDate;
}

export type ICalendarDateMark = {
  selected?: boolean;
  dayoff: boolean;
  dots: number;
  color: string;
}

export type ICalendarMark = {
  [date: string]: ICalendarDateMark;
}

export type IScheduleSettings = {
  recordPeriod: number;
  cancelForbid: number;
}

export type ISchedule = {
  [date: string]: IInterval;
}

export type IInterval = {
  isWorking?: boolean;
  timeFrom?: number;
  timeTo?: number;
}

export type ICategory = {
  id: string;
  title: string;
}

export type IService = {
  id?: number;
  catalogServiceId?: string;
  category?: ICategory;
  categoryId?: string;
  title: string;
  description: string;
  duration: number;
  price: number;
}

export type ICatalogService = {
  id: string;
  categoryId: string;
  title: string;
  description: string;
  duration: number;
  price: number;
}

export type IProfile = {
  id?: string;
  photo: string;
  name: string;
  phone: string;
  address: string;
  specialization: string;
  instagram: string;
  mode?: string;
}

export type IUpload = {
  uri: string;
  name: string;
  type: string;
}