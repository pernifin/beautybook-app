import React, { ReactNode, ReactElement } from 'react';
import {
  View, TouchableOpacity, TouchableNativeFeedback, Platform, StyleSheet, StyleProp, ViewStyle
} from 'react-native';
import { RectButton, BorderlessButton } from 'react-native-gesture-handler';
import ActionButton from 'react-native-action-button';

import t from 'src/Lang';
import { color } from 'src/theme';

import Touchable from './shims/Touchable';
import Gradient from './containers/Gradient';
import Span from './Span';
import Icon from './Icon';

export function HeaderButtonContainer(props) {
  return (
    <View style={styles.headerBtnContainer}>
      {props.children}
    </View>
  );
}

export type HeaderButtonProps = {
  enabled?: boolean;
  label?: string;
  iconName?: string;
  bold?: boolean;
  toggle?: boolean;
  onPress: () => void;
}

export function HeaderButton(props: HeaderButtonProps) {
  const { enabled = true, label, iconName } = props;
  const isToggle = 'toggle' in props;

  const iconColor = !enabled
    ? color.inactive
    : Platform.select({
        ios: isToggle && !props.toggle ? color.accentLight : color.primary,
        android: isToggle && props.toggle ? color.primary : color.white
      });

  return (
    <BorderlessButton
      enabled={enabled}
      onPress={props.onPress}
      style={[styles.headerBtn]}
    >
      <View hitSlop={{ left: 10, right: 10, top: 10, bottom: 10 }}>
        {iconName && (
          <Icon name={iconName} size={Platform.OS === 'ios' ? 20 : 24} color={iconColor} />
        )}
        {label && (
          <Span style={[
            styles.headerBtnText,
            enabled && styles.headerBtnTextEnabled,
            props.bold && styles.headerBoldText
          ]}>
            {label}
          </Span>
        )}
      </View>
    </BorderlessButton>
  );
}

export function SingleHeaderButton(props: HeaderButtonProps) {
  return (
    <HeaderButtonContainer>
      <HeaderButton {...props} />
    </HeaderButtonContainer>
  );
}

export type MainButtonProps = {
  enabled?: boolean;
  short?: boolean;
  thin?: boolean;
  label?: string;
  children?: ReactNode | ReactNode[];
  onPress: () => any;
  style?: StyleProp<ViewStyle>;
}

export function MainButton(props: MainButtonProps) {
  const { enabled = true, short = false, thin = false, label, onPress } = props;
  const Container = enabled ? Gradient : View;

  return (
    <Touchable
      disabled={!enabled}
      onPress={() => onPress()}
      style={[
        styles.mainButton,
        enabled && styles.mainBtnEnabled,
        short && styles.btnShort,
        thin && styles.btnThin,
        props.style
      ]}
    >
      <Container style={styles.mainButtonInner}>
        {props.children || <Span style={[styles.mainBtnText, enabled && styles.mainBtnTextEnabled]}>{label}</Span>}
      </Container>
    </Touchable>
  );
}

export function SecondaryButton(props: MainButtonProps) {
  const { enabled = true, short = false, thin = false, label, onPress } = props;

  return (
    <Touchable
      disabled={!enabled}
      onPress={() => onPress()}
      style={[
        styles.secondaryBtn,
        enabled && styles.secondaryBtnEnabled,
        short && styles.btnShort,
        thin && styles.btnThin,
        props.style
      ]}
    >
      {props.children || <Span style={styles.secondaryBtnText}>{label}</Span>}
    </Touchable>
  );
}

export type FABProps = {
  iconName?: string;
  onPress: () => void;
  children?: ReactElement<FABItemProps>[];
}

export function FAB(props: FABProps) {
  const icon = (
    <Gradient colors={color.primaryRange} style={styles.fabIcon}>
      <Icon name={props.iconName} size={18} color='#FFF' />
    </Gradient>
  );

  const items = React.Children.map(props.children, child => (
    <ActionButton.Item size={40} buttonColor={color.accent} title={t(child.props.label)} onPress={child.props.onPress}>
      <Icon name={child.props.iconName} size={15} color='#FFF' />
    </ActionButton.Item>
  ));

  return (
    <ActionButton
      fixNativeFeedbackRadius
      nativeFeedbackRippleColor={color.accentAlpha(0.2)}
      renderIcon={active => icon}
      degrees={135}
      offsetX={16}
      offsetY={16}
      spacing={16}
      bgColor='#FFFD'
      onPress={props.onPress}
    >
      {items}
    </ActionButton>
  );
}

export type FABItemProps = {
  iconName: string;
  label?: string;
  onPress: () => void;
}

export function FabItem(props: FABItemProps) {
  return (
    <ActionButton.Item size={40} buttonColor={color.accent} title={props.label} onPress={props.onPress}>
      <Icon name={props.iconName} size={18} color='#fff' />
    </ActionButton.Item>
  );
}

FAB.Item = FabItem;

export type ModalButtonIOSProps = {
  label: string;
  onPress: () => void;
  style?: ViewStyle;
}

export function ModalButtonIOS(props: ModalButtonIOSProps) {
  return (
    <View style={[styles.modalButton, props.style]}>
      <TouchableOpacity onPress={() => props.onPress()} style={styles.modalButtonInner}>
        <Span style={styles.modalButtonLabel}>{props.label}</Span>
      </TouchableOpacity>
    </View>
  );
}

export const styles = StyleSheet.create({
  headerBtnContainer: {
    flexDirection: 'row',
    height: 41,
    paddingHorizontal: 5
  },
  headerBtn: {
    paddingHorizontal: 11,
    paddingVertical: Platform.OS === 'ios' ? 10 : 8
  },
  headerBtnText: {
    color: color.inactive,
    fontSize: 17,
    paddingTop: Platform.OS === 'ios' ? 0 : 2
  },
  headerBtnTextEnabled: {
    color: Platform.OS === 'ios' ? color.primary : color.white
  },
  headerBoldText: {
    fontWeight: '600'
  },
  buttonContainer: {
    flex: 1,
  },
  mainButton: {
    //flex: 1,
    justifyContent: 'center',
    alignSelf: 'stretch',
    overflow: 'hidden',
    height: 60,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 8,
    opacity: 0.5
  },
  mainBtnEnabled: {
    opacity: 1
  },
  mainButtonInner: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color.accentLightAlpha(0.3)
  },
  mainBtnText: {
    fontSize: 16,
    fontWeight: '500',
    lineHeight: 20,
    color: color.accentLight
  },
  mainBtnTextEnabled: {
    color: '#fff'
  },
  secondaryBtn: {
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    marginBottom: 8,
    marginHorizontal: 16,
    borderRadius: 8,
    opacity: 0.5
  },
  secondaryBtnEnabled: {
    opacity: 1
  },
  secondaryBtnText: {
    fontSize: 16,
    color: '#000'
  },
  btnShort: {
    minWidth: '50%',
    alignSelf: 'center'
  },
  btnThin: {
    height: 40,
    borderRadius: Platform.OS === 'ios' ? 8 : 4
  },
  fabContainer: {
    position: 'absolute',
    overflow: 'hidden',
    bottom: 16,
    right: 16,
    width: 56,
    flexDirection: 'column-reverse',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  fabIcon: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  fab: {
    width: 56,
    height: 56,
    borderRadius: 28,
    elevation: 6
  },
  fabSmall: {
    width: 40,
    height: 40,
    borderRadius: 16,
    elevation: 6
  },
  modalButton: {
    flex: 1,
    height: 60,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderColor: color.accentAlpha(0.2)
  },
  modalButtonInner: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%'
  },
  modalButtonLabel: {
    fontSize: 16,
    fontWeight: '500'
  }
});