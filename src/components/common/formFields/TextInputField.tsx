import React from 'react';
import { View, TextInput, StyleSheet, Animated, Platform, TouchableHighlight, TextInputProps, TextStyle, StyleProp, ViewStyle } from 'react-native';
import { TextInputMask, TextInputMaskProps } from 'react-native-masked-text';

import { color, font } from '../../../theme';
import t from '../../../Lang';
import Span from '../Span';

export type Props = TextInputProps & Partial<TextInputMaskProps> & {
  label?: string;
  value: string;
  onValueChange: (val: string) => void;
  masked?: boolean;
  touchable?: boolean;
  changeEvent?: 'input' | 'end';
  multiline?: boolean;
  fontSize?: number;
  align?: 'left' | 'center' | 'right';
  prepend?: string | object;
  append?: string | object;
  style?: StyleProp<ViewStyle>;
  inputStyle?: TextStyle;
}

export type State = {
  blockHeight: Animated.Value;
  inputHeight: number;
  value: string;
}

export default class TextInputField extends React.Component<Props, State> {
  static defaultProps = {
    touchable: true,
    changeEvent: 'input',
    fontSize: 16,
    align: 'left'
  };

  constructor(props: Props) {
    super(props);
    this.onFieldTap = this.onFieldTap.bind(this);
    this.adjustHeight = this.adjustHeight.bind(this);

    this.lineHeight = props.fontSize + 4;

    this.state = {
      blockHeight: new Animated.Value(this.padding * 2 + this.lineHeight),
      inputHeight: props.multiline ? this.lineHeight * 2 : this.lineHeight,
      value: props.value
    };
  }

  inputRef: TextInput;
  padding = 20;
  lineHeight;

  componentDidUpdate(prevProps: Props, prevState: State) {
    if (this.state === prevState && this.props.value !== this.state.value) {
      this.setState({ value: this.props.value });
    }
  }

  adjustHeight({ nativeEvent }) {
    const { height } = nativeEvent.contentSize;
    const inputHeight = height + this.lineHeight;

    if (this.props.multiline && this.state.inputHeight !== inputHeight) {
      Animated.timing(this.state.blockHeight, {
        toValue: height + this.padding * 2,
        duration: 200
      }).start();

      this.setState({ inputHeight });
    }
  }

  onFieldTap() {
    this.inputRef.focus();
  }

  onValueChange = (text: string, rawText?: string) => {
    const value = this.props.masked ? rawText : text;

    this.setState({ value });
    if (this.props.changeEvent === 'input') {
      this.props.onValueChange(value);
    }
  }

  onEndEditing = () => {
    if (this.props.changeEvent === 'end') {
      this.props.onValueChange(this.state.value);
    }
  }

  mergeStyles(...base: TextStyle[]) {
    return StyleSheet.flatten([
      base,
      this.props.inputStyle,
      { fontSize: this.props.fontSize, lineHeight: this.props.fontSize + 4 }
    ]);
  }

  render() {
    const { label, prepend, multiline, append, ...props } = this.props;
    
    return (
      <TouchableHighlight
        underlayColor={props.touchable ? color.accent : 'transparent'}
        activeOpacity={0.9}
        onPress={this.onFieldTap}
      >
        <Animated.View
          style={[
            styles.fieldLine,
            props.style,
            { height: multiline ? this.state.blockHeight : 'auto' }
          ]}
        >
          {label && <Span style={styles.label}>{label}</Span>}
          <View style={[styles.inputBlock, { height: this.state.inputHeight }]}>
            {prepend && <Span style={this.mergeStyles(styles.fieldPrepend)}>{prepend}</Span>}

            {this.props.masked ? this.renderInputMask() : this.renderTextInput()}

            {append && <Span style={this.mergeStyles(styles.fieldAppend)}>{append}</Span>}
          </View>
        </Animated.View>
      </TouchableHighlight>
    );
  }

  renderTextInput() {
    const { value, placeholder = '',  ...props } = this.props;

    return (
      <TextInput
        ref={input => this.inputRef = input}
        value={this.state.value}
        onChangeText={this.onValueChange}
        onEndEditing={this.onEndEditing}
        onContentSizeChange={this.adjustHeight}
        placeholder={t(placeholder)}
        placeholderTextColor={color.accentAlpha(0.4)}
        blurOnSubmit={true}
        maxLength={250}
        autoCompleteType='off'
        {...props}
        style={this.mergeStyles(styles.textInput, props.align && { textAlign: props.align })}
      />
    );
  }

  renderInputMask() {
    const { type = 'custom', placeholder = '', ...props } = this.props;
    
    return (
      <TextInputMask
        refInput={input => this.inputRef = input}
        type={type}
        includeRawValueInChangeText
        value={this.state.value}
        onChangeText={this.onValueChange}
        onEndEditing={this.onEndEditing}
        placeholder={t(placeholder)}
        placeholderTextColor={color.accentLight}
        blurOnSubmit={true}
        maxLength={250}
        autoCompleteType='off'
        {...props}
        style={this.mergeStyles(styles.textInput, props.align && { textAlign: props.align })}
      />
    )
  }
}

export const styles = StyleSheet.create({
  fieldLine: {
    flexDirection: 'row',
    paddingHorizontal: 30,
    paddingVertical: 20,
    backgroundColor: '#fff'
  },
  label: {
    flex: 2,
    fontSize: 14,
    lineHeight: 20
  },
  inputBlock: {
    flex: 3,
    flexDirection: 'row',
    alignItems: 'flex-start'
  },
  textInput: {
    flex: 1,
    padding: 0,
    paddingTop: Platform.OS === 'android' ? 2 : 1,
    paddingBottom: 0,
    fontSize: 16,
    lineHeight: 20,
    color: '#000000',
    textAlignVertical: 'top'
  },
  fieldPrepend: {
    marginRight: 5
  },
  fieldAppend: {
    marginLeft: 5
  },
  fieldReadonly: {
    color: '#777',
    fontSize: 13
  }
});