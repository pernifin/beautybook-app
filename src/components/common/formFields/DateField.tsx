import React from 'react';
import { StyleSheet } from 'react-native';
import { range } from 'lodash';
import moment from 'moment';

import t from 'src/Lang';
import { formatDate } from 'src/utils';
import Picker from '../shims/Picker';

export type Props = {
  value: string;
  onValueChange: (val: string) => void;
}

function getDateItems() {
  return range(-30, 31).map(day => {
    const m = moment().add(day, 'd');
    return {
      label: day === 0 ? t('#datetime.today') : formatDate(m),
      value: m.format('YYYY-MM-DD')
    }
  });
}

export default class DateField extends React.Component<Props> {
  dateItems = getDateItems();

  render() {
    const { value, onValueChange } = this.props;

    return (
      <Picker
        data={this.dateItems}
        value={value}
        onValueChange={onValueChange}
        itemStyle={styles.item}
      />
    );
  }
}

export const styles = StyleSheet.create({
  item: {
    fontSize: 18
  }
});