import React, { Component } from 'react';
import { View, StyleSheet, ViewStyle } from 'react-native';
import { range, padStart } from 'lodash';

import { IInterval } from '../DataTypes';
import Picker from '../shims/Picker';

export type Props = {
  interval: IInterval;
  onIntervalChange: (time: IInterval) => void;
  style?: ViewStyle;
}

function getTimeList() {
  return range(0, 49).map(i => ({
    label: `${padStart(String(Math.floor(i % 48 / 2)), 2, '0')}:${i % 2 ? '30' : '00'}`,
    value: i * 30
  }));
}

export default class IntervalField extends Component<Props> {
  timeList = getTimeList()

  constructor(props) {
    super(props);
    this.onIntervalChange = this.onIntervalChange.bind(this);
  }

  onIntervalChange(time: Partial<IInterval>) {
    const { timeFrom, timeTo } = this.props.interval;
    if ('timeFrom' in time) {
      time.timeTo = Math.max(time.timeFrom, timeTo);
    } else {
      time.timeFrom = Math.min(time.timeTo, timeFrom);
    }

    this.props.onIntervalChange({ ...this.props.interval, ...time });
  }

  render() {
    const interval = this.props.interval || { timeFrom: 720, timeTo: 720 };

    return (
      <View style={[styles.container, this.props.style]}>
        {this.renderFakePicker()}
        <Picker
          data={this.timeList}
          value={interval.timeFrom}
          onValueChange={time => this.onIntervalChange({ timeFrom: time })}
          style={styles.picker}
          itemStyle={styles.item}
        />
        <View style={styles.delimiter} />
        <Picker
          data={this.timeList}
          value={interval.timeTo}
          onValueChange={time => this.onIntervalChange({ timeTo: time })}
          style={styles.picker}
          itemStyle={styles.item}
        />
        {this.renderFakePicker()}
      </View>
    );
  }

  renderFakePicker() {
    return (
      <Picker
        style={[styles.picker, { flex: 0.5 }]}
        itemStyle={styles.item}
      />
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 180
  },
  picker: {
    flex: 1
  },
  item: {
    fontSize: 18
  },
  delimiter: {
    position: 'absolute',
    top: '50%',
    left: '46%',
    right: '46%',
    height: 2,
    backgroundColor: '#000'
  }
});