import React, { Component } from 'react';
import { View, TextInput, StyleSheet, Animated, TouchableHighlight, Keyboard, Platform, TextStyle, ViewStyle } from 'react-native';

import { color } from 'src/theme';
import Span from '../Span';

export type Props = {
  label: string;
  value: string;
  align?: 'left' | 'right';
  openHeight?: number;
  style?: ViewStyle;
  labelStyle?: TextStyle;
  valueStyle?: TextStyle;
}

export type State = {
  pickerHeight: Animated.Value;
  expanded: boolean;
}

export default class PickerField extends Component<Props, State> {
  fakeInputRef = React.createRef<TextInput>();
  viewRef = React.createRef<View>();

  constructor(props) {
    super(props);
    this.hidePicker = this.hidePicker.bind(this);
    this.togglePicker = this.togglePicker.bind(this);

    Keyboard.addListener('keyboardWillShow', this.hidePicker);

    this.state = {
      pickerHeight: new Animated.Value(0),
      expanded: false
    };
  }

  componentWillUnmount() {
    Keyboard.removeListener('keyboardWillShow', this.hidePicker);
  }

  hidePicker() {
    if (this.state.expanded) {
      this.togglePicker();
    }
  }

  onPress() {
    this.togglePicker();
  }

  togglePicker() {
    Animated.timing(this.state.pickerHeight, {
      toValue: this.state.expanded ? 0 : this.props.openHeight || 180,
      duration: 300
    }).start(({ finished }) => {
      if (finished) {
        this.setState({ expanded: !this.state.expanded });
      }
    });

    if (!this.state.expanded) {
      this.fakeInputRef.current.focus();
      Keyboard.dismiss();

      if (Platform.OS === 'ios') {
        this.viewRef.current.focus();
        this.viewRef.current.blur();
      }
    }
  }

  render() {
    const { label, value, align, ...props } = this.props;

    return (
      <View ref={this.viewRef}>
        <TouchableHighlight underlayColor={color.accent} activeOpacity={0.9} onPress={this.togglePicker}>
          <View style={[styles.fieldLine, props.style]}>
            <Span style={[styles.label, props.labelStyle]}>{label}</Span>
            <Span style={[styles.value, props.valueStyle, align && { textAlign: align }]}>{value}</Span>
          </View>
        </TouchableHighlight>
        <TextInput ref={this.fakeInputRef} style={styles.fakeInput} />
        <Animated.View style={[styles.pickerLine, { height: this.state.pickerHeight }]}>
          {this.props.children}
        </Animated.View>
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  fieldLine: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingHorizontal: 30,
    paddingVertical: 20,
    backgroundColor: '#fff'
  },
  label: {
    flex: 2,
    fontSize: 14,
    lineHeight: 20
  },
  value: {
    flex: 3,
    fontSize: 16,
    lineHeight: 20
  },
  fakeInput: {
    width: 0,
    height: 0,
    padding: 0
  },
  pickerLine: {
    overflow: 'hidden',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  modal: {
    justifyContent: 'space-evenly',
    paddingVertical: 20,
    borderRadius: 10,
    backgroundColor: '#fff'
  }
});