import React from 'react';

import TextInputField from './TextInputField';

export type Props = {
  label: string;
  value: number;
  onValueChange: (val: number) => void;
}

export default function CurrencyField(props: Props) {
  return (
    <TextInputField
      masked
      align='right'
      append='#common.uah'
      keyboardType='decimal-pad'
      maxLength={5}
      {...props}
      type={'only-numbers'}
      value={String(props.value)}
      onValueChange={value => props.onValueChange(+value)}
    />
  );
}
