import React from 'react';

import TextInputField, { Props as TextInputFieldProps } from './TextInputField';

export type Props = TextInputFieldProps & {
  countryCode?: string;
}

export default function PhoneField(props: Props) {
  const { value, onValueChange, countryCode = '380', ...otherProps } = props;
  const cleanValue = value && value.replace(new RegExp(`^${countryCode}`), '');

  return (
    <TextInputField
      masked
      keyboardType='number-pad'
      placeholder='(00) 000 00 00'
      {...otherProps}
      type={'custom'}
      options={{
        mask: '(99) 999 99 99',
        getRawValue: val => val.replace(/\D/g, '')
      }}
      prepend={`+${countryCode}`}
      value={cleanValue}
      onValueChange={value => onValueChange(countryCode + value)}
    />
  );
}
