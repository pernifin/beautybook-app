import React from 'react';
import { View, StyleSheet } from 'react-native';
import { range } from 'lodash';

import { formatDuration } from 'src/utils';

import Picker from '../shims/Picker';
import Span from '../Span';

export type Props = {
  value: number;
  onValueChange: (val: number) => void;
  min?: number;
  max?: number;
  step?: number;
  style?: { [key: string]: any };
}

const hourItems = range(8).map(h => ({
  label: String(h),
  value: h
}));

const minuteItems = range(4).map(m => ({
  label: String(m * 15),
  value: m * 15
}));

export default class DurationField extends React.Component<Props> {
  static defaultProps = {
    min: 15,
    max: 480,
    step: 15
  };

  get durationItems() {
    const { min, max, step } = this.props;

    return range(min, max + 1, step).map(d => ({
      label: formatDuration(d),
      value: d
    }));
  }

  onHoursChange = (hours) => {
    const time = this.props.value % 60 + hours * 60;
    this.props.onValueChange(time);
  }

  onMinutesChange = (minutes) => {
    const time = Math.floor(this.props.value / 60) * 60 + minutes;
    this.props.onValueChange(time);
  }

  render() {
    const hours = Math.floor(this.props.value / 60);
    const minutes = this.props.value % 60;

    return (
      <View style={styles.container}>
        {this.renderFakePicker()}
        <Picker
          cyclic
          data={hourItems}
          value={hours}
          onValueChange={this.onHoursChange}
          style={styles.picker}
          itemStyle={styles.item}
        />
        <Span style={styles.hours}>{{ key: '#datetime.hours', count: hours }}</Span>
        <Picker
          cyclic
          data={minuteItems}
          value={minutes}
          onValueChange={this.onMinutesChange}
          style={styles.picker}
          itemStyle={styles.item}
        />
        <Span style={styles.minutes}>{{ key: '#datetime.minutes', count: minutes }}</Span>
        {this.renderFakePicker()}
      </View>
    );
  }

  renderFakePicker() {
    return (
      <Picker
        style={styles.picker}
        itemStyle={styles.item}
      />
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 180,
  },
  picker: {
    flex: 1
  },
  item: {
    fontSize: 18
  },
  hours: {
    position: 'absolute',
    zIndex: -1,
    top: 80,
    left: '42%',
    fontSize: 16,
    lineHeight: 20,
    fontWeight: '500'
  },
  minutes: {
    position: 'absolute',
    zIndex: -1,
    top: 80,
    left: '67%',
    fontSize: 16,
    lineHeight: 20,
    fontWeight: '500'
  }
});