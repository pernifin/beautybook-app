import React from 'react';
import { View, Platform, StyleSheet } from 'react-native';
import { range } from 'lodash';
import moment from 'moment';

import t from 'src/Lang';
import { minutesToTime, formatDate } from 'src/utils';
import Picker from '../shims/Picker';

export type Props = {
  value: string;
  onValueChange: (val: string) => void;
}

function getDateItems() {
  return range(-60, 61).map(day => {
    const m = moment().add(day, 'd');
    return {
      label: day === 0 ? t('#datetime.today') : formatDate(m),
      value: m.format('YYYY-MM-DD')
    }
  });
}

function getTimeItems() {
  return range(0, 48).map(t => {
    return {
      label: minutesToTime(t * 30),
      value: minutesToTime(t * 30, true)
    };
  });
}

export default class DateTimeField extends React.Component<Props> {
  constructor(props) {
    super(props);
    this.onDateChange = this.onDateChange.bind(this);
    this.onTimeChange = this.onTimeChange.bind(this);
  }

  dateItems = getDateItems();
  timeItems = getTimeItems();

  onDateChange = (date: string) => {
    const [_, time] = this.props.value.split(' ');
    this.props.onValueChange(`${date} ${time}`);
  }

  onTimeChange = (time: string) => {
    const [date] = this.props.value.split(' ');
    this.props.onValueChange(`${date} ${time}`);
  }

  render() {
    const [date, time] = this.props.value.split(' ');

    return (
      <View style={styles.container}>
        {this.renderFakePicker()}
        <Picker
          data={this.dateItems}
          value={date}
          onValueChange={this.onDateChange}
          style={styles.picker}
          itemStyle={styles.item}
        />
        <Picker
          data={this.timeItems}
          value={time}
          onValueChange={this.onTimeChange}
          style={styles.picker}
          itemStyle={styles.item}
        />
        {this.renderFakePicker()}
      </View>
    );
  }

  renderFakePicker() {
    return (
      <Picker
        style={[styles.picker, { flex: 0.5 }]}
        itemStyle={styles.item}
      />
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 180
  },
  picker: {
    flex: 1
  },
  item: {
    fontSize: 18
  }
});
