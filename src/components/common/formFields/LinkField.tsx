import React from 'react'
import { View, Image, TouchableHighlight, StyleSheet, ImageRequireSource } from 'react-native'
import IonIcon from 'react-native-vector-icons/Ionicons';

import { color } from '../../../theme';
import Span from '../Span';

export type Props = {
  label: string;
  icon?: ImageRequireSource;
  value?: string;
  narrow?: boolean;
  bold?: boolean;
  onPress: () => void;
}

const LinkField = (props: Props) => {
  return (
    <TouchableHighlight underlayColor={color.accentAlpha(0.1)} onPress={props.onPress}>
      <View style={[styles.field, props.narrow && styles.fieldNarrow]}>
        {props.icon && <Image source={props.icon} style={styles.icon} />}
        <Span numberOfLines={1} style={[
          styles.label,
          props.bold && styles.labelBold,
          props.icon && styles.labelShifted
        ]}>
          {props.label}
        </Span>
        {props.value && <Span style={styles.value}>{props.value}</Span>}
        <IonIcon name='ios-arrow-forward' size={15} color={color.accentAlpha(0.5)} style={styles.arrow} />
      </View>
    </TouchableHighlight>
  );
}

export default LinkField;

export const styles = StyleSheet.create({
  field: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 30,
    paddingRight: 50,
    paddingVertical: 20
  },
  fieldNarrow: {
    paddingVertical: 10
  },
  icon: {
    position: 'absolute',
    top: 15,
    left: 30,
    width: 30,
    height: 30,
    resizeMode: 'contain'
  },
  label: {
    flex: 2,
    fontSize: 14,
    lineHeight: 20
  },
  labelBold: {
    fontWeight: '500'
  },
  labelShifted: {
    paddingLeft: 50
  },
  value: {
    flex: 3,
    fontSize: 16,
    lineHeight: 20,
  },
  arrow: {
    position: 'absolute',
    right: 30
  }
});