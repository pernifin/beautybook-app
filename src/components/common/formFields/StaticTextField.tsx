import React from 'react';
import { View, StyleSheet } from 'react-native';

import Span from '@/common/Span';

export type Props = {
  label: string;
  value: string;
  align?: 'left' | 'right';
  bold?: boolean;
}

export default function StaticTextField(props: Props) {
  return (
    <View style={styles.fieldLine}>
      <Span style={[styles.label, props.bold && { fontWeight: '500' }]}>{props.label}</Span>
      <Span raw style={[
        styles.value,
        props.align && { textAlign: props.align },
        props.bold && { fontWeight: '500' }
      ]}>
        {props.value}
      </Span>
    </View>
  );
}

export const styles = StyleSheet.create({
  fieldLine: {
    flexDirection: 'row',
    paddingHorizontal: 30,
    paddingVertical: 20,
    backgroundColor: '#fff'
  },
  label: {
    flex: 2,
    fontSize: 14,
    lineHeight: 20
  },
  value: {
    flex: 3,
    fontSize: 16,
    lineHeight: 20,
    color: '#666'
  }
});