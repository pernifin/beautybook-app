import React from 'react'
import { View, TouchableHighlight, StyleSheet } from 'react-native'
import IonIcon from 'react-native-vector-icons/Ionicons';

import { color } from 'src/theme';
import Span from '@/common/Span';

export type Props = {
  label: string;
  selected: boolean;
  onPress: () => void;
}

export default function SelectableField(props: Props) {
  return (
    <TouchableHighlight underlayColor={color.accent} activeOpacity={0.9} onPress={props.onPress}>
      <View style={styles.fieldLine}>
        <Span style={styles.label}>{props.label}</Span>
        {props.selected ? <IonIcon name='ios-checkmark' size={30} color={color.accent} style={styles.mark} /> : null}
      </View>
    </TouchableHighlight>
  );
}

export const styles = StyleSheet.create({
  fieldLine: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 30,
    paddingVertical: 20,
    height: 60,
    backgroundColor: '#fff'
  },
  label: {
    flex: 1,
    fontSize: 14,
    lineHeight: 20
  },
  mark: {
    position: 'absolute',
    right: 30
  }
});