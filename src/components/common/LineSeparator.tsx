import React from 'react';
import { View, StyleSheet } from 'react-native';

import { color } from '../../theme';

export type Props = {
  hidden?: boolean;
  short?: boolean;
  style?: { [key: string]: any };
}

export default function LineSeparator(props: Props) {
  return <View style={[
    styles.line,
    props.style,
    props.hidden && styles.hidden,
    props.short && styles.short
  ]} />;
}

export const styles = StyleSheet.create({
  line: {
    marginHorizontal: 16,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: color.accentAlpha(0.1)
  },
  hidden: {
    borderColor: 'transparent'
  },
  short: {
    width: '20%',
    alignSelf: 'center'
  }
});
