import React from 'react';
import { ScrollView, Platform, StyleSheet } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { fetchCatalogCategories } from 'src/actions';

import { ICategory } from '@/common/DataTypes';
import { SingleHeaderButton } from '@/common/buttons';
import Form from '@/common/containers/Form';
import { LinkField } from '@/common/formFields';

export type Props = {
  navigation: NavigationStackProp;
  categories: ICategory[];
  fetchCatalogCategories: () => Promise<any>;
}

const icons = {
  hair: require('assets/images/catalog/hair.png'),
  nails: require('assets/images/catalog/nails.png'),
  cosmetology: require('assets/images/catalog/cosmetology.png'),
  makeup: require('assets/images/catalog/makeup.png'),
  eyelash: require('assets/images/catalog/eyelash.png'),
  eyebrow: require('assets/images/catalog/eyebrow.png'),
  epilation: require('assets/images/catalog/epilation.png'),
  tattoo: require('assets/images/catalog/tattoo.png'),
  massage: require('assets/images/catalog/massage.png')
};

export class SelectCategoryScreen extends React.Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    headerLeft: ({ onPress }) => (
      <SingleHeaderButton
        label={Platform.OS === 'ios' && '#common.cancel'}
        iconName={Platform.OS === 'android' && 'back'}
        onPress={() => navigation.popToTop()}
      />
    ),
    title: t('#services.selectCategory'),
    headerBackTitle: t('#common.back')
  });

  async componentDidMount() {
    await this.props.fetchCatalogCategories();
  }

  onCategorySelect = (category: ICategory) => {
    this.props.navigation.navigate('SelectService', { category: category });
  }

  render() {
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <Form>
          {this.props.categories.map(category => (
            <LinkField
              key={category.id}
              icon={icons[category.id]}
              label={category.title}
              onPress={() => this.onCategorySelect(category)}
            />
          ))}
        </Form>
      </ScrollView>
    );
  }
}

export default connect(
  state => ({
    categories: state.catalog.categories
  }),
  {
    fetchCatalogCategories
  }
)(SelectCategoryScreen);

export const styles = StyleSheet.create({
  container: {
    paddingVertical: 10
  }
});