import React from 'react';
import { View, Image, StyleSheet, Platform } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';
import { cloneDeep } from 'lodash';

import t from 'src/Lang';
import * as theme from 'src/theme';

import { IService } from '@/common/DataTypes';
import { SingleHeaderButton, FAB } from '@/common/buttons';
import ServiceList from '@/common/elements/ServiceList';
import Span from '@/common/Span';

export type Props = {
  navigation: NavigationStackProp;
  services: IService[];
}

export class ServiceListScreen extends React.Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: t('#nav.services'),
    headerRight: Platform.OS === 'ios' && (
      <SingleHeaderButton iconName='add' onPress={() => navigation.navigate('CreateService')} />
    )
  });

  selectService = (service: IService) => {
    this.props.navigation.navigate('EditService', { service: cloneDeep(service) });
  }

  deleteService = (service: IService) => {
    
  }

  render() {
    const { services, navigation } = this.props;

    return (
      <View style={styles.container}>
        {services.length ? (
          <ServiceList
            services={this.props.services}
            onServicePress={this.selectService}
            onDeletePress={this.deleteService}
          />
        ) : this.renderBlank()}

        {Platform.OS === 'android' && (
          <FAB iconName='add' onPress={() => navigation.navigate('CreateService')} />
        )}
      </View>
    );
  }

  renderBlank() {
    return (
      <View style={styles.blank}>
        <Image source={require('assets/images/icons/services.png')} style={styles.icon} />
        <Span style={styles.message}>#services.emptyList</Span>
      </View>
    );
  }
}

export default connect(
  state => ({
    services: state.services
  })
)(ServiceListScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  blank: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    width: 100,
    height: 100,
    resizeMode: 'contain'
  },
  message: {
    marginTop: 40,
    width: 200,
    textAlign: 'center',
    fontSize: 20,
    color: theme.color.accentLight
  }
});