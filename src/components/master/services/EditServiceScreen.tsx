import React from 'react';
import { StyleSheet, Platform } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { saveService, deleteService } from 'src/actions';
import { formatDuration } from 'src/utils';

import { IService } from '@/common/DataTypes'
import { SingleHeaderButton, MainButton, SecondaryButton } from '@/common/buttons';
import KeyboardAvoidScrollView from '@/common/shims/KeyboardAvoidScrollView';
import Form from '@/common/containers/Form';
import { DurationField, TextInputField, PickerField, StaticTextField, CurrencyField } from '@/common/formFields';

export type Props = {
  navigation: NavigationStackProp;
  saveService: (service: IService) => Promise<any>;
  deleteService: (service: IService) => Promise<any>;
  querying: boolean;
}

export type State = {
  service: IService;
}

export class EditServiceScreen extends React.Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: navigation.getParam('service', {}).id ? t('#services.editService') : t('#services.newService'),
    headerRight: Platform.OS === 'ios' &&
      <SingleHeaderButton
        label='#common.save'
        bold
        enabled={navigation.getParam('saveEnabled', false)}
        onPress={navigation.getParam('saveOnPress')}
      />
  });

  constructor(props: Props) {
    super(props);
    this.saveForm = this.saveForm.bind(this);
    this.deleteService = this.deleteService.bind(this);
    this.updateRecord = this.updateRecord.bind(this);

    this.state = {
      service: props.navigation.getParam('service')
    };

    props.navigation.setParams({
      saveEnabled: this.isFormValid,
      saveOnPress: this.saveForm
    });
  }

  get isFormValid() {
    const isValid = this.state.service.title && this.state.service.duration && !this.props.querying;
    return Boolean(isValid);
  }

  componentDidUpdate(prevProps, prevState: State) {
    if (prevState.service !== this.state.service) {
      this.props.navigation.setParams({
        saveEnabled: this.isFormValid,
      });
    }

    if (prevProps.querying !== this.props.querying) {
      this.props.navigation.setParams({
        saveEnabled: this.isFormValid
      });
    }
  }
  
  async saveForm() {
    const result = await this.props.saveService(this.state.service);
    if (result.success) {
      this.props.navigation.navigate('ServiceList');
    }
  }

  async deleteService(force?: boolean) {
    if (!force) {
      return this.props.navigation.navigate('Modal', {
        message: '#services.confirmDelete',
        closeLabel: '#common.cancel',
        confirmLabel: '#common.delete',
        onConfirmPress: () => this.deleteService(true)
      });
    }
    
    const result = await this.props.deleteService(this.state.service);
    if (result.success) {
      this.props.navigation.popToTop();
    }
  }

  updateRecord(values: Partial<IService>) {
    this.setState(state => ({
      service: { ...state.service, ...values }
    }));
  }

  render() {
    const { service } = this.state;
    return (
      <KeyboardAvoidScrollView contentContainerStyle={styles.container}>
        <Form style={styles.form}>
          <StaticTextField label='#label.category' value={service.category.title} />
          <TextInputField
            label='#label.title'
            value={service.title}
            onValueChange={title => this.updateRecord({ title })}
            multiline
          />
          <PickerField label='#label.duration' align='right' value={formatDuration(service.duration)}>
            <DurationField
              value={service.duration}
              onValueChange={duration => this.updateRecord({ duration })}
            />
          </PickerField>
          <CurrencyField
            label='#label.price'
            value={service.price || 0}
            onValueChange={price => this.updateRecord({ price })}
          />
          {/* <TextInputField
            label='#label.description'
            value={service.description}
            onValueChange={description => this.updateRecord({ description })}
            multiline
            placeholder='#label.moreDetails'
          /> */}
        </Form>

        {Platform.OS === 'android' && (
          <MainButton enabled={this.isFormValid} label='#common.save' onPress={this.saveForm} />
        )}

        {service.id && (
          <SecondaryButton label='#services.deleteService' onPress={this.deleteService} />
        )}
      </KeyboardAvoidScrollView>
    );
  }
}

export default connect(
  state => ({
    querying: state.querying
  }),
  {
    saveService,
    deleteService
  }
)(EditServiceScreen);

export const styles = StyleSheet.create({
  container: {
    paddingVertical: 20
  },
  form: {
    marginBottom: 20
  }
});