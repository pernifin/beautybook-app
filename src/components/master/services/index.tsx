import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import t from '../../../Lang';
import { color } from '../../../theme';

import ServiceListScreen from './ServiceListScreen';
import SelectCategoryScreen from './SelectCategoryScreen';
import SelectServiceScreen from './SelectServiceScreen';
import EditServiceScreen from './EditServiceScreen';

export default createStackNavigator({
  Services: createStackNavigator({
    ServiceList: ServiceListScreen,
    EditService: EditServiceScreen,
  }, {
    navigationOptions: {
      header: null
    },
    defaultNavigationOptions: () => ({
      headerTintColor: Platform.OS === 'ios' ? color.primary : color.white,
      headerTitleStyle: { color: color.black },
    })
  }),
  CreateService: createStackNavigator({
    SelectCategory: SelectCategoryScreen,
    SelectService: SelectServiceScreen,
    CompleteServiceForm: EditServiceScreen
  }, {
    navigationOptions: {
      header: null
    },
    defaultNavigationOptions: () => ({
      headerTintColor: Platform.OS === 'ios' ? color.primary : color.white,
      headerTitleStyle: { color: color.black },
    })
  })
}, {
  mode: 'modal',
  navigationOptions: ({ navigation }) => ({
    tabBarVisible: navigation.state.index === 0,
    tabBarLabel: t('#nav.services')
  })
});
