import React from 'react';
import { View, ScrollView, SafeAreaView, StyleSheet, Platform } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import SearchBar from 'react-native-search-bar';
import { connect } from 'react-redux';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { fetchCatalogServices } from 'src/actions';

import { ICategory, IService, ICatalogService } from '@/common/DataTypes';
import { SingleHeaderButton } from '@/common/buttons';
import Form from '@/common/containers/Form';
import { LinkField } from '@/common/formFields';

export type Props = {
  navigation: NavigationStackProp;
  category: ICategory;
  services: ICatalogService[];
  fetchCatalogServices: (category: string) => Promise<any>;
}

export type State = {
  services: ICatalogService[];
}

export class SelectServiceScreen extends React.Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: t('#services.selectService'),
    headerBackTitle: t('#common.back'),
    headerRight: (
      <SingleHeaderButton iconName='edit-clean' onPress={navigation.getParam('openClean')} />
    )
  });

  constructor(props: Props) {
    super(props);

    this.state = {
      services: props.services
    };

    props.navigation.setParams({
      openClean: this.openEmptyServiceForm.bind(this)
    });
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.services !== this.props.services) {
      this.setState({ services: this.props.services });
    }
  }

  get emptyService(): Partial<ICatalogService> {
    return { id: null, title: '' };
  }

  async componentDidMount() {
    await this.props.fetchCatalogServices(this.props.category.id);
  }

  onServiceSelect(service: Partial<ICatalogService>) {
    this.props.navigation.navigate('CompleteServiceForm', { service: this.transformCatalogService(service) });
  }

  openEmptyServiceForm() {
    this.props.navigation.navigate('CompleteServiceForm', {
      service: this.transformCatalogService({ id: null, title: '' })
    });
  }

  transformCatalogService(service: Partial<ICatalogService>): IService {
    return {
      catalogServiceId: service.id,
      category: { ...this.props.category },
      title: service.title,
      description: service.description,
      duration: service.duration || 60,
      price: service.price
    };
  }

  onSearch = (text: string) => {
    const services = text.length > 1
      ? this.props.services.filter(service => service.title.toLowerCase().match(text.toLowerCase()))
      : this.props.services;

    this.setState({ services });
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.search}>
          <SearchBar
            searchBarStyle='minimal'
            tintColor={theme.color.grey}
            placeholder={t('#common.search')}
            cancelButtonText={t('#common.cancel')}
            onChangeText={this.onSearch}
          />
        </View>
        <ScrollView>
          <Form>
            {/* <LinkField key={-1} narrow bold label='Добавить вручную' onPress={() => this.onServiceSelect(this.emptyService)} /> */}
            {this.state.services.map(service => (
              <LinkField key={service.id} narrow label={service.title} onPress={() => this.onServiceSelect(service)} />
            ))}
          </Form>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, ownProps) => {
    const selectedCategory = ownProps.navigation.getParam('category');

    return {
      category: selectedCategory,
      services: state.catalog.services[selectedCategory.id] || []
    };
  },
  {
    fetchCatalogServices
  }
)(SelectServiceScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  search: {
    paddingRight: Platform.OS === 'ios' ? 6 : 4,
    paddingLeft: Platform.OS === 'ios' ? 6 : 12,
    paddingBottom: Platform.OS === 'ios' ? 0 : 10,
    paddingTop: Platform.OS === 'ios' ? 0 : 6
  }
});