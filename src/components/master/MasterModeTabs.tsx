import React from 'react';
import { Platform } from 'react-native';
import { createBottomTabNavigator, BottomTabBar } from 'react-navigation-tabs';

import { color } from 'src/theme';

import { avoidKeyboard } from 'src/components/common/shims/KeyboardAvoidContainer';
import Icon from '@/common/Icon';

import CalendarStack from './calendar';
import ScheduleStack from './schedule';
import MasterCodeScreen from './code/MasterCodeScreen';
import ServicesStack from './services';
import ProfileStack from '@/shared/profile';

const KeyboardAvoidBottomTabBar = avoidKeyboard(BottomTabBar);

export default createBottomTabNavigator({
  Calendar: CalendarStack,
  Schedule: ScheduleStack,
  Code: MasterCodeScreen,
  Services: ServicesStack,
  Profile: ProfileStack
}, {
  initialRouteName: 'Calendar',
  ...(Platform.OS === 'android' ? { tabBarComponent: props => <KeyboardAvoidBottomTabBar {...props} /> } : {}),
  tabBarOptions: {
    activeTintColor: color.accent,
    inactiveTintColor: color.accentLight
  },
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      const { routeName } = navigation.state;

      let iconName;
      if (routeName === 'Calendar') {
        iconName = 'nav-calendar';
      } else if (routeName === 'Schedule') {
        iconName = 'nav-schedule';
      } else if (routeName === 'Code') {
        iconName = 'nav-qrcode';
      } else if (routeName === 'Services') {
        iconName = 'nav-services';
      } else if (routeName === 'Profile') {
        iconName = 'nav-profile';
      }

      return <Icon name={iconName} size={23} color={tintColor} />;
    }
  })
});
