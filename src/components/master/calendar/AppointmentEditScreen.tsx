import React, { Component } from 'react';
import { StyleSheet, Platform } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';
import { sum } from 'lodash';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { saveAppointment } from 'src/actions';
import { formatDatetime, formatDuration, checkPhone } from 'src/utils';

import { IAppointment, IService } from '@/common/DataTypes';
import { MainButton, SingleHeaderButton } from '@/common/buttons';
import KeyboardAvoidScrollView from '@/common/shims/KeyboardAvoidScrollView';
import Form from '@/common/containers/Form';
import SelectedServicesList from '@/common/elements/SelectedServicesList';
import {
  PickerField, DateTimeField, LinkField, DurationField, TextInputField, PhoneField, CurrencyField
} from '@/common/formFields';

export type Props = {
  navigation: NavigationStackProp;
  appointment: IAppointment;
  saveAppointment: (appointment: IAppointment, force?: boolean) => Promise<any>;
  warningMessage: string;
  querying: boolean;
}

export type State = {
  appointment: IAppointment;
}

const emptyAppointment = (datetime: string): IAppointment => {
  return {
    clientName: '',
    clientPhone: '',
    datetime,
    services: [],
    duration: 0,
    price: 0,
    note: ''
  };
}

export class AppointmentEditScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    headerLeft: ({ onPress }) => (
      <SingleHeaderButton
        label={Platform.OS === 'ios' && '#common.cancel'}
        iconName={Platform.OS === 'android' && 'close'}
        onPress={() => navigation.popToTop()}
      />
    ),
    headerBackTitle: t('#common.back'),
    title: navigation.getParam('id') ? t('#calendar.editRecord') : t('#calendar.newRecord'),
    headerRight: Platform.OS === 'ios' && (
      <SingleHeaderButton
        label='#common.save'
        bold
        enabled={navigation.getParam('saveEnabled', !!navigation.getParam('id'))}
        onPress={navigation.getParam('saveOnPress')}
      />
    )
  });

  constructor(props: Props) {
    super(props);
    this.openServiceSelection = this.openServiceSelection.bind(this);
    this.update = this.update.bind(this);
    this.save = this.save.bind(this);

    this.state = {
      appointment: props.appointment
    };

    props.navigation.setParams({
      saveEnabled: this.isFormValid,
      saveOnPress: this.save
    });
  }

  componentDidUpdate(prevProps, prevState: State) {
    if (prevState.appointment !== this.state.appointment) {
      this.props.navigation.setParams({ saveEnabled: this.isFormValid });
    }
  }

  openServiceSelection() {
    this.props.navigation.navigate('SelectServices', {
      selected: this.state.appointment.services,
      onSave: services => this.update({ services })
    });
  }

  removeService = (service: IService) => {
    const services = this.state.appointment.services.filter(s => s !== service);

    this.update({ services });
  }

  get isFormValid() {
    const { appointment } = this.state;

    const valid = !this.props.querying
      && appointment.clientName
      && checkPhone(appointment.clientPhone)
      && appointment.services.length > 0
      && appointment.duration > 0;

    return Boolean(valid);
  }

  update(values: Partial<IAppointment>) {
    const { appointment } = this.state;
    const services = values.services || appointment.services;

    this.setState({
      appointment: {
        ...appointment,
        duration: sum(services.map(s => s.duration)),
        price: sum(services.map(s => s.price)),
        ...values
      }
    });
  }

  async save(force?: boolean) {
    const result = await this.props.saveAppointment(this.state.appointment, force);

    if (result.success) {
      this.props.navigation.popToTop();
    } else if (result.warning) {
      this.props.navigation.navigate('Modal', {
        message: this.props.warningMessage,
        icon: 'calendar',
        closeLabel: '#common.cancel',
        confirmLabel: '#common.save',
        onConfirmPress: () => this.save(true)
      });
    }
  }

  render() {
    const { appointment } = this.state;

    return (
      <KeyboardAvoidScrollView>
        <Form style={styles.form}>
          <TextInputField
            label='#label.clientName'
            value={appointment.clientName}
            onValueChange={clientName => this.update({ clientName })}
          />
          <PhoneField
            label='#label.phone'
            value={appointment.clientPhone}
            onValueChange={clientPhone => this.update({ clientPhone })}
          />
          <PickerField label='#label.dateTime' value={formatDatetime(appointment.datetime)}>
            <DateTimeField
              value={appointment.datetime}
              onValueChange={datetime => this.update({ datetime })}
            />
          </PickerField>
          <LinkField label='#label.selectServices' onPress={this.openServiceSelection} />
          {appointment.services.length > 0 && (
            <SelectedServicesList services={appointment.services} onDeletePress={this.removeService} />
          )}
          <PickerField label='#label.duration' align='right' value={formatDuration(appointment.duration)}>
            <DurationField
              value={appointment.duration}
              onValueChange={duration => this.update({ duration })}
            />
          </PickerField>
          <CurrencyField
            label='#label.price'
            value={appointment.price}
            onValueChange={price => this.update({ price })}
          />
          <TextInputField
            label='#label.note'
            value={appointment.note}
            onValueChange={note => this.update({ note })}
            placeholder='#label.addNote'
            multiline
          />
        </Form>

        {Platform.OS === 'android' && (
          <MainButton enabled={this.isFormValid} label='#common.save' onPress={this.save} />
        )}
      </KeyboardAvoidScrollView>
    );
  }
}

export default connect(
  (state, ownProps) => {
    const recordId = ownProps.navigation.getParam('id');

    const appointment = recordId && state.calendar.records[recordId]
      ? state.calendar.records[recordId].appointment
      : emptyAppointment(ownProps.navigation.getParam('datetime'));

    return {
      appointment,
      warningMessage: state.warningMessage,
      querying: state.querying
    };
  },
  {
    saveAppointment
  }
)(AppointmentEditScreen);

export const styles = StyleSheet.create({
  form: {
    paddingVertical: 10
  }
});