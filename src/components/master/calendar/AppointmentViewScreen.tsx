import React, { Component } from 'react';
import { ScrollView, StyleSheet, Platform } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';
import moment from 'moment';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { removeAppointment } from 'src/actions';

import { IAppointment, ITimeBlock } from '@/common/DataTypes';
import { SingleHeaderButton, MainButton, SecondaryButton } from '@/common/buttons';
import KeyboardAvoidScrollView from '@/common/shims/KeyboardAvoidScrollView';
import AppointmentView from '@/common/elements/AppointmentView';

export type Props = {
  navigation: NavigationStackProp;
  record: ITimeBlock;
  removeAppointment: (record: ITimeBlock) => Promise<any>;
}

export class AppointmentViewScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: t('#appointments.details'),
    headerRight: Platform.OS === 'ios' && (
      <SingleHeaderButton 
        label='#common.edit'
        onPress={() => navigation.navigate('AppointmentForm', navigation.state.params)}
      />
    )
  });

  constructor(props: Props) {
    super(props);
    this.delete = this.delete.bind(this);
  }

  openEdit = () => {
    this.props.navigation.navigate('AppointmentForm', this.props.navigation.state.params);
  }

  async delete(force?: boolean) {
    if (!force) {
      return this.props.navigation.navigate('Modal', {
        message: '#calendar.confirmDelete',
        closeLabel: '#common.cancel',
        confirmLabel: '#common.delete',
        onConfirmPress: () => this.delete(true)
      });
    }

    const result = await this.props.removeAppointment(this.props.record);
    if (result.success) {
      this.props.navigation.popToTop();
    }
  }

  render() {
    const { record } = this.props;
    if (!record || !record.appointment) {
      return null;
    }

    return (
      <ScrollView>
        <AppointmentView appointment={record.appointment} />

        {Platform.OS === 'android' && <MainButton label='#common.edit' onPress={this.openEdit} />}
        <SecondaryButton label='#calendar.deleteAppointment' onPress={this.delete} />
      </ScrollView>
    );
  }
}

export default connect(
  (state, ownProps) => {
    const recordId = ownProps.navigation.getParam('id');

    return {
      record: state.calendar.records[recordId],
      warningMessage: state.warningMessage,
      querying: state.querying
    };
  },
  {
    removeAppointment
  }
)(AppointmentViewScreen);