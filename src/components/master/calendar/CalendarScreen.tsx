import React from 'react';
import { View, ScrollView, Animated, Platform, StyleSheet } from 'react-native';
import { NavigationEventSubscription } from 'react-navigation';
import { NavigationStackProp } from 'react-navigation-stack';
import ActionSheet from 'react-native-action-sheet';
import { connect } from 'react-redux';
import moment from 'moment';
import {
  differenceInCalendarDays, parseISO, addMonths, endOfMonth, addDays, formatISO, isBefore, isSameDay, eachDayOfInterval
} from 'date-fns';
import { mapValues, range, fromPairs, findKey } from 'lodash';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { loadCalendarDate } from 'src/actions';
import { minutesToTime } from 'src/utils';

import { ICalendar, ISchedule, ICalendarMark, ITimeBlock, IAppointment } from '@/common/DataTypes';
import { HeaderButtonContainer, HeaderButton, FAB } from '@/common/buttons';
import MonthCalendar from '@/common/elements/MonthCalendar';
import SlidablePanel from '@/common/containers/SlidablePanel';
import Icon from '@/common/Icon';
import Span from '@/common/Span';

import DayList from './views/DayList';
import WeekList from './views/WeekList';
import CalendarDay from './views/CalendarDay';
import DayView from './views/DayView';
import reactotron from 'reactotron-react-native';


enum MODE {
  WEEKLY,
  MONTHLY
}

export type Props = {
  navigation: NavigationStackProp;
  calendar: ICalendar;
  schedule: ISchedule;
  startDate: string;
  timeBounds: { min: number; max: number };
  loadCalendarDate: (date: string, wholeMonth?: boolean) => Promise<any>;
}

export type State = {
  currentDay: Date;
  markedDates: ICalendarMark;
  mode: MODE;

  panelDraggable: boolean;
  overlay?: boolean;
}

export class CalendarScreen extends React.Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: t('#nav.calendar'),
    headerRight: (
      <HeaderButtonContainer>
        {/* <HeaderButton
          iconName='month-view'
          toggle={navigation.getParam('modeToggled', false)}
          onPress={navigation.getParam('onMode')}
        /> */}
        {Platform.OS === 'ios' && <HeaderButton iconName='add' onPress={navigation.getParam('onAdd')} />}
      </HeaderButtonContainer>
    )
  });

  constructor(props: Props) {
    super(props);

    this.state = {
      currentDay: new Date(),
      markedDates: {},
      mode: MODE.WEEKLY,

      panelDraggable: true,
      overlay: false
    };

    props.navigation.setParams({
      onMode: () => this.switchMode(),
      onAdd: () => this.onTimePress()
    });

    //this.panelHeight.addListener(this.onPanelHeightChange);
  }

  willFocusSub: NavigationEventSubscription;
  loadedMonths = [];
  fab = new Animated.Value(0);

  shouldComponentUpdate(nextProps: Props, nextState: State) {
    if (this.state !== nextState) {
      return true;
    }

    return this.props.calendar !== nextProps.calendar;
  }

  componentDidMount() {
    this.willFocusSub = this.props.navigation.addListener('willFocus', () => {
      this.props.loadCalendarDate(moment(this.state.currentDay).format('YYYY-MM-DD'));
    });

    requestAnimationFrame(() => {
      requestAnimationFrame(() => {
        this.updateMarkedDates(true);
      });
    });
  }

  componentDidUpdate(prevProps: Props, prevState: State) {
    if (this.props.calendar !== prevProps.calendar) {
      requestAnimationFrame(() => {
        this.updateMarkedDates(false);
      });
    }

    if (this.state.mode !== prevState.mode) {
      this.props.navigation.setParams({
        modeToggled: this.state.mode === MODE.MONTHLY
      });
    }
  }

  componentWillUnmount() {
    this.willFocusSub.remove();
    //this.panelHeight.removeAllListeners();
  }
  
  switchMode() {
    this.setState({
      mode: this.state.mode === MODE.WEEKLY ? MODE.MONTHLY : MODE.WEEKLY
    });
  }

  onDateChange = (date: Date) => {
    const dateString = moment(date).format('YYYY-MM-DD');
    const markedDates = mapValues(this.state.markedDates, (marking, date) => ({ ...marking, selected: false }));
    markedDates[dateString] = { ...markedDates[dateString], selected: true };

    this.props.loadCalendarDate(dateString);
    this.setState({ currentDay: date, markedDates });
  }

  onRecordPress = (record: ITimeBlock) => {
    this.props.navigation.navigate(record.appointment ? 'AppointmentView' : 'TimeBreakForm', { id: record.id });
  }

  onTimePress = (time = 720) => {
    if (Platform.OS === 'ios') {
      ActionSheet.showActionSheetWithOptions({
        options: [t('#calendar.createAppointment'), t('#calendar.blockTime'), t('#common.cancel')],
        cancelButtonIndex: 2,
        tintColor: theme.color.accent
      }, buttonIndex => {
        if (buttonIndex === 0) {
          this.openAppointmentEdit(time);
        } else if (buttonIndex === 1) {
          this.openTimeBrakeEdit(time);
        }
      });
    } else {
      this.openAppointmentEdit(time);
    }
  }

  openAppointmentEdit = (time = 720) => {
    this.props.navigation.navigate('AppointmentEdit', {
      datetime: `${moment(this.state.currentDay).format('YYYY-MM-DD')} ${minutesToTime(time, true)}`
    });
  }

  openTimeBrakeEdit = (time = 720) => {
    this.props.navigation.navigate('TimeBreakForm', {
      datetime: `${moment(this.state.currentDay).format('YYYY-MM-DD')} ${minutesToTime(time, true)}`
    });
  }

  onMonthChange = (months: string[]) => {
    months.filter(m => !this.loadedMonths.includes(m)).forEach(m => {
      this.loadedMonths.push(m);
      this.props.loadCalendarDate(m, true);
    });
  }

  updateMarkedDates(full: boolean) {
    let dates;
    
    if (full) {
      const count = moment().endOf('month').add(3, 'months').diff(this.props.startDate, 'days');
      dates = range(count).map(d => moment(this.props.startDate).add(d, 'days').format('YYYY-MM-DD'));
    } else {
      dates = Object.keys(this.props.calendar);
    }

    //const startDate = parseISO(this.props.startDate);
    //const count = differenceInCalendarDays(startDate, addMonths(endOfMonth(new Date()), 3));

    // const pairs = eachDayOfInterval({ start: startDate, end: addMonths(endOfMonth(new Date()), 3) }).map(date => {
    //   const dt = formatISO(date, { representation: 'date' });
    //   const workload = this.props.calendar[dt]?.workload || 0;
    //   const schedule = this.props.schedule[dt];
    //   const dayoff = !schedule || !schedule.isWorking;
    //   const past = isBefore(date, new Date());
    //   const selected = isSameDay(date, this.state.currentDay);

    //   const load = schedule && schedule.isWorking
    //     ? Math.round(workload / (schedule.timeTo - schedule.timeFrom) * 100)
    //     : workload > 0
    //       ? 100
    //       : null;

    //   const dots = load > 90 ? 3 : (load > 50 ? 2 : (load > 0 ? 1 : 0));
    //   const dotsColor = dayoff || past ? 'inactive' : (dots === 3 ? 'red' : (dots === 2 ? 'yellow' : 'green'));

    //   return [dt, {
    //     selected,
    //     dayoff,
    //     dots,
    //     color: theme.color[dotsColor]
    //   }];
    // });

    const pairs = dates.map(date => {
      const workload = this.props.calendar[date]?.workload || 0;
      const schedule = this.props.schedule[date];
      const dayoff = !schedule || !schedule.isWorking;
      const past = moment().isAfter(date, 'day');
      const selected = moment(this.state.currentDay).isSame(date, 'day');

      const load = schedule && schedule.isWorking
        ? Math.round(workload / (schedule.timeTo - schedule.timeFrom) * 100)
        : workload > 0
          ? 1
          : null;

      const dots = load > 90 ? 3 : (load > 50 ? 2 : (load > 0 ? 1 : 0));
      const dotsColor = dayoff || past ? 'inactive' : (dots === 3 ? 'red' : (dots === 2 ? 'yellow' : 'green'));

      return [date, {
        selected,
        dayoff,
        dots,
        color: theme.color[dotsColor]
      }];
    });

    this.setState({
      markedDates: { ...this.state.markedDates, ...fromPairs(pairs) }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderWeeklyMode()}

        {Platform.OS === 'android' && this.renderActionButtons()}
      </View>
    );
  }

  renderActionButtons() {
    return (
      <FAB iconName='add' onPress={() => null}>
        <FAB.Item iconName='lock' label='#calendar.blockTime' onPress={this.openTimeBrakeEdit} />
        <FAB.Item iconName='edit-clean' label='#calendar.createAppointment' onPress={this.openAppointmentEdit} />
      </FAB>
    );
  }

  renderWeeklyMode() {
    const weekday = moment(this.state.currentDay).format('dddd');
    const date = moment(this.state.currentDay).format('D MMMM YYYY');

    return (
      <>
        <Span style={styles.title}>
          <Span style={styles.weekday}>{weekday}, </Span>
          <Span style={styles.date}>{date}</Span>
        </Span>
        <WeekList
          currentDay={this.state.currentDay}
          markedDates={this.state.markedDates}
          onDateChange={this.onDateChange}
        />
        <DayList
          currentDay={this.state.currentDay}
          calendar={this.props.calendar}
          schedule={this.props.schedule}
          timeBounds={this.props.timeBounds}
          onDateChange={this.onDateChange}
          onRecordPress={this.onRecordPress}
          onTimePress={this.onTimePress}
        />
      </>
    );
  }

  // panelHeight = new Animated.Value(300);

  // onDayScroll = ({ nativeEvent }) => {
  //   if (nativeEvent.contentOffset.y < 0) {
  //     this.setState({ panelDraggable: true });
  //   }
  // }

  // onMomentumDragEnd = (position: number) => {
  //   if (position === 554) {
  //     this.setState({ panelDraggable: false });
  //   }
  // }

  // onPanelHeightChange = ({ value }) => {
  //   // if (value === 600) {
  //   //   this.setState({ panelDraggable: false });
  //   // }
  // }

  // renderMonthlyMode() {
  //   const weekday = moment(this.state.currentDay).format('dddd');
  //   const formatDate = moment(this.state.currentDay).format('D MMMM');
  //   const date = moment(this.state.currentDay).format('YYYY-MM-DD');

  //   const headerTop = this.panelHeight.interpolate({
  //     inputRange: [554, 555],
  //     outputRange: [0, 1],
  //     extrapolateLeft: 'clamp'
  //   });

  //   const headerBottomBorder = this.panelHeight.interpolate({
  //     inputRange: [554, 564],
  //     outputRange: [0, 1],
  //     extrapolate: 'clamp'
  //   });

  //   return (
  //     <>
  //       <MonthCalendar
  //         startDate={this.props.startDate}
  //         currentDate={findKey(this.state.markedDates, marking => marking.selected)}
  //         markedDates={this.state.markedDates}
  //         monthHeight={370}
  //         dayComponent={CalendarDay as any}
  //         onDayPress={date => this.onDateChange(new Date(date))}
  //         onMonthChange={this.onMonthChange}
  //       />

  //       <SlidablePanel
  //         //animatedValue={this.panelHeight}
  //         range={{ top: 554, bottom: 135 }}
  //         header={(
  //           <View style={styles.panelHeader}>
  //             <Span>
  //               <Span style={styles.weekday}>{weekday}, </Span>
  //               <Span style={styles.date}>{formatDate}</Span>
  //             </Span>
  //           </View>
  //         )}
  //         //snappingPoints={[110, 800]}
  //         //height={554}
  //         //showBackdrop={false}
  //         //allowDragging={this.state.panelDraggable}
  //         //onMomentumDragEnd={this.onMomentumDragEnd}
  //         //containerStyle={styles.slidePanel}
  //       >
  //         <Animated.View style={[styles.slidePanel]}>
  //           <DayView
  //             date={date}
  //             calendar={this.props.calendar[date]}
  //             workday={this.props.schedule[date]}
  //             timeBounds={this.props.timeBounds}
  //             onRecordPress={this.onRecordPress}
  //             onTimePress={this.onTimePress}
  //           />
  //         </Animated.View>
  //       </SlidablePanel>
  //     </>
  //   );
  // }
}

function getTimeBounds(records: ITimeBlock[], schedule: ISchedule) {
  const workDays = Object.values(schedule);
  const defaultFrom = 480; // 8:00
  const defaultTo = 1200; // 20:00

  return {
    min: Math.min(
      ...records.map(r => r.timeFrom),
      ...workDays.filter(d => d.isWorking).map(d => d.timeFrom),
      defaultFrom
    ),
    max: Math.max(
      ...records.map(r => r.timeTo),
      ...workDays.filter(d => d.isWorking).map(d => d.timeTo),
      defaultTo
    )
  };
}

export default connect(
  state => ({
    calendar: state.calendar.dates,
    schedule: state.schedule.days,
    startDate: state.schedule.startDate,
    timeBounds: getTimeBounds(Object.values(state.calendar.records), state.schedule.days)
  }),
  {
    loadCalendarDate
  }
)(CalendarScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    paddingHorizontal: 16,
    paddingTop: 20,
    paddingBottom: 5
  },
  weekday: {
    textTransform: 'capitalize',
    fontSize: 16,
    lineHeight: 20,
    color: '#AFB6CD'
  },
  date: {
    fontSize: 20,
    lineHeight: 20
  },
  panelHeader: {
    // position: 'absolute',
    // zIndex: 1,
    // left: 0,
    // right: 0,
    // paddingTop: 15,
    // paddingBottom: 10,
    // paddingHorizontal: 16,
    // borderBottomWidth: 0,
    // borderColor: '#DDD',
    // shadowColor: '#000',
    // shadowRadius: 5,
    // shadowOpacity: 0.2,
    // shadowOffset: { width: 0, height: 4 },
    // borderTopLeftRadius: 15,
    // borderTopRightRadius: 15,
    // backgroundColor: '#FFF'
    paddingHorizontal: 16,
    paddingBottom: 10
  },
  panelContent: {
    marginTop: 45,
  },
  slidePanel: {
    // borderTopLeftRadius: 15,
    // borderTopRightRadius: 15,
    // borderWidth: StyleSheet.hairlineWidth,
    //borderBottomWidth: 0,
    //borderColor: '#ddd',
    // borderColor: 'red',
    // backgroundColor: '#FFF',
    // shadowColor: '#000',
    // shadowRadius: 5,
    // shadowOpacity: 0.1,
    // elevation: 12
  }
});