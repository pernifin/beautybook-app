import React, { Component } from 'react';
import { View, ScrollView, Platform, StyleSheet } from 'react-native';

import { ICalendarDate, IInterval } from '@/common/DataTypes';
import SlidablePanel from '@/common/containers/SlidablePanel';
import DayView from './DayView';

export type Props = {
  date: Date;
  calendar: ICalendarDate;
  workday: IInterval;
  timeBounds: { min: number; max: number };
  onRecordPress: 
}

export default class SlideDayPanel extends Component<Props> {
  render() {
    return (
      <SlidablePanel expanded fixed range={{ top: 500, bottom: 35 }}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <DayView
            date={date}
            calendar={this.props.calendar[date]}
            workday={this.props.schedule[date]}
            timeBounds={this.props.timeBounds}
            onRecordPress={this.onRecordPress}
            onTimePress={this.onTimePress}
          />
        </ScrollView>
      </SlidablePanel>
    )
  }
}
