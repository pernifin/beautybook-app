import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { Placeholder, PlaceholderLine, Fade } from 'rn-placeholder';
import { range, map, sortBy, compact } from 'lodash';

import { color } from 'src/theme';
import { formatPhone } from 'src/utils';

import { ICalendarDate, IAppointment, IInterval, ITimeBlock, ICalendarSlot } from '@/common/DataTypes';
import Span from '@/common/Span';
import Icon from '@/common/Icon';
import TimeSlot from '@/common/elements/TimeSlot';

export type Props = {
  date: string;
  calendar?: ICalendarDate;
  workday?: IInterval;
  timeBounds: { min: number; max: number };
  onRecordPress: (record: ITimeBlock) => void;
  onTimePress: (time: number) => void;
}

export default class DayView extends React.Component<Props> {
  offset = 60;

  constructor(props: Props) {
    super(props);
  }

  render() {
    const { calendar, workday, timeBounds } = this.props;
    if (!calendar || calendar.loading) {
      return this.renderPlaceholder();
    }

    return (
      <View style={styles.container}>
        <View style={{ width: 1, height: this.offset * (timeBounds.max - timeBounds.min) / 60 }}></View>
        <View style={styles.timeLabelSection}>{this.renderTimes(calendar.slots, workday)}</View>
        <View style={styles.slotsSection}>
          {this.renderGrid(workday)}
          {this.renderSlots(calendar.slots)}
        </View>
      </View>
    );
  }

  renderPlaceholder() {
    return (
      <Placeholder Animation={Fade} style={styles.placeholder}>
        {range(0, 24).map(i => <PlaceholderLine key={i} height={15} style={styles.placeholderLine} />)}
      </Placeholder>
    );
  }

  renderTimes(slots: ICalendarSlot[], workday: IInterval) {
    const { min, max } = this.props.timeBounds;

    return compact(range(min, max, 30).map(time => {
      const slot = slots.find(s => s.timeFrom <= time && s.timeTo > time);
      let active = Boolean(workday && workday.isWorking && workday.timeFrom <= time && workday.timeTo > time);
      let record;
      
      if (slot) {
        record = slot.records.find(record => record.timeFrom === time);
        if (!record) {
          return null;
        }
      }

      return (
        <TimeSlot
          key={`time-${time}`}
          time={time}
          enabled={active}
          selected={record}
          onPress={() => record ? this.props.onRecordPress(record) : (active ? this.props.onTimePress(time) : null)}
          style={[styles.timeLabel, { top: time - min }]}
        />
      );
    }));
  }

  renderGrid(workday: IInterval) {
    const from = workday && workday.isWorking ? workday.timeFrom : 1440;
    const to = workday && workday.isWorking ? workday.timeTo : 1440;
    const { min, max } = this.props.timeBounds;

    return range(min, max, 30).map(time => (
      <View key={`line-${time}`} style={[styles.timeLine, time >= from && time < to && styles.timeLineActive, { top: time - min }]} />
    ));
  }

  renderSlots(slots: ICalendarSlot[]) {
    const { min } = this.props.timeBounds;

    return map(slots, slot => {
      return (
        <View key={`slot-${slot.timeFrom}`} style={[styles.slot, { top: slot.timeFrom - min, height: slot.timeTo - slot.timeFrom }]}>
          { // Blocks always go first
            sortBy(slot.records, r => r.type === 'timeBlock' ? -r.timeFrom : r.timeFrom)
              .map(record => record.appointment
                ? this.renderAppointment(record, slot)
                : this.renderTimeBlock(record, slot)
              )
          }
        </View>
      );
    });
  }

  renderAppointment(record: ITimeBlock, slot: ICalendarSlot) {
    const { appointment } = record;

    return (
      <TouchableOpacity
        key={`appointment-${appointment.id}`}
        activeOpacity={0.8}
        onPress={() => this.props.onRecordPress(record)}
        style={[
          styles.appointment,
          appointment.note && styles.appointmentNote,
          { height: Math.max(record.timeTo - record.timeFrom, 30) - 8, marginTop: record.timeFrom - slot.timeFrom }
        ]}
      >
        <View>
          {appointment.services.map(service => (
            <Span
              key={service.id}
              raw
              numberOfLines={appointment.duration > 30 ? 2 : 1}
              style={styles.serviceText}
            >
              {service.title}
            </Span>
          ))}
          <Span raw style={styles.clientNameText}>{appointment.clientName}</Span>
          <Span style={styles.clientPhoneText}>{formatPhone(appointment.clientPhone)}</Span>

          {appointment.note
            ? <View style={styles.noteMark}><Span style={styles.noteMarkText}>{'\u2022'}</Span></View>
            : null
          }
        </View>
      </TouchableOpacity>
    );
  }

  renderTimeBlock(timeBlock: ITimeBlock, slot: ICalendarSlot) {
    return (
      <TouchableOpacity
        key={`timeblock-${timeBlock.id}`}
        activeOpacity={0.8}
        onPress={() => this.props.onRecordPress(timeBlock)}
        style={[
          styles.timeBlockContainer,
          { height: timeBlock.timeTo - timeBlock.timeFrom - 7, marginTop: timeBlock.timeFrom - slot.timeFrom, width: slot.records.length === 1 ? '100%' : 25 }
        ]}
      >
        <View style={styles.timeBlockInner}>
          <View style={styles.locks}>
            {range(timeBlock.timeFrom, timeBlock.timeTo, 30).map(time => (
              <Icon key={`lock-${time}`} name='lock' size={16} color={color.accentAlpha(0.5)} style={{ height: 30 }} />
            ))}
          </View>
          <Span raw style={styles.timeBlockNote}>{timeBlock.note}</Span>
        </View>
        
      </TouchableOpacity>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 8
  },
  timeLabelSection: {
    width: 60
  },
  slotsSection: {
    flex: 1,
    paddingRight: 8
  },
  timeLabel: {
    position: 'absolute',
    left: 8
  },
  offtime: {
    position: 'absolute',
    width: '100%',
    borderRadius: 5,
    backgroundColor: '#f6f6f6'
  },
  timeLine: {
    position: 'absolute',
    width: '100%',
    marginTop: 11,
    height: 1,
    backgroundColor: color.accentAlpha(0.1)
  },
  timeLineActive: {
    backgroundColor: color.accentAlpha(0.3)
  },
  slot: {
    position: 'absolute',
    flexDirection: 'row',
    width: '100%',
    backgroundColor: '#fff'
  },
  appointment: {
    flex: 1,
    overflow: 'hidden',
    paddingHorizontal: 7,
    paddingVertical: 2,
    marginHorizontal: 2,
    borderRadius: 4,
    backgroundColor: color.accentAlpha(0.1)
  },
  appointmentNote: {
    paddingRight: 13
  },
  serviceText: {
    fontSize: 12,
    lineHeight: 16,
    color: '#000'
  },
  clientNameText: {
    fontSize: 11,
    fontWeight: '500',
    lineHeight: 18,
    color: color.accent
  },
  clientPhoneText: {
    fontSize: 10,
    fontWeight: '500',
    lineHeight: 12,
    color: color.accentAlpha(0.7)
  },
  noteMark: {
    position: 'absolute',
    top: 5,
    right: -8,
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: color.primary
  },
  noteMarkText: {
    fontSize: 20,
    lineHeight: 0,
    color: '#808080'
  },
  timeBlockContainer: {
    width: 30
  },
  timeBlockInner: {
    flex: 1,
    flexDirection: 'row'
  },
  locks: {
    paddingTop: 2
  },
  timeBlockNote: {
    flex: 1,
    paddingHorizontal: 8,
    paddingTop: 4,
    fontSize: 13
  },
  placeholder: {
    paddingHorizontal: 20,
    paddingVertical: 10
  },
  placeholderLine: {
    marginBottom: 15
  }
});
