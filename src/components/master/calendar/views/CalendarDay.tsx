import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { DateObject } from 'react-native-calendars';
import { range } from 'lodash';

import { color } from 'src/theme';

import { ICalendarDateMark } from '@/common/DataTypes';
import Gradient from '@/common/containers/Gradient';
import Span from '@/common/Span';

export type Props = {
  marking?: ICalendarDateMark;
  date: Date | DateObject;
  onPress: (date: Date | DateObject) => void;
};

export default function CalendarDay(props: Props) {
  const { selected = false, dayoff = true, dots, color } = props.marking || {};

  return (
    <TouchableOpacity activeOpacity={0.8} onPress={() => props.onPress(props.date)} style={styles.day}>
      <Gradient active={selected} style={styles.dayInner}>
        <Span style={[
          styles.dayText,
          dayoff && styles.dayOffText,
          selected && styles.daySelectedText
        ]}>
          {props.date instanceof Date ? props.date.getDate() : props.date.day}
        </Span>
      </Gradient>

      {dots && !selected ? (
        <View style={styles.workload}>
          {range(dots).map(d => <View key={d} style={[styles.dot, { backgroundColor: color }]} />)}
        </View>
      ) : null}
    </TouchableOpacity>
  );
}

export const styles = StyleSheet.create({
  day: {
    maxWidth: 36,
    minHeight: 44,
    alignItems: 'center'
  },
  dayInner: {
    width: '100%',
    aspectRatio: 1,
    borderRadius: 100,
    justifyContent: 'space-evenly'
  },
  dayText: {
    fontSize: 15,
    lineHeight: 20,
    fontWeight: '500',
    textAlign: 'center',
    color: '#333'
  },
  dayOffText: {
    color: color.inactive
  },
  daySelectedText: {
    color: '#fff'
  },
  workload: {
    position: 'absolute',
    bottom: 8,
    flexDirection: 'row'
  },
  dot: {
    marginHorizontal: 1,
    width: 5,
    height: 5,
    borderRadius: 3
  }
});