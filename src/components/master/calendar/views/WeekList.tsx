import React from 'react';
import { View, Dimensions, TouchableOpacity, ScrollView, StyleSheet } from 'react-native';
import { range } from 'lodash';
import moment from 'moment';

import { color } from 'src/theme';

import { ICalendarMark } from '@/common/DataTypes';
import Gradient from '@/common/containers/Gradient';
import Span from '@/common/Span';

import CalendarDay from './CalendarDay';
import reactotron from 'reactotron-react-native';

export type Props = {
  currentDay: Date;
  markedDates: ICalendarMark;
  onDateChange: (date: Date) => void;
}

export type State = {
  weeks: number[];
}

const screenWidth = Dimensions.get('window').width;
const dayWidth = screenWidth / 7;

export default class WeekList extends React.Component<Props, State> {
  weekList = React.createRef<ScrollView>();
  blockScrollHandle = true;
  triggerDateChange = true;
  middleIndex = 1;
  spacerLength = 100;

  constructor(props: Props) {
    super(props);
    this.renderDay = this.renderDay.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.onDaySelect = this.onDaySelect.bind(this);

    const diff = Math.round(moment(props.currentDay).weekday(0).diff(moment().weekday(0), 'day') / 7);

    this.state = {
      weeks: range(diff - 1, diff + 2)
    };
  }

  get weekDayNames() {
    return range(0, 7).map(index => moment().weekday(index).format('ddd'));
  }

  componentDidMount() {
    requestAnimationFrame(() => {
      this.weekList.current.scrollTo({ x: screenWidth * (this.middleIndex + this.spacerLength), animated: false });
    });
  }

  componentDidUpdate(prevProps: Props) {
    const diff = moment(this.props.currentDay).weekday(0)
      .diff(moment().add(this.state.weeks[this.middleIndex], 'week').weekday(0), 'days');

    if (this.props.currentDay !== prevProps.currentDay && diff) {
      const moveToIndex = this.middleIndex + (diff < 0 ? -1 : 1);

      this.blockScrollHandle = false;
      this.triggerDateChange = false;
      this.weekList.current.scrollTo({ x: screenWidth * (moveToIndex + this.spacerLength) });
    }
  }

  onDaySelect(date: Date) {
    if (!moment(date).isSame(this.props.currentDay, 'day')) {
      this.props.onDateChange(new Date(date));
    }
  }

  onScroll({ nativeEvent }) {
    if (this.blockScrollHandle) {
      return;
    }

    let selectedIndex = Math.round(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width) - this.spacerLength;

    if (selectedIndex !== this.middleIndex) {
      this.blockScrollHandle = true;

      if (this.triggerDateChange) {
        const selectedDate = moment(this.props.currentDay)
          .add(7 * (selectedIndex < this.middleIndex ? -1 : 1), 'days').toDate();
        this.props.onDateChange(selectedDate);
      } else {
        this.triggerDateChange = true;
      }

      let weeks = this.state.weeks;

      if (selectedIndex === 0) {
        weeks = [weeks[0] - 1, ...weeks.slice(0, weeks.length - 1)];
        this.spacerLength--;
      } else if (selectedIndex === 2) {
        weeks = [...weeks.slice(1), weeks[weeks.length - 1] + 1];
        this.spacerLength++;
      }

      this.setState({ weeks });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          {this.weekDayNames.map(weekDay => <Span key={weekDay} style={styles.title}>{weekDay}</Span>)}
        </View>

        <ScrollView
          ref={this.weekList}
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          onScrollEndDrag={() => this.blockScrollHandle = false}
          onMomentumScrollEnd={() => this.blockScrollHandle = true}
          onScroll={this.onScroll}
          scrollEventThrottle={100}
        //contentContainerStyle={styles.dayList}
        >
          <View style={{ width: screenWidth * this.spacerLength }} />
          {this.state.weeks.map(week => (
            <View key={week} style={styles.dayList}>
              {range(0, 7).map(i => this.renderDay(moment().add(week, 'week').weekday(i).toDate()))}
            </View>
          ))}
        </ScrollView>
      </View>
    );
  }

  renderDay(date: Date) {
    const dateString = moment(date).format('YYYY-MM-DD');
    const selected = moment(this.props.currentDay).isSame(date, 'day');
    const marking = { ...this.props.markedDates[dateString], selected };

    return (
      <View key={date.getTime()} style={styles.day}>
        <CalendarDay date={date} marking={marking} onPress={this.onDaySelect} />
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#ddd'
  },
  header: {
    width: '100%',
    marginVertical: 4,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  title: {
    flex: 1,
    maxWidth: 36,
    textAlign: 'center',
    fontSize: 12,
    color: '#888'
  },
  dayList: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: screenWidth,
    borderColor: 'transparent' // Android scroll has glitches without this
  },
  day: {
    flex: 1,
    maxWidth: 36
  }
});
