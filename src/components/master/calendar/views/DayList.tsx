import React from 'react';
import { View, Dimensions, ScrollView, StyleSheet } from 'react-native';
import { range, uniqueId } from 'lodash';
import moment from 'moment';

import DayView from './DayView';
import { ICalendar, ISchedule, ITimeBlock, IAppointment } from '@/common/DataTypes';

export type Props = {
  currentDay: Date;
  calendar: ICalendar;
  schedule: ISchedule;
  timeBounds?: { min: number; max: number };
  onDateChange: (date: Date) => void;
  onRecordPress?: (record: ITimeBlock | IAppointment) => void;
  onTimePress?: (time: number) => void;
}

export type State = {
  days: Date[];
}

const { width } = Dimensions.get('window');

export default class DayList extends React.Component<Props, State> {
  dayList = React.createRef<ScrollView>();
  blockScrollHandle = true;
  rebuildDates = false;
  middleIndex = 2;
  spacer = 100;

  constructor(props: Props) {
    super(props);
    this.renderDay = this.renderDay.bind(this);
    this.onScroll = this.onScroll.bind(this);

    this.state = {
      days: range(-this.middleIndex, this.middleIndex + 1)
        .map(index => moment(props.currentDay).add(index, 'd').toDate())
    };
  }

  componentDidMount() {
    requestAnimationFrame(() => {
      this.dayList.current.scrollTo({ x: width * (this.middleIndex + this.spacer), animated: false });
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const diff = this.props.currentDay !== prevProps.currentDay
      ? Math.round(moment(this.props.currentDay).diff(this.state.days[this.middleIndex], 'hours') / 24)
      : 0;

    if (diff) {
      const moveToIndex = this.middleIndex + (diff < 0 ? -1 : 1);

      this.blockScrollHandle = false;
      this.dayList.current.scrollTo({ x: width * (moveToIndex + this.spacer) });

      if (Math.abs(diff) > 1) {
        const days = range(0, this.state.days.length).map(i => null);

        days[this.middleIndex] = this.state.days[this.middleIndex];
        days[moveToIndex] = this.props.currentDay;
        this.rebuildDates = true;

        this.setState({ days });
      }
    }
  }

  onScroll({ nativeEvent }) {
    if (this.blockScrollHandle) {
      return;
    }

    let selectedIndex = Math.round(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width) - this.spacer;

    if (selectedIndex !== this.middleIndex) {
      this.blockScrollHandle = true;

      const selectedDate = this.state.days[selectedIndex];
      if (!moment(selectedDate).isSame(this.props.currentDay, 'day')) {
        this.props.onDateChange(new Date(selectedDate));
      }

      let days = [...this.state.days];
      if (this.rebuildDates) {
        days = range(-selectedIndex, this.middleIndex + 1)
          .map(index => moment(selectedDate).add(index, 'd').toDate());
        this.rebuildDates = false;
      }

      if (selectedIndex < this.middleIndex) {
        days = [moment(days[0]).add(-1, 'd').toDate(), ...days.slice(0, days.length - 1)];
        this.spacer--;
      } else if (selectedIndex > this.middleIndex) {
        days = [...days.slice(1), moment(days[days.length - 1]).add(1, 'd').toDate()];
        this.spacer++;
      }

      this.setState({ days });
    }
  }

  renderDay(date: Date) {
    if (!date) {
      return <View key={uniqueId()} style={styles.day} />;
    }

    const dateString = moment(date).format('YYYY-MM-DD');
    const calendarDay = this.props.calendar[dateString];
    const workday = this.props.schedule[dateString];

    return (
      <View key={date.getTime()} style={styles.day}>
        <DayView
          date={dateString}
          calendar={calendarDay}
          workday={workday}
          timeBounds={this.props.timeBounds}
          onRecordPress={this.props.onRecordPress}
          onTimePress={this.props.onTimePress}
        />
      </View>
    );
  }

  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        <ScrollView
          ref={this.dayList}
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          onScrollEndDrag={() => { this.blockScrollHandle = false }}
          onMomentumScrollEnd={() => this.blockScrollHandle = true}
          onScroll={this.onScroll}
          scrollEventThrottle={100}
        >
          <View style={{ width: width * this.spacer }} />
          {this.state.days.map(this.renderDay)}
        </ScrollView>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  day: {
    width: width,
    borderColor: 'transparent' // Android scroll has glitches without this
  }
})