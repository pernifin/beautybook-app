import React, { Component } from 'react';
import { View, Image, StyleSheet, Platform } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';

import t from 'src/Lang';
import * as theme from 'src/theme';

import { IService } from '@/common/DataTypes';
import { SingleHeaderButton } from '@/common/buttons';
import ServiceList from '@/common/elements/ServiceList';
import Span from '@/common/Span';

export type Props = {
  navigation: NavigationStackProp;
  services: IService[];
}

export type State = {
  selected: IService[];
}

export class SelectServicesScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: t('#calendar.selectServicesHeader'),
    headerRight: <SingleHeaderButton label='#common.done' onPress={navigation.getParam('save')} />
  });

  constructor(props: Props) {
    super(props);
    this.selectService = this.selectService.bind(this);

    const selected = props.navigation.getParam('selected') || [];
    this.state = {
      selected: [...selected]
    };

    props.navigation.setParams({
      save: () => {
        props.navigation.getParam('onSave')(this.state.selected);
        props.navigation.goBack();
      }
    });
  }

  selectService(service: IService) {
    const selected = this.state.selected.map(s => s.id).includes(service.id)
      ? this.state.selected.filter(s => s.id !== service.id)
      : this.state.selected.concat(service);

    this.setState({ selected });
  }

  render() {
    const { services } = this.props;

    return (
      <View style={styles.container}>
        {services.length ? (
          <ServiceList
            services={this.props.services}
            selectable
            selected={this.state.selected}
            onServicePress={this.selectService}
          />
        ) : this.renderBlank()}
      </View>
    );
  }

  renderBlank() {
    return (
      <View style={styles.blank}>
        <Image source={require('assets/images/icons/services.png')} style={styles.icon} />
        <Span style={styles.message}>#services.emptyList</Span>
      </View>
    );
  }
}

export default connect(
  state => ({
    services: state.services
  })
)(SelectServicesScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  blank: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    width: 100,
    height: 100,
    resizeMode: 'contain'
  },
  message: {
    marginTop: 40,
    width: 200,
    textAlign: 'center',
    fontSize: 20,
    color: theme.color.accentLight
  }
});