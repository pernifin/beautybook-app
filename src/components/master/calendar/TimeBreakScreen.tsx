import React, { Component } from 'react';
import { View, StyleSheet, Platform } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { saveTimeBlock, removeTimeBlock } from 'src/actions';
import { minutesToTime, formatDate, timeToMinutes } from 'src/utils';

import { ITimeBlock } from '@/common/DataTypes';
import { MainButton, SecondaryButton, SingleHeaderButton } from '@/common/buttons';
import KeyboardAvoidScrollView from '@/common/shims/KeyboardAvoidScrollView';
import Form from '@/common/containers/Form';
import { TextInputField, IntervalField, PickerField, DateField } from '@/common/formFields';

export type Props = {
  navigation: NavigationStackProp;
  timeBlock: ITimeBlock;
  saveTimeBlock: (timeBlock: ITimeBlock, force?: boolean) => Promise<any>;
  removeTimeBlock: (timeBlock: ITimeBlock) => Promise<any>;
  warningMessage: string;
  querying: boolean;
}

export type State = {
  timeBlock: ITimeBlock;
}

const emptyTimeBlock = (datetime): ITimeBlock => {
  const [date, time] = datetime.split(' ');
  const timeFrom = timeToMinutes(time);

  return {
    date,
    timeFrom,
    timeTo: timeFrom + 60
  };
}

export class TimeBreakScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    headerLeft: ({ onPress }) => (
      <SingleHeaderButton
        label={Platform.OS === 'ios' && '#common.cancel'}
        iconName={Platform.OS === 'android' && 'close'}
        onPress={() => navigation.popToTop()}
      />
    ),
    headerBackTitle: t('#common.back'),
    title: navigation.getParam('id') ? t('#calendar.editRecord') : t('#calendar.newRecord'),
    headerRight: Platform.OS === 'ios' && (
      <SingleHeaderButton
        label='#common.save'
        bold
        enabled={navigation.getParam('saveEnabled', true)}
        onPress={navigation.getParam('saveOnPress')}
      />
    )
  });

  constructor(props: Props) {
    super(props);
    this.update = this.update.bind(this);
    this.save = this.save.bind(this);
    this.delete = this.delete.bind(this);

    this.state = {
      timeBlock: props.timeBlock
    };

    props.navigation.setParams({
      saveEnabled: this.isFormValid,
      saveOnPress: this.save
    });
  }

  componentDidUpdate(prevProps, prevState: State) {
    if (prevState.timeBlock !== this.state.timeBlock) {
      this.props.navigation.setParams({ saveEnabled: this.isFormValid });
    }
  }

  get isFormValid() {
    const { timeBlock } = this.state;
    const valid = !this.props.querying && timeBlock.timeFrom < timeBlock.timeTo;

    return Boolean(valid);
  }

  update(values: Partial<ITimeBlock>) {
    const { timeBlock } = this.state;

    this.setState({
      timeBlock: {
        ...timeBlock,
        ...values
      }
    });
  }

  async save(force?: boolean) {
    const result = await this.props.saveTimeBlock(this.state.timeBlock, force);

    if (result.success) {
      this.props.navigation.popToTop();
    } else if (result.warning) {
      this.props.navigation.navigate('Modal', {
        message: this.props.warningMessage,
        icon: 'calendar',
        closeLabel: '#common.cancel',
        confirmLabel: '#common.save',
        onConfirmPress: () => this.save(true)
      });
    }
  }

  async delete() {
    const result = await this.props.removeTimeBlock(this.state.timeBlock);
    if (result.success) {
      this.props.navigation.popToTop();
    }
  }
  
  render() {
    const { timeBlock } = this.state;
    if (!timeBlock) {
      return null;
    }

    return (
      <KeyboardAvoidScrollView>
        <Form style={styles.form}>
          <PickerField label='#label.date' value={formatDate(timeBlock.date, true)}>
            <DateField
              value={timeBlock.date}
              onValueChange={date => this.update({ date })}
            />
          </PickerField>
          <PickerField
            label='#label.time'
            value={`${minutesToTime(timeBlock.timeFrom)} — ${minutesToTime(timeBlock.timeTo)}`}
          >
            <IntervalField
              interval={timeBlock}
              onIntervalChange={interval => this.update(interval)}
            />
          </PickerField>
          <TextInputField
            label='#label.note'
            value={timeBlock.note}
            onValueChange={note => this.update({ note })}
            placeholder='#label.addNote'
            multiline
          />
        </Form>

        {Platform.OS === 'android' && (
          <MainButton enabled={!this.props.querying} label='#common.save' onPress={this.save} />
        )}

        {timeBlock.id && <SecondaryButton label='#calendar.unblockTime' onPress={this.delete} />}
      </KeyboardAvoidScrollView>
    )
  }
}

export default connect(
  (state, ownProps) => {
    const recordId = ownProps.navigation.getParam('id');

    const timeBlock = recordId
      ? state.calendar.records[recordId]
      : emptyTimeBlock(ownProps.navigation.getParam('datetime'));

    return {
      timeBlock,
      warningMessage: state.warningMessage,
      querying: state.querying
    };
  },
  {
    saveTimeBlock,
    removeTimeBlock
  }
)(TimeBreakScreen);

export const styles = StyleSheet.create({
  form: {
    paddingVertical: 10
  }
});
