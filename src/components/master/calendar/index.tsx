import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import t from 'src/Lang';
import { color } from 'src/theme';

import CalendarScreen from './CalendarScreen';
import AppointmentViewScreen from './AppointmentViewScreen';
import AppointmentEditScreen from './AppointmentEditScreen';
import TimeBreakScreen from './TimeBreakScreen';
import SelectServicesScreen from './SelectServicesScreen';

export default createStackNavigator({
  CalendarStack: createStackNavigator({
    CalendarView: CalendarScreen,
    AppointmentView: AppointmentViewScreen
  }, {
    navigationOptions: {
      header: null
    },
    defaultNavigationOptions: () => ({
      headerTintColor: Platform.OS === 'ios' ? color.primary : color.white,
      headerTitleStyle: { color: color.black },
    })
  }),
  AppointmentForm: createStackNavigator({
    AppointmentEdit: AppointmentEditScreen,
    SelectServices: SelectServicesScreen
  }, {
    navigationOptions: {
      header: null
    },
    defaultNavigationOptions: () => ({
      headerTintColor: Platform.OS === 'ios' ? color.primary : color.white,
      headerTitleStyle: { color: color.black },
    })
  }),
  TimeBreakForm: TimeBreakScreen
}, {
  mode: 'modal',
  navigationOptions: ({ navigation }) => ({
    tabBarVisible: navigation.state.index === 0,
    tabBarLabel: t('#nav.calendar')
  })
});
