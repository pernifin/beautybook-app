import React, { Component } from 'react';
import { Platform, StyleSheet } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';
import { isNil, padStart, capitalize, range } from 'lodash';
import moment from 'moment';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { saveWorkWeek } from 'src/actions';

import { IInterval } from '@/common/DataTypes';
import { SingleHeaderButton, MainButton } from '@/common/buttons';
import KeyboardAvoidScrollView from '@/common/shims/KeyboardAvoidScrollView';
import Span from '@/common/Span';
import Form from '@/common/containers/Form';
import PickerField from '@/common/formFields/PickerField';

import WorkTime from './views/WorkTime';

export type WeekItem = IInterval & {
  weekday: number;
}

export type Props = {
  navigation: NavigationStackProp;
  workWeek: { [weekday: string]: IInterval };
  saveWorkWeek: (weekData: WeekItem[]) => Promise<any>;
  querying: boolean;
};

export type State = {
  weekSchedule: WeekItem[];
}

function minutesToTime(minutes) {
  return `${padStart(Math.floor(minutes / 60).toString(), 2, '0')}:${padStart((minutes % 60).toString(), 2, '0')}`;
}

export class WeekScheduleScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: t('#schedule.weekHeader'),
    headerRight: Platform.OS === 'ios' && (
      <SingleHeaderButton
        label='#common.done'
        enabled={navigation.getParam('saveEnabled', true)}
        onPress={navigation.getParam('save')}
      />
    )
  });

  constructor(props: Props) {
    super(props);
    this.save = this.save.bind(this);

    props.navigation.setParams({ save: this.save });

    this.state = {
      weekSchedule: range(1, 8).map(
        weekday => props.workWeek[weekday]
          ? { ...props.workWeek[weekday], weekday }
          : { weekday, isWorking: false })
    };
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.querying !== this.props.querying) {
      this.props.navigation.setParams({
        saveEnabled: !this.props.querying
      });
    }
  }

  async save() {
    await this.props.saveWorkWeek(this.state.weekSchedule);
    this.props.navigation.goBack();
  }

  updateWeekSchedule(interval: IInterval, weekday: number) {
    const weekSchedule = [...this.state.weekSchedule];
    weekSchedule[weekday - 1] = { ...interval, weekday };

    this.setState({ weekSchedule });
  }

  renderWeekday = (item: WeekItem) => {
    const interval = {
      timeFrom: isNil(item.timeFrom) ? 540 : item.timeFrom,
      timeTo: isNil(item.timeTo) ? 1080 : item.timeTo
    };

    return (
      <PickerField
        key={item.weekday}
        openHeight={200}
        label={capitalize(moment().isoWeekday(item.weekday).format('dddd'))}
        value={item.isWorking ? `${minutesToTime(interval.timeFrom)} — ${minutesToTime(interval.timeTo)}` : '#schedule.dayOff'}
      >
        <WorkTime
          enabled={!item.isWorking}
          interval={interval}
          onValueChange={(interval, enabled) => this.updateWeekSchedule({ ...interval, isWorking: !enabled }, item.weekday)}
        />
      </PickerField>
    );
  }

  render() {
    return (
      <KeyboardAvoidScrollView>
        <Span style={styles.info}>#schedule.weekInfo</Span>
        <Form style={styles.form}>
          {this.state.weekSchedule.map(this.renderWeekday)}
        </Form>

        {Platform.OS === 'android' && (
          <MainButton enabled={!this.props.querying} label='#common.save' onPress={this.save} />
        )}
      </KeyboardAvoidScrollView>
    )
  }
}

export default connect(
  state => ({
    workWeek: state.schedule.week,
    querying: state.querying
  }),
  {
    saveWorkWeek
  }
)(WeekScheduleScreen);

export const styles = StyleSheet.create({
  info: {
    marginVertical: 20,
    marginHorizontal: 30,
    textAlign: 'center',
    fontSize: 16,
    color: '#666'
  },
  form: {
    marginBottom: 10
  }
});