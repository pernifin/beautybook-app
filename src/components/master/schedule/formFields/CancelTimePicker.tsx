import React from 'react';
import { StyleSheet } from 'react-native';

import t from 'src/Lang';
import { formatLongPeriod } from 'src/utils';
import Picker from '@/common/shims/Picker';

export type Props = {
  value: number;
  onValueChange: (val: number) => void;
}

function getItems() {
  return [0, 1, 2, 4, 8, 24, 48, 72, 96, 168].map(time => {
    return {
      label: time === 0 ? t('#schedule.nolimit') : formatLongPeriod(time),
      value: time
    };
  });
}

export default function CancelTimePicker(props: Props) {
  return (
    <Picker
      data={getItems()}
      value={props.value}
      onValueChange={props.onValueChange}
      itemStyle={styles.item}
    />
  );
}

export const styles = StyleSheet.create({
  item: {
    fontSize: 18
  }
});