import React from 'react';
import { View, Switch, StyleSheet, Platform } from 'react-native';

import { color } from 'src/theme';
import Span from '@/common/Span';

export type Props = {
  label: string;
  value: boolean;
  onValueChange?(value: boolean): void;
}

export default function LabeledSwitch(props: Props) {
  return (
    <View style={styles.container}>
      <Span>{props.label}</Span>
      <Switch
        value={props.value}
        onValueChange={props.onValueChange}
        thumbColor={Platform.OS === 'android' ? '#FFFFFF' : undefined}
        trackColor={{ true: color.primary, false: color.inactive }}
        ios_backgroundColor={color.inactive}
        style={styles.switch}
      />
    </View>
  );
}

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  switch: {
    top: 2,
    marginLeft: 5,
    marginBottom: 5,
    transform: [
      { scale: Platform.OS === 'ios' ? 0.75 : 1 }
    ]
  }
});