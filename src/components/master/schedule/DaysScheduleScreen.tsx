import React, { Component } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';
import { fromPairs, merge } from 'lodash';
import moment from 'moment';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { saveWorkdays } from 'src/actions';

import { IInterval, ISchedule } from '@/common/DataTypes';
import { HeaderButtonContainer, HeaderButton } from '@/common/buttons';
import MonthCalendar from '@/common/elements/MonthCalendar';

import CalendarDay from './views/CalendarDay';
import TimePanel from './views/TimePanel';

export type WorkDateTime = {
  dates: string[];
  workTime: IInterval;
  breakTime: IInterval;
  force?: boolean;
}

export type Props = {
  navigation: NavigationStackProp;
  workdays: ISchedule;
  overlapDays: string[];
  startDate: string;
  saveWorkdays: (workData: WorkDateTime) => Promise<any>;
  warningMessage: string;
  querying: boolean;
};

export type State = {
  selectedDates: string[];
}

export class DaysScheduleScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: t('#schedule.dateHeader'),
    headerBackTitle: t('#common.back'),
    headerRight: (
      <HeaderButtonContainer>
        <HeaderButton iconName='week-schedule' onPress={() => navigation.navigate('WeekSchedule')} />
        <HeaderButton iconName='settings' onPress={() => navigation.navigate('WorkSettings')} />
      </HeaderButtonContainer>
    )
  });

  constructor(props: Props) {
    super(props);
    this.onDayPress = this.onDayPress.bind(this);
    this.saveSchedule = this.saveSchedule.bind(this);

    this.state = {
      selectedDates: []
    };
  }

  workDataCache: WorkDateTime;

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.workdays !== this.props.workdays) {
      this.setState({ selectedDates: [] });
    }
  }

  async saveSchedule(timeData: Partial<WorkDateTime>) {
    const { workTime, breakTime } = timeData;
    if (this.state.selectedDates.length === 0
        || (workTime && workTime.timeFrom >= workTime.timeTo)
        || (breakTime && breakTime.timeFrom >= breakTime.timeTo)
      ) {
      return;
    }

    const workData = {
      dates: this.state.selectedDates,
      workTime: timeData.workTime,
      breakTime: timeData.breakTime
    };

    const result = await this.props.saveWorkdays(workData);

    if (result.warning) {
      this.props.navigation.navigate('Modal', {
        message: this.props.warningMessage,
        icon: 'calendar',
        closeLabel: '#common.cancel',
        confirmLabel: '#common.save',
        onConfirmPress: () => this.forceSaveSchedule(workData)
      });
    }
  }

  async forceSaveSchedule(workData: WorkDateTime) {
    const result = await this.props.saveWorkdays({ ...workData, force: true });
    if (result.warning) {
      this.props.navigation.navigate('Modal', {
        message: this.props.warningMessage,
        icon: 'calendar',
        closeLabel: '#common.close'
      });
    }
  }

  getMarkedDates() {
    const selectedDates = fromPairs(this.state.selectedDates.map(date => [date, { selected: true }]));
    const overlapDates = fromPairs(this.props.overlapDays.map(date => [date, { overlap: true }]));

    return merge({}, this.props.workdays, selectedDates, overlapDates);
  }

  onDayPress(date: string) {
    const selectedDates = this.state.selectedDates.includes(date)
      ? this.state.selectedDates.filter(d => d !== date)
      : this.state.selectedDates.concat(date);

    this.setState({ selectedDates });
  }

  render() {
    return (
      <View style={styles.container}>
        <MonthCalendar
          startDate={this.props.startDate}
          markedDates={this.getMarkedDates()}
          monthHeight={425}
          dayComponent={CalendarDay as any}
          onDayPress={this.onDayPress}
        />

        <TimePanel enabled={!this.props.querying} onSave={this.saveSchedule} />
      </View>
    );
  }
}

export default connect(
  state => ({
    workdays: state.schedule.days,
    overlapDays: state.schedule.overlaps,
    startDate: state.schedule.startDate,
    warningMessage: state.warningMessage,
    querying: state.querying
  }),
  {
    saveWorkdays
  }
)(DaysScheduleScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
