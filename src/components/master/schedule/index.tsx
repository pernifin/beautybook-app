import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import t from '../../../Lang';
import { color } from '../../../theme';

import DaysScheduleScreen from './DaysScheduleScreen';
import WeekScheduleScreen from './WeekScheduleScreen';
import WorkSettingsScreen from './WorkSettingsScreen';

export default createStackNavigator({
  DaysSchedule: DaysScheduleScreen,
  WeekSchedule: WeekScheduleScreen,
  WorkSettings: WorkSettingsScreen
}, {
  navigationOptions: () => ({
    tabBarLabel: t('#nav.schedule')
  }),
  defaultNavigationOptions: () => ({
    headerTintColor: Platform.OS === 'ios' ? color.primary : color.white,
    headerTitleStyle: { color: color.black },
  })
});
