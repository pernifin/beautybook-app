import React, { Component } from 'react';
import { Platform, StyleSheet } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { saveWorkSettings } from 'src/actions';
import { formatLongPeriod } from 'src/utils';

import { IScheduleSettings } from '@/common/DataTypes';
import { SingleHeaderButton, MainButton } from '@/common/buttons';
import KeyboardAvoidScrollView from '@/common/shims/KeyboardAvoidScrollView';
import Form from '@/common/containers/Form';
import { PickerField, TextInputField } from '@/common/formFields';

import CancelTimePicker from './formFields/CancelTimePicker';

export type Props = {
  navigation: NavigationStackProp;
  settings: IScheduleSettings;
  saveWorkSettings: (settins: IScheduleSettings) => Promise<any>;
  querying: boolean;
}

export type State = {
  settings: IScheduleSettings;
};

export class WorkSettingsScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: t('#schedule.settingsHeader'),
    headerRight: Platform.OS === 'ios' && (
      <SingleHeaderButton
        label='#common.save'
        enabled={navigation.getParam('saveEnabled', true)}
        onPress={navigation.getParam('save')}
      />
    )
  });

  constructor(props: Props) {
    super(props);
    this.save = this.save.bind(this);

    props.navigation.setParams({ save: this.save });

    this.state = {
      settings: this.props.settings
    };
  }
  
  componentDidUpdate(prevProps: Props) {
    if (prevProps.querying !== this.props.querying) {
      this.props.navigation.setParams({
        saveEnabled: !this.props.querying
      });
    }
  }

  updateSettings(option: Partial<IScheduleSettings>) {
    const settings = { ...this.state.settings, ...option };

    settings.recordPeriod = Math.max(Math.min(settings.recordPeriod, 60), 1);
    settings.cancelForbid = Math.max(Math.min(settings.cancelForbid, 168), 0);

    this.setState({ settings });
  }

  async save() {
    await this.props.saveWorkSettings(this.state.settings);
    this.props.navigation.goBack();
  }

  render() {
    const { recordPeriod = 0, cancelForbid = 0 } = this.state.settings;
    return (
      <KeyboardAvoidScrollView>
        <Form footer='#schedule.recordPeriodHint' style={styles.form}>
          <TextInputField
            label='#schedule.recordPeriod'
            align='right'
            changeEvent='end'
            value={String(recordPeriod)}
            onValueChange={recordPeriod => this.updateSettings({ recordPeriod: +recordPeriod })}
            append={{ key: '#datetime.days', count: recordPeriod }}
            keyboardType='number-pad'
          />
        </Form>
        <Form footer='#schedule.cancelForbidHint' style={styles.form}>
          <PickerField
            label='#schedule.cancelForbid'
            align='right'
            value={cancelForbid ? formatLongPeriod(cancelForbid) : '#schedule.nolimit'}
          >
            <CancelTimePicker
              value={cancelForbid}
              onValueChange={cancelForbid => this.updateSettings({ cancelForbid })}
            />
          </PickerField>
        </Form>

        {Platform.OS === 'android' && (
          <MainButton enabled={!this.props.querying} label='#common.save' onPress={this.save} />
        )}
      </KeyboardAvoidScrollView>
    );
  }
}

export default connect(
  state => ({
    settings: state.schedule.settings,
    querying: state.querying
  }),
  {
    saveWorkSettings
  }
)(WorkSettingsScreen);

export const styles = StyleSheet.create({
  form: {
    marginVertical: 15
  }
});