import React from 'react';
import { View, TouchableWithoutFeedback, StyleSheet } from 'react-native';
import { DayComponentProps } from 'react-native-calendars';
import { isEqual } from 'lodash';

import { minutesToTime } from 'src/utils';
import Gradient from '@/common/containers/Gradient';
import Span from '@/common/Span';

export type DateMark = {
  selected: boolean;
  overlap: boolean;
  isWorking: boolean;
  timeFrom: number;
  timeTo: number;
}

export type Props = {
  marking: DateMark;
} & DayComponentProps;

export default class CalendarDay extends React.Component<Props> {
  constructor(props) {
    super(props);
    this.onDayPress = this.onDayPress.bind(this);
  }

  shouldComponentUpdate(nextProps) {
    return !isEqual(this.props.marking, nextProps.marking);
  }

  onDayPress() {
    this.props.onPress(this.props.date);
  }

  renderTime(marking, time) {
    return (
      <Span style={[
        styles.timeText,
        marking.selected && styles.timeTextSelected,
        marking.overlap && styles.textOverlap
      ]}>
        {minutesToTime(time)}
      </Span>
    );
  }

  render() {
    const marking = Array.isArray(this.props.marking) || !this.props.marking ? {} as DateMark : this.props.marking;

    return (
      <TouchableWithoutFeedback onPress={this.onDayPress}>
        <View style={styles.container}>
          <Gradient active={marking.selected} style={styles.day}>
            <Span style={[
              styles.dayText,
              marking.selected && styles.dayTextSelected,
              marking.overlap && styles.textOverlap
            ]}>
              {String(this.props.children)}
            </Span>
          </Gradient>
          
          {marking.isWorking && (
            <View style={styles.time}>
              {this.renderTime(marking, marking.timeFrom)}
              {this.renderTime(marking, marking.timeTo)}
            </View>
          )}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 55,
    alignItems: 'center'
  },
  day: {
    height: 30,
    aspectRatio: 1,
    paddingVertical: 8,
    marginBottom: 0,
    borderRadius: 15
  },
  dayText: {
    fontSize: 15,
    fontWeight: '500',
    lineHeight: 15,
    textAlign: 'center',
    color: '#333'
  },
  dayTextSelected: {
    color: '#fff'
  },
  time: {
    alignItems: 'center'
  },
  timeText: {
    fontSize: 9,
    color: '#888'
  },
  timeTextSelected: {
    fontWeight: '500',
    color: '#333'
  },
  textOverlap: {
    color: 'red'
  }
});