import React, { Component } from 'react';
import { View, StyleSheet, Platform } from 'react-native';

import { IInterval } from '@/common/DataTypes';
import { MainButton } from '@/common/buttons';
import SlidablePanel from '@/common/containers/SlidablePanel';
import TabView from '@/common/containers/TabView';
import { IntervalField } from '@/common/formFields';

import LabeledSwitch from '../formFields/LabeledSwitch';
import WorkTime from './WorkTime';

export type Props = {
  enabled?: boolean;
  onSave: (timeData: { workTime: IInterval; breakTime: IInterval }) => void;
}

export type State = {
  sectionIndex: number;
  dayoff: boolean;
  workTime: IInterval;
  breakTimeOn: boolean;
  breakTime: IInterval;
}

export default class TimePanel extends Component<Props, State> {
  static defaultProps = {
    enabled: true
  };

  readonly state: State = {
    sectionIndex: 0,
    // Need to retreive from user storage
    dayoff: false,
    workTime: { timeFrom: 540, timeTo: 1080 },
    breakTimeOn: false,
    breakTime: { timeFrom: 720, timeTo: 720 },
  };

  save = () => {
    this.props.onSave({
      workTime: this.state.dayoff ? null : this.state.workTime,
      breakTime: this.state.breakTimeOn ? this.state.breakTime : null
    });
  }

  render() {
    return (
      <SlidablePanel expanded range={{ top: Platform.OS === 'ios' ? 333 : 348, bottom: 35 }}>
        <TabView
          tabs={['#schedule.workDay', '#schedule.timeBreak']}
          enabled={!this.state.dayoff}
          tabsStyle={styles.tabs}
        >
          <View>
            <WorkTime
              enabled={this.state.dayoff}
              interval={this.state.workTime}
              onValueChange={(interval, enabled) =>
                this.setState({ workTime: interval, dayoff: enabled, breakTimeOn: enabled && this.state.breakTimeOn })
              }
            />
          </View>
          <View>
            <LabeledSwitch
              label='#schedule.addTimeBreak'
              value={this.state.breakTimeOn}
              onValueChange={breakTimeOn => this.setState({ breakTimeOn })}
            />
            {this.state.breakTimeOn && (
              <IntervalField
                interval={this.state.breakTime}
                onIntervalChange={interval => this.setState({ breakTime: interval })}
                style={styles.interval}
              />
            )}
          </View>
        </TabView>

        <MainButton short thin enabled={this.props.enabled} label='#common.save' onPress={this.save} />
      </SlidablePanel>
    )
  }
}

export const styles = StyleSheet.create({
  panelContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 300
  },
  tabs: {
    marginHorizontal: 40,
    height: 40
  },
  interval: {
    height: Platform.OS === 'ios' ? 150 : 170
  }
});