import React from 'react';
import { View, StyleSheet, Platform } from 'react-native';

import { IInterval } from '@/common/DataTypes';
import Overlay from '@/common/containers/Overlay';
import { IntervalField } from '@/common/formFields';

import LabeledSwitch from '../formFields/LabeledSwitch';

export type Props = {
  enabled: boolean;
  interval: IInterval;
  onValueChange: (interval: IInterval, enabled: boolean) => void;
}

export default function WorkTime(props: Props) {
  return (
    <View>
      <LabeledSwitch
        label='#schedule.makeDayOff'
        value={props.enabled}
        onValueChange={enabled => props.onValueChange(props.interval, enabled)}
      />

      <Overlay enabled={props.enabled}>
        <IntervalField
          interval={props.interval}
          onIntervalChange={interval => props.onValueChange(interval, props.enabled)}
          style={styles.interval}
        />
      </Overlay>
    </View>
  );
}

export const styles = StyleSheet.create({
  interval: {
    height: Platform.OS === 'ios' ? 150 : 170
  }
});