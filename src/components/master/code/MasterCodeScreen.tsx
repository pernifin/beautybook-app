import React from 'react';
import { View, Dimensions, SafeAreaView, PixelRatio, StyleSheet } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import Config from 'react-native-config';
import QRCode from 'react-native-qrcode-svg';
import { connect } from 'react-redux';

import t from 'src/Lang';
import { color } from 'src/theme';
import { loadMasterCode } from 'src/actions';

import Span from '@/common/Span';
import TabView from '@/common/containers/TabView';

export type Props = {
  navigation: NavigationStackProp;
  masterCode: string;
  masterId: string;
  loadMasterCode: () => Promise<any>;
}

const { width } = Dimensions.get('window');

export class MasterCodeScreen extends React.Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    tabBarLabel: t('#nav.qrcode')
  });

  appLink = Config.APP_INSTALL_URL;

  componentDidMount() {
    this.props.loadMasterCode();
  }

  render() {
    const { masterCode, masterId } = this.props;

    const code = this.props.masterCode
      ? JSON.stringify({ mid: masterId, cid: masterCode })
      : null;

    return (
      <SafeAreaView style={styles.container}>
        <TabView tabs={['#code.application', '#code.master']} style={styles.tabs}>
          <View style={styles.section}>
            <Span style={styles.info}>#code.applicationInfo</Span>
            <View style={styles.code}>
              <QRCode value={this.appLink} size={width * (PixelRatio.get() >= 2 ? 0.75 : 0.7)} ecl='L' />
            </View>
          </View>

          <View style={styles.section}>
            <Span style={styles.info}>#code.masterInfo</Span>
            <View style={styles.code}>
              {code && <QRCode value={code} size={width * (PixelRatio.get() >= 2 ? 0.75 : 0.7)} ecl='L' />}
            </View>
            <Span style={styles.warn}>#code.masterWarn</Span>
          </View>
        </TabView>
      </SafeAreaView>
    );
  }
}

export default connect(
  state => ({
    masterCode: state.masterCode,
    masterId: state.profile.id
  }),
  {
    loadMasterCode
  }
)(MasterCodeScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white
  },
  tabs: {
    paddingTop: 20
  },
  section: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal: 16
  },
  code: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  info: {
    paddingVertical: 10,
    paddingHorizontal: 12,
    textAlign: 'center',
    fontSize: PixelRatio.get() >= 2 ? 16 : 14,
    color: color.grey
  },
  warn: {
    marginBottom: 15,
    paddingHorizontal: 10,
    textAlign: 'center',
    fontSize: 14,
    color: color.primary
  }
});