import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';

import t from 'src/Lang';
import * as theme from 'src/theme'
import { finishAppointment } from 'src/actions'
import { minutesToTime, formatDuration, formatDatetimeLong, formatPrice } from 'src/utils';

import { NotificationAccess } from '@/common/modals/access';
import KeyboardAvoidScrollView from '@/common/shims/KeyboardAvoidScrollView';
import { MainButton } from '@/common/buttons';
import Span from '@/common/Span';
import Form from '@/common/containers/Form';
import { DurationField, TextInputField, PickerField, StaticTextField } from '@/common/formFields';
import SelectedServicesList from '@/common/elements/SelectedServicesList';

export type Props = {
  navigation: NavigationStackProp;
  appointment: any;
  finishAppointment: (note: string, reminder: number) => Promise<any>;
  querying: boolean;
}

export type State = {
  note: string;
  remindIn: number;
}

export class FinishAppointmentScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: t('#masters.appointment')
  });

  constructor(props: Props) {
    super(props);
    this.finishAppointment = this.finishAppointment.bind(this);

    this.state = {
      note: '',
      remindIn: 120
    };
  }

  async finishAppointment() {
    await NotificationAccess(this.props.navigation, '#access.clientNotifications');

    const result = await this.props.finishAppointment(this.state.note, this.state.remindIn);
    if (result.success) {
      this.props.navigation.popToTop();
      this.props.navigation.navigate('Appointments');
    }
  }

  render() {
    const { appointment } = this.props;
    const { date, time } = appointment.selectedTime;

    return (
      <View style={styles.container}>
        <KeyboardAvoidScrollView keyboardShouldPersistTaps='handled'>
          <View style={styles.datetime}>
            <Span style={styles.headerText}><Span>#label.dateTime</Span>:</Span>
            <Span style={styles.bigText}>
              {formatDatetimeLong(`${date} ${minutesToTime(time)}`)}
            </Span>
          </View>
          <Form>
            <SelectedServicesList label services={appointment.selectedServices} />
            <StaticTextField
              bold
              align='right'
              label='#label.duration'
              value={formatDuration(appointment.duration)}
            />
            <StaticTextField
              bold
              align='right'
              label='#label.price'
              value={formatPrice(appointment.price)}
            />
            <PickerField label='#label.reminder' align='right' value={formatDuration(this.state.remindIn)}>
              <DurationField
                min={60} max={480} step={60}
                value={this.state.remindIn}
                onValueChange={remindIn => this.setState({ remindIn })}
              />
            </PickerField>
            <TextInputField
              label='#label.note'
              placeholder='#masters.noteHint'
              value={this.state.note}
              onValueChange={note => this.setState({ note })}
              multiline
            />
          </Form>
        </KeyboardAvoidScrollView>

        <MainButton
          enabled={!this.props.querying}
          label='#masters.finishAppointment'
          onPress={this.finishAppointment}
        />
      </View>
    );
  }
}

export default connect(
  state => ({
    appointment: state.appointmentWizard,
    querying: state.querying
  }),
  {
    finishAppointment
  }
)(FinishAppointmentScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  datetime: {
    paddingVertical: 20,
    paddingHorizontal: 30
  },
  headerText: {
    marginBottom: 10,
    fontSize: 18,
    fontWeight: '600'
  },
  bigText: {
    fontSize: 18
  }
});