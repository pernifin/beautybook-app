import React from 'react';
import { View, Image, Dimensions, StyleSheet, Platform } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { RNCamera } from 'react-native-camera';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import { throttle } from 'lodash';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { addMaster, setError } from 'src/actions';

import { SingleHeaderButton } from '@/common/buttons';
import { CameraAccess } from '@/common/modals/access';
import Span from '@/common/Span';

type MasterCode = {
  mid: string;
  cid: string;
}

export type Props = {
  navigation: NavigationStackProp;
  addMaster(cpde: MasterCode): Promise<any>;
  setError(message: string): void;
}

export type State = {
  permission: boolean;
  error: string;
}

const { width } = Dimensions.get('window');

export class AddMasterScreen extends React.Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    headerLeft: ({ onPress }) => (
      <SingleHeaderButton
        label={Platform.OS === 'ios' && '#common.cancel'}
        iconName={Platform.OS === 'android' && 'close'}
        onPress={() => navigation.popToTop()}
      />
    ),
    title: t('#masters.scanCode')
  });

  constructor(props) {
    super(props);
    this.onCodeRead = this.onCodeRead.bind(this);

    this.state = {
      permission: false,
      error: null
    };
  }

  async componentDidMount() {
    this.setState({
      permission: await CameraAccess(this.props.navigation)
    });
  }

  async onCodeRead(code) {
    try {
      const result = await this.props.addMaster(JSON.parse(code.data));
      if (result.success) {
        this.props.navigation.navigate('MasterList');
      } else {
        this.setState({ error: result.error });
      }
    } catch (error) {
      this.setState({ error: '#error.wrongCode' });
    }
  }
  onBarCodeRead = throttle(this.onCodeRead.bind(this), 3000);

  render() {
    if (!this.state.permission) {
      return (
        <View style={styles.container}>
          <Image source={require('assets/images/icons/camera.png')} style={styles.icon} />
          <Span style={styles.accessDenied}>#masters.noPermission</Span>
        </View>
      );
    }

    return (
      <RNCamera
        type={RNCamera.Constants.Type.back}
        captureAudio={false}
        barCodeTypes={[RNCamera.Constants.BarCodeType.qr]}
        onBarCodeRead={this.onBarCodeRead}
        style={styles.container}
      >
        <LinearGradient colors={['#0008', '#0000']} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }} style={styles.hint}>
          <Span style={styles.hintText}>#masters.cameraHint</Span>
        </LinearGradient>
        
        <View style={styles.boxContainer}>
          <View style={[styles.border, styles.borderTopLeft]} />
          <View style={[styles.border, styles.borderTopRight]} />
          <View style={[styles.border, styles.borderBottomLeft]} />
          <View style={[styles.border, styles.borderBottomRight]} />
        </View>

        {this.state.error && <Span style={styles.error}>{this.state.error}</Span>}
      </RNCamera>
    );
  }
}

export default connect(
  state => ({
    masterCode: state.masterCode,
    masterId: state.profile.id
  }),
  {
    addMaster,
    setError
  }
)(AddMasterScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
    top: -30,
    marginBottom: 30
  },
  accessDenied: {
    textAlign: 'center',
    fontSize: 20,
    color: theme.color.accentLight
  },
  hint: {
    position: 'absolute',
    top: 0,
    width: '100%',
    padding: 25,
    paddingBottom: 60
  },
  hintText: {
    fontSize: 16,
    textAlign: 'center',
    color: '#FFF'
  },
  error: {
    position: 'absolute',
    bottom: 40,
    width: '100%',
    paddingHorizontal: 30,
    fontSize: 16,
    textAlign: 'center',
    color: theme.color.error
  },
  boxContainer: {
    width: width,
    aspectRatio: 1,
    padding: width / 8,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    alignContent: 'space-between'
  },
  border: {
    borderWidth: 4,
    borderColor: '#FFFC',
    width: width / 8,
    aspectRatio: 1
  },
  borderTopLeft: {
    marginRight: width / 2,
    borderBottomWidth: 0,
    borderRightWidth: 0,
  },
  borderTopRight: {
    borderBottomWidth: 0,
    borderLeftWidth: 0,
  },
  borderBottomLeft: {
    borderTopWidth: 0,
    borderRightWidth: 0,
  },
  borderBottomRight: {
    borderTopWidth: 0,
    borderLeftWidth: 0,
  }
});
