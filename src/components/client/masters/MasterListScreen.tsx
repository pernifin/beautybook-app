import React from 'react';
import { View, ScrollView, Image, StyleSheet, Platform } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';

import t from 'src/Lang';
import * as theme from 'src/theme';

import { IProfile } from '@/common/DataTypes';
import Touchable from '@/common/shims/Touchable';
import { SingleHeaderButton, FAB } from '@/common/buttons';
import Span from '@/common/Span';
import SmallAvatar from '@/common/elements/SmallAvatar';

export type Props = {
  navigation: NavigationStackProp;
  masters: IProfile[];
}

export class MasterListScreen extends React.Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    headerBackTitle: null,
    title: t('#masters.myMasters'),
    headerRight: Platform.OS === 'ios' && (
      <SingleHeaderButton iconName='add' onPress={() => navigation.navigate('AddMaster')} />
    )
  });

  openMaster(profile: IProfile) {
    this.props.navigation.navigate('MasterView', { masterId: profile.id });
  }

  renderMasterBlock = (profile: IProfile) => {
    return (
      <Touchable key={profile.id} onPress={() => this.openMaster(profile)} style={styles.masterContainer}>
        <View style={styles.masterBlock}>
          <SmallAvatar profile={profile} />
          <View style={styles.description}>
            <Span raw style={styles.name}>{profile.name}</Span>
            <Span raw style={styles.specialization}>{profile.specialization}</Span>
          </View>
        </View>
      </Touchable>
    );
  }

  render() {
    const { masters = [], navigation } = this.props;

    return (
      <View style={styles.container}>
        {masters.length ? (
          <ScrollView contentContainerStyle={styles.container}>
            {masters.map(this.renderMasterBlock)}
          </ScrollView>
        ) : this.renderBlank()}

        {Platform.OS === 'android' && (
          <FAB iconName='add' onPress={() => navigation.navigate('AddMaster')} />
        )}
      </View>
    );
  }

  renderBlank() {
    return (
      <View style={styles.blank}>
        <Image source={require('assets/images/icons/add-user.png')} style={styles.icon} />
        <Span style={styles.message}>#masters.emptyList</Span>
      </View>
    );
  }
}

export default connect(
  state => ({
    masters: state.masters
  }),
  null
)(MasterListScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15
  },
  blank: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    width: 100,
    height: 100,
    resizeMode: 'contain'
  },
  message: {
    marginTop: 40,
    width: 200,
    textAlign: 'center',
    fontSize: 20,
    color: theme.color.accentLight
  },
  masterContainer: {
    marginHorizontal: 15,
    marginBottom: 10,
    borderRadius: Platform.OS === 'android' ? 3 : 8,
    backgroundColor: '#fff',
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 3,
    shadowOpacity: 0.2,
    elevation: 4
  },
  masterBlock: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
  },
  emptyText: {
    margin: 20,
    fontSize: 15
  },
  description: {
    marginLeft: 20
  },
  name: {
    fontWeight: '600'
  },
  specialization: {
    fontSize: 12,
    color: '#888'
  }
});
