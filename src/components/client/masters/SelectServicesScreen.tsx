import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { selectAppointmentServices } from 'src/actions';

import { IService } from '@/common/DataTypes';
import { MainButton } from '@/common/buttons';
import ServiceList from '@/common/elements/ServiceList';

export type Props = {
  navigation: NavigationStackProp;
  services: IService[];
  selectAppointmentServices: (services: IService[]) => Promise<any>;
}

export type State = {
  selected: IService[];
}

export class SelectServicesScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: t('#masters.services')
  });

  constructor(props: Props) {
    super(props);
    this.selectService = this.selectService.bind(this);
    this.openMasterTimetable = this.openMasterTimetable.bind(this);

    this.state = {
      selected: []
    };
  }

  selectService(service: IService) {
    const selected = this.state.selected.includes(service)
      ? this.state.selected.filter(s => s !== service)
      : this.state.selected.concat(service);

    this.setState({ selected });
  }

  openMasterTimetable() {
    this.props.selectAppointmentServices(this.state.selected);
    this.props.navigation.navigate('SelectTime');
  }

  render() {
    return (
      <View style={styles.container}>
        <ServiceList
          selectable
          services={this.props.services}
          selected={this.state.selected}
          onServicePress={this.selectService}
        />
        <MainButton
          enabled={!!this.state.selected.length}
          label='#common.next'
          onPress={this.openMasterTimetable}
        />
      </View>
    );
  }
}

export default connect(
  state => ({
    services: state.appointmentWizard.services
  }),
  {
    selectAppointmentServices
  }
)(SelectServicesScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});