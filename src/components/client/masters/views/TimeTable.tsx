import React, { Component } from 'react';
import { ScrollView, View, TouchableOpacity, StyleSheet } from 'react-native';
import IonIcon from 'react-native-vector-icons/Ionicons';
import { map, isEmpty } from 'lodash';
import moment from 'moment';

import Span from '@/common/Span';
import TimeSlot from '@/common/elements/TimeSlot';

export type Props = {
  timeSlots: { [date: string]: number[] };
  selectedSlot: { date: string, time: number };
  onSlotSelect: (date: string, time: number) => void;
}

export type State = {
  scrollWidth: number;
  columnWidth: number;
}

export default class TimeTable extends Component<Props, State> {
  scrollRef = React.createRef<ScrollView>();
  scrollOffset = 0;

  constructor(props: Props) {
    super(props);
    this.onLayout = this.onLayout.bind(this);
    this.renderDateSection = this.renderDateSection.bind(this);

    this.state = {
      scrollWidth: 0,
      columnWidth: 0
    };
  }

  onLayout({ nativeEvent }) {
    const { width } = nativeEvent.layout;
    const columnWidth = Math.round(width / Math.floor(width / 60));

    this.setState({
      scrollWidth: width,
      columnWidth
    });
  }

  scrollTo(offset: number) {
    if (this.scrollOffset % this.state.columnWidth > 5) {
      return;
    }

    this.scrollRef.current.scrollTo({
      x: (Math.round(this.scrollOffset / this.state.columnWidth) + offset) * this.state.columnWidth
    });
  }

  render() {
    if (isEmpty(this.props.timeSlots)) {
      return (
        <View style={styles.container}>
          <Span style={styles.emptySlots}>#masters.noTimeSlots</Span>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.scrollTo.bind(this, -1)} style={styles.arrow}>
          <IonIcon name='ios-arrow-back' size={15} color='#888' />
        </TouchableOpacity>

        <ScrollView
          ref={this.scrollRef}
          horizontal
          decelerationRate='fast'
          overScrollMode='never'
          snapToInterval={this.state.columnWidth}
          onLayout={this.onLayout}
          onScroll={({ nativeEvent }) => this.scrollOffset = nativeEvent.contentOffset.x}
          scrollEventThrottle={100}
          showsHorizontalScrollIndicator={false}
          style={styles.slots}
          contentContainerStyle={{ minWidth: this.state.scrollWidth }}
        >
          {map(this.props.timeSlots, this.renderDateSection)}
        </ScrollView>
        
        <TouchableOpacity onPress={this.scrollTo.bind(this, 1)} style={styles.arrow}>
          <IonIcon name='ios-arrow-forward' size={15} color='#888' />
        </TouchableOpacity>
      </View>
    )
  }

  renderDateSection(slots: number[], date: string) {
    return (
      <View key={date} style={[styles.dateCol, { width: this.state.columnWidth }]}>
        <Span style={styles.dateName}>{moment(date).format('D MMM')}</Span>
        <Span style={styles.weekdayName}>{moment(date).format('ddd')}</Span>

        {slots.map(slot => this.renderTimeSlot(slot, date))}
      </View>
    );
  }

  renderTimeSlot(time: number, date: string) {
    const { selectedSlot } = this.props;
    const selected = selectedSlot && selectedSlot.date === date && selectedSlot.time === time;

    return (
      <TimeSlot
        key={time}
        time={time}
        selected={selected}
        onPress={() => this.props.onSlotSelect(date, time)}
        style={styles.timeSlot}
      />
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: 8
  },
  arrow: {
    width: 20,
    height: 40,
    alignItems: 'center'
  },
  emptySlots: {
    marginTop: 10,
    fontSize: 18,
    fontWeight: '600'
  },
  slots: {
    height: '100%'
  },
  dateCol: {
    alignItems: 'center',
    borderColor: 'transparent' // Android scroll has glitches without this
  },
  dateName: {
    fontWeight: '600'
  },
  weekdayName: {
    marginTop: 3,
    marginBottom: 10,
    fontSize: 12,
    color: '#888'
  },
  timeSlot: {
    marginVertical: 3,
    paddingVertical: 2,
    paddingHorizontal: 10
  }
});