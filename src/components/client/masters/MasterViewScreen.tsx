import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { startAppointmentWizard } from 'src/actions';

import { IProfile } from '@/common/DataTypes';
import { MainButton } from '@/common/buttons';
import ProfileView from '@/common/elements/ProfileView';

export type Props = {
  navigation: NavigationStackProp;
  master: IProfile;
  startAppointmentWizard: (masterId: string) => Promise<any>;
}

export class MasterViewScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: t('#masters.master')
  });

  makeAppointment(profile: IProfile) {
    this.props.startAppointmentWizard(profile.id);
    this.props.navigation.navigate('SelectServices');
  }

  render() {
    const { master } = this.props;

    return (
      <View style={styles.container}>
        <ProfileView profile={master} full showActions />
        <MainButton label='#masters.makeAppointment' onPress={() => this.makeAppointment(master)} />
      </View>
    )
  }
}

export default connect(
  (state, ownProps) => {
    const masterId = ownProps.navigation.getParam('masterId');

    return {
      master: state.masters.find(m => m.id === masterId)
    };
  },
  {
    startAppointmentWizard
  }
)(MasterViewScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});