import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import t from '../../../Lang';
import { color } from '../../../theme';

import MasterListScreen from './MasterListScreen';
import AddMasterScreen from './AddMasterScreen';
import MasterViewScreen from './MasterViewScreen';
import SelectServicesScreen from './SelectServicesScreen';
import SelectTimeScreen from './SelectTimeScreen';
import FinishAppointmentScreen from './FinishAppointmentScreen';

export default createStackNavigator({
  MasterList: createStackNavigator({
    MasterList: MasterListScreen,
    MasterView: MasterViewScreen,
    SelectServices: SelectServicesScreen,
    SelectTime: SelectTimeScreen,
    FinishAppointment: FinishAppointmentScreen
  }, {
    navigationOptions: {
      header: null
    },
    defaultNavigationOptions: () => ({
      headerTintColor: Platform.OS === 'ios' ? color.primary : color.white,
      headerTitleStyle: { color: color.black },
    })
  }),
  AddMaster: createStackNavigator({
    AddMaster: AddMasterScreen
  }, {
    navigationOptions: {
      header: null
    }
  })
}, {
  mode: 'modal',
  navigationOptions: ({ navigation }) => ({
    tabBarVisible: navigation.state.index === 0 && navigation.state.routes[0].routes.length === 1,
    tabBarLabel: t('#nav.masters')
  })
});
