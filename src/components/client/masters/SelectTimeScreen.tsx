import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, PixelRatio } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';
import { padStart } from 'lodash';
import moment from 'moment';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { selectTimeRange, selectTimeSlot } from 'src/actions';
import { minutesToTime } from 'src/utils';

import { IInterval } from '@/common/DataTypes';
import { MainButton } from '@/common/buttons';
import Span from '@/common/Span';
import TimeSlot from '@/common/elements/TimeSlot';

import TimeTable from './views/TimeTable';

type TimeSlot = { date: string, time: number };

export type Props = {
  navigation: NavigationStackProp;
  timeSlots: { [date: string]: number[] };
  duration: number;
  ranges: IInterval[];
  selectedRange: number;
  selectedTimeSlot: TimeSlot;
  selectTimeRange: (range: number) => void;
  selectTimeSlot: (timeSlot: TimeSlot) => void;
}

export type State = {
  selectedSlot: TimeSlot;
}

export class SelectTimeScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: t('#masters.dateTimeHeader')
  });

  constructor(props: Props) {
    super(props);
    this.confirmTime = this.confirmTime.bind(this);
    this.selectSlot = this.selectSlot.bind(this);
    this.renderTimeRange = this.renderTimeRange.bind(this);

    this.state = {
      selectedSlot: props.selectedTimeSlot
    };
  }

  confirmTime() {
    this.props.selectTimeSlot(this.state.selectedSlot);
    this.props.navigation.navigate('FinishAppointment');
  }

  selectSlot(date, time) {
    this.setState({
      selectedSlot: { date, time }
    });
  }

  render() {
    const { selectedSlot } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.rangesContainer}>
          {this.props.ranges.map(this.renderTimeRange)}
        </View>
        <TimeTable
          timeSlots={this.props.timeSlots}
          selectedSlot={this.state.selectedSlot}
          onSlotSelect={this.selectSlot}
        />

        {selectedSlot && (
          <View style={styles.confirmBlock}>
            <Span style={styles.confirmLabel}><Span>#masters.selectedTime</Span>:</Span>
            <Span style={styles.confirmDate}>{moment(selectedSlot.date).format('MMMM D, dddd')}</Span>
            <Span style={styles.confirmDate}>
              {minutesToTime(selectedSlot.time)} — {minutesToTime(selectedSlot.time + this.props.duration)}
            </Span>
          </View>
        )}

        <MainButton enabled={!!selectedSlot} label='#common.next' onPress={this.confirmTime} />
      </View>
    );
  }

  renderTimeRange(range, index) {
    const selected = this.props.selectedRange === index;
    const toHours = minutes => padStart(String(minutes / 60), 2, '0');
    const rangeNames = ['#datetime.morning', '#datetime.afternoon', '#datetime.evening'];

    return (
      <TimeSlot
        key={index}
        selected={selected}
        onPress={() => !selected && this.props.selectTimeRange(index)}
        style={styles.range}
      >
        <Span style={[styles.rangeTitle, selected && styles.rangeTitleSelected]}>{rangeNames[index]}</Span>
        <Span style={styles.rangeTime}>{`${toHours(range.timeFrom)}-${toHours(range.timeTo)}`}</Span>
      </TimeSlot>
    );
  }
}

export default connect(
  state => ({
    timeSlots: state.appointmentWizard.timeSlots,
    duration: state.appointmentWizard.duration,
    ranges: state.appointmentWizard.ranges,
    selectedRange: state.appointmentWizard.selectedRange,
    selectedTimeSlot: state.appointmentWizard.selectedTime
  }),
  {
    selectTimeRange,
    selectTimeSlot
  }
)(SelectTimeScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  rangesContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginVertical: 20
  },
  range: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    maxWidth: '30%',
    paddingVertical: 10,
    paddingHorizontal: 4,
    borderRadius: 20
  },
  rangeTitle: {
    fontSize: PixelRatio.get() >= 2 ? 13 : 11,
    color: '#000'
  },
  rangeTitleSelected: {
    color: '#fff'
  },
  rangeTime: {
    fontSize: PixelRatio.get() >= 2 ? 12 : 10,
    color: '#bbb'
  },
  confirmBlock: {
    alignItems: 'center',
    height: 100,
    paddingTop: 15,
    borderTopWidth: 1,
    borderTopColor: '#e0e0e0'
  },
  confirmLabel: {
    fontSize: 16,
    color: '#888'
  },
  confirmDate: {
    marginTop: 5,
    fontSize: 18,
    fontWeight: '600'
  }
});