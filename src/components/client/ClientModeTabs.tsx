import React from 'react';
import { Platform } from 'react-native';
import { createBottomTabNavigator, BottomTabBar } from 'react-navigation-tabs';

import { color } from 'src/theme';

import { avoidKeyboard } from 'src/components/common/shims/KeyboardAvoidContainer';
import Icon from '@/common/Icon';

import MastersStack from './masters';
import AppointmentsStack from './appointments';
import ProfileStack from '@/shared/profile';

const KeyboardAvoidBottomTabBar = avoidKeyboard(BottomTabBar);

export default createBottomTabNavigator({
  Masters: MastersStack,
  Appointments: AppointmentsStack,
  Profile: ProfileStack
}, {
  initialRouteName: 'Masters',
  ...(Platform.OS === 'android' ? { tabBarComponent: props => <KeyboardAvoidBottomTabBar {...props} /> } : {}),
  tabBarOptions: {
    activeTintColor: color.accent,
    inactiveTintColor: color.accentLight
  },
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      const { routeName } = navigation.state;

      let iconName;
      if (routeName === 'Masters') {
        iconName = 'nav-masterlist';
      } else if (routeName === 'Appointments') {
        iconName = 'nav-appointments';
      } else if (routeName === 'Profile') {
        iconName = 'nav-profile';
      }

      return <Icon name={iconName} size={23} color={tintColor} />;
    }
  })
});
