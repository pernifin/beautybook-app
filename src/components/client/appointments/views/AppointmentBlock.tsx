import React from 'react';
import { View, Platform, StyleSheet } from 'react-native';

import { formatDatetimeLong } from 'src/utils';
import { IAppointment } from '@/common/DataTypes';
import Touchable from '@/common/shims/Touchable';
import Span from '@/common/Span';
import SmallAvatar from '@/common/elements/SmallAvatar';

export type Props = {
  appointment: IAppointment;
  onPress: () => void;
}

export default function AppointmentBlock(props: Props) {
  const { appointment, onPress } = props;

  return (
    <Touchable onPress={onPress} style={styles.container}>
      <View style={styles.inner}>
        <View style={styles.master}>
          <SmallAvatar small profile={appointment.master} />
          <Span raw style={styles.masterName}>{appointment.master.name}</Span>
        </View>

        <Span style={styles.datetime}>
          {formatDatetimeLong(appointment.datetime)}
        </Span>

        {appointment.services.map(service => (
          <Span raw key={service.id} style={styles.service}>{service.title}</Span>
        ))}
      </View>
    </Touchable>
  );
}

export const styles = StyleSheet.create({
  container: {
    marginHorizontal: 4,
    marginTop: 4,
    marginBottom: 8,
    borderRadius: Platform.OS === 'android' ? 3 : 8,
    backgroundColor: '#fff',
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 3,
    shadowOpacity: 0.2,
    elevation: 4
  },
  inner: {
    padding: 15
  },
  master: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  masterName: {
    marginLeft: 15,
    fontSize: 16,
    fontWeight: '600'
  },
  datetime: {
    marginVertical: 10,
    fontSize: 16,
    fontWeight: '600'
  },
  service: {
    marginVertical: 4,
    fontSize: 16,
    lineHeight: 20
  }
});