import React, { Component } from 'react';
import { StyleSheet, Platform } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';
import moment from 'moment';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { saveClientAppointment, cancelClientAppointment } from 'src/actions';

import { IAppointment } from '@/common/DataTypes';
import { SingleHeaderButton, MainButton, SecondaryButton } from '@/common/buttons';
import KeyboardAvoidScrollView from '@/common/shims/KeyboardAvoidScrollView';
import AppointmentView from '@/common/elements/AppointmentView';

export type Props = {
  navigation: NavigationStackProp;
  appointment: IAppointment;
  saveClientAppointment: (appointment: IAppointment) => Promise<any>;
  cancelClientAppointment: (appointment: IAppointment) => Promise<any>;
  querying: boolean;
}

export type State = {
  appointment: IAppointment;
}

export class AppointmentDetailsScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: t('#appointments.details'),
    headerRight: Platform.OS === 'ios' && navigation.getParam('canSave') && (
      <SingleHeaderButton label='#common.save' onPress={navigation.getParam('saveOnPress')} />
    )
  });

  constructor(props: Props) {
    super(props);
    this.save = this.save.bind(this);
    this.delete = this.delete.bind(this);

    this.state = {
      appointment: props.appointment
    };

    props.navigation.setParams({
      canSave: this.isUpcoming,
      saveOnPress: this.save
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.querying !== this.props.querying) {
      this.props.navigation.setParams({
        canSave: this.isUpcoming && !this.props.querying
      });
    }

    if (prevProps.appointment !== this.props.appointment) {
      this.setState({
        appointment: this.props.appointment
      });
    }
  }

  get isUpcoming() {
    return moment(this.state.appointment?.datetime).isAfter();
  }

  async save() {
    const result = await this.props.saveClientAppointment({
      ...this.props.appointment,
      ...this.state
    });

    if (result.success) {
      this.props.navigation.goBack();
    }
  }

  async delete(confirm = false) {
    if (!confirm) {
      return this.props.navigation.navigate('Modal', {
        message: '#appointments.confirmCancel',
        icon: 'calendar',
        closeLabel: '#common.cancel',
        confirmLabel: '#common.confirm',
        onConfirmPress: () => this.delete(true)
      });
    }
    
    const result = await this.props.cancelClientAppointment(this.props.appointment);
    if (result.success) {
      this.props.navigation.goBack();
    }
  }

  render() {
    const { appointment } = this.props;
    if (!appointment) {
      return null;
    }

    return (
      <KeyboardAvoidScrollView keyboardShouldPersistTaps='handled'>
        <AppointmentView
          appointment={appointment}
          editable={this.isUpcoming}
          onUpdate={appointment => this.setState({ appointment })} 
        />

        {this.isUpcoming && (
          <>
            {Platform.OS === 'android' && (
              <MainButton enabled={!this.props.querying} label='#common.save' onPress={this.save} />
            )}
            <SecondaryButton label='#appointments.cancel' onPress={this.delete} />
          </>
        )}
      </KeyboardAvoidScrollView>
    );
  }
}

export default connect(
  (state, ownProps) => {
    const appointmentId = +ownProps.navigation.getParam('appointmentId');
    const all = state.appointments.upcoming.concat(state.appointments.past);

    return {
      appointment: all.find(a => a.id === appointmentId),
      querying: state.querying
    };
  },
  {
    saveClientAppointment,
    cancelClientAppointment
  }
)(AppointmentDetailsScreen);

export const styles = StyleSheet.create({
  
});