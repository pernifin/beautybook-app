import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import t from '../../../Lang';
import { color } from '../../../theme';

import AppointmentListScreen from './AppointmentListScreen';
import AppointmentDetailsScreen from './AppointmentDetailsScreen';

export default createStackNavigator({
  AppointmentList: AppointmentListScreen,
  AppointmentDetails: AppointmentDetailsScreen
}, {
  navigationOptions: ({ navigation }) => ({
    tabBarLabel: t('#nav.appointments')
  }),
  defaultNavigationOptions: () => ({
    headerTintColor: Platform.OS === 'ios' ? color.primary : color.white,
    headerTitleStyle: { color: color.black },
  })
});
