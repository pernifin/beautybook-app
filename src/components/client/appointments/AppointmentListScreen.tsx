import React, { Component } from 'react';
import { ScrollView, View, Image, StyleSheet } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { loadPastAppointments } from 'src/actions';

import { IAppointment } from '@/common/DataTypes';
import TabView from '@/common/containers/TabView';
import Span from '@/common/Span';

import AppointmentBlock from './views/AppointmentBlock';

export type Props = {
  navigation: NavigationStackProp;
  upcoming: IAppointment[];
  past: IAppointment[];
  loadPastAppointments: () => Promise<any>;
}

export class AppointmentListScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    headerBackTitle: t('#appointments.appointments'),
    title: t('#appointments.myAppointments')
  });

  componentDidMount() {
    this.props.loadPastAppointments();
  }

  selectAppointment(appointment: IAppointment) {
    this.props.navigation.navigate('AppointmentDetails', { appointmentId: appointment.id });
  }

  render() {
    if (this.props.upcoming.concat(this.props.past).length === 0) {
      return (
        <View style={styles.blankContainer}>
          <Image source={require('assets/images/icons/appointments.png')} style={styles.icon} />
          <Span style={styles.message}>#appointments.emptyList</Span>
        </View>
      );
    }

    return (
      <TabView tabs={['#appointments.upcoming', '#appointments.past']} style={styles.container}>
        <ScrollView style={styles.section}>
          {this.props.upcoming.map(record => (
            <AppointmentBlock
              key={record.id}
              appointment={record}
              onPress={() => this.selectAppointment(record)}
            />
          ))}
        </ScrollView>
        <ScrollView style={styles.section}>
          {this.props.past.map(record => (
            <AppointmentBlock
              key={record.id}
              appointment={record}
              onPress={() => this.selectAppointment(record)}
            />
          ))}
        </ScrollView>
      </TabView>
    );
  }
}

export default connect(
  state => ({
    upcoming: state.appointments.upcoming,
    past: state.appointments.past
  }),
  {
    loadPastAppointments
  }
)(AppointmentListScreen);

export const styles = StyleSheet.create({
  container: {
    paddingTop: 15
  },
  section: {
    paddingHorizontal: 16
  },
  blankContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    width: 100,
    height: 100,
    resizeMode: 'contain'
  },
  message: {
    marginTop: 40,
    width: 220,
    textAlign: 'center',
    fontSize: 20,
    color: theme.color.accentLight
  },
});