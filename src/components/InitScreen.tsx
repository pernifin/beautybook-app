import React, { Component } from 'react';
import { View, Image, Animated, Dimensions, StyleSheet, Easing, Platform } from 'react-native';
import { SvgXml } from 'react-native-svg';

import Span from './common/Span';

const xml = `
  <svg viewBox="0 0 375 461" preserveAspectRatio="none">
    <path d="M0 433C0 433 73 461 187.5 461C302 461 375 433 375 433V0H0V433Z" fill="url(#grad)" />
    <defs>
      <linearGradient id="grad" x1="0" y1="0" x2="100%" y2="0">
        <stop offset="0%" stop-color="#F15459" />
        <stop offset="100%" stop-color="#F17A54" />
      </linearGradient>
    </defs>
  </svg>
`;

const { width, height } = Dimensions.get('screen');

export default class InitScreen extends Component {
  constructor(props) {
    super(props);
  }

  animateValue = new Animated.Value(1);

  componentDidMount() {
    Animated.timing(this.animateValue, {
      toValue: 0,
      duration: 2000,
      delay: 200,
      easing: Easing.back(1.3)
    }).start();
  }

  render() {
    const backgroundHeight = this.animateValue.interpolate({
      inputRange: [0, 1],
      outputRange: [-50, height / 2 + 55]
    });

    const logoPosition = this.animateValue.interpolate({
      inputRange: [0, 1],
      outputRange: [-200, height / 3 - 80],
      extrapolateRight: 'clamp'
    });

    const logoSize = this.animateValue.interpolate({
      inputRange: [0, 1],
      outputRange: [80, 160],
      extrapolateRight: 'clamp'
    });

    const labelPosition = this.animateValue.interpolate({
      inputRange: [0, 0.8],
      outputRange: [-50, height / 3 * 2 + 10],
      extrapolateRight: 'clamp'
    });

    return (
      <View style={styles.container}>
        {/* <Animated.View style={[styles.background, { height: backgroundHeight }]}>
          <SvgXml xml={xml} width="100%" height="100%" />
        </Animated.View> */}

        <Animated.Image
          source={require('../../assets/images/init-logo.png')}
          style={[styles.logo]}
        />

        <Animated.Image
          source={require('../../assets/images/init-label.png')}
          style={[styles.label]}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFF'
  },
  background: {
    position: 'absolute',
    zIndex: 0,
    width: '100%',
    height: height / 2 + 55
  },
  logoContainer: {
    ...StyleSheet.absoluteFillObject,
    bottom: height / 3,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    /* position: 'absolute',
    top: height / 2 - 80,
    alignSelf: 'center', */
    //left: width / 2 - 80,
    top: Platform.OS === 'android' ? 12 : 0,
    width: 160,
    height: 160
  },
  label: {
    position: 'absolute',
    bottom: 40,
    //left: width / 2 - 90,
    alignSelf: 'center',
    width: 180,
    height: 30
  }
})
