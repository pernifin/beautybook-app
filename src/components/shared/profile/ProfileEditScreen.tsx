import React, { Component } from 'react';
import { View, Image, TouchableOpacity, Platform, StyleSheet } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import ActionSheet from 'react-native-action-sheet';
import ImagePicker, { Image as ImageFile } from 'react-native-image-crop-picker';
import Spinner from 'react-native-spinkit';
import { connect } from 'react-redux';
import { last } from 'lodash';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { saveProfile, uploadPhoto } from 'src/actions';
import { formatPhone, getPhotoSource } from 'src/utils';

import { IProfile, IUpload } from '@/common/DataTypes';
import { LibraryAccess } from '@/common/modals/access';
import { MainButton, SingleHeaderButton } from '@/common/buttons';
import KeyboardAvoidScrollView from '@/common/shims/KeyboardAvoidScrollView';
import Form from '@/common/containers/Form';
import { LinkField, TextInputField } from '@/common/formFields';
import Icon from '@/common/Icon';

export type Props = {
  navigation: NavigationStackProp;
  profile: IProfile;
  mode: string;
  saveProfile: (profile: IProfile) => Promise<any>;
  uploadPhoto: (upload: IUpload) => Promise<any>;
  querying: boolean;
}

export type State = {
  profile: IProfile;
  uploading: boolean;
}

export class ProfileEditScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    headerLeft: ({ onPress }) => (
      <SingleHeaderButton
        label={Platform.OS === 'ios' && '#common.cancel'}
        iconName={Platform.OS === 'android' && 'back'}
        onPress={() => navigation.popToTop()}
      />
    ),
    headerBackTitle: t('#common.back'),
    title: t('#profile.editProfile'),
    headerRight: Platform.OS === 'ios' && (
      <SingleHeaderButton
        label='#common.done'
        bold
        enabled={navigation.getParam('saveEnabled', false)}
        onPress={navigation.getParam('saveOnPress')}
      />
    )
  });

  constructor(props: Props) {
    super(props);
    this.saveForm = this.saveForm.bind(this);
    this.updateProfile = this.updateProfile.bind(this);
    this.openPhotoSelect = this.openPhotoSelect.bind(this);

    this.state = {
      profile: props.profile,
      uploading: false
    };

    props.navigation.setParams({
      saveEnabled: this.isFormValid,
      saveOnPress: this.saveForm
    });
  }

  componentDidUpdate(prevProps, prevState: State) {
    if (prevProps.profile !== this.props.profile) {
      this.setState({ profile: this.props.profile });
    }

    if (prevState.profile !== this.state.profile) {
      this.props.navigation.setParams({
        saveEnabled: this.isFormValid,
      });
    }

    if (prevProps.querying !== this.props.querying) {
      this.props.navigation.setParams({
        saveEnabled: this.isFormValid
      });
    }
  }

  get isFormValid() {
    const isValid = this.state.profile.name
      && this.state.profile.phone
      && !this.state.uploading
      && !this.props.querying;

    return Boolean(isValid);
  }

  openPhotoSelect() {
    if (!this.state.profile.photo) {
      return this.openLibrary();
    }

    ActionSheet.showActionSheetWithOptions({
      options: [t('#profile.selectPhoto'), t('#profile.deletePhoto'), t('#common.cancel')].slice(0, Platform.OS === 'android' ? 2 : 3),
      title: t('#profile.changePhoto'),
      cancelButtonIndex: 2,
      destructiveButtonIndex: 1,
      tintColor: theme.color.accent
    }, buttonIndex => {
      if (buttonIndex === 0) {
        this.openLibrary();
      } else if (buttonIndex === 1) {
        this.setState({
          profile: { ...this.state.profile, photo: null }
        });
      }
    });
  }

  async openLibrary() {
    const access = await LibraryAccess(this.props.navigation);
    if (!access) {
      return null;
    }

    let image: ImageFile;
    
    try {
      image = await ImagePicker.openPicker({
        width: 1250,
        height: 1000,
        cropping: true,
        mediaType: 'photo',
        writeTempFile: true,
        smartAlbums: ['UserLibrary', 'SelfPortraits', 'Favorites', 'Bursts', 'DepthEffect'],
        cropperChooseText: t('#common.done'),
        cropperCancelText: t('#common.cancel'),
      }) as ImageFile;
    } catch (error) {
      return null;
    }

    this.setState({ uploading: true });

    const result = await this.props.uploadPhoto({
      uri: image.path, //.replace('file://', ''),
      name: image.filename || last(image.path.split('/')),
      type: image.mime
    });

    if (result.success) {
      this.setState({
        profile: { ...this.state.profile, photo: result.filename },
        uploading: false
      });
    }
  }

  async saveForm() {
    const result = await this.props.saveProfile(this.state.profile);
    if (result.success) {
      this.props.navigation.navigate('ViewProfile');
    }
  }

  updateProfile(values: Partial<IProfile>) {
    this.setState(state => ({
      profile: { ...state.profile, ...values }
    }));
  }

  render() {
    const { profile } = this.state;

    return (
      <KeyboardAvoidScrollView>
        <View style={styles.photoContainer}>
          <TouchableOpacity activeOpacity={0.7} onPress={this.openPhotoSelect} style={styles.photoCover}>
            {this.state.uploading
              ? <Spinner type='Circle' size={60} color='#fff' />
              : <Icon name='photo' size={60} color='#fff' />
            }
          </TouchableOpacity>
          
          <Image
            source={getPhotoSource(profile)}
            resizeMode='cover'
            style={styles.photo}
          />
        </View>

        <Form>
          <TextInputField
            label='#label.name'
            value={profile.name}
            onValueChange={name => this.updateProfile({ name })}
          />
          <LinkField
            label='#label.phone'
            value={formatPhone(profile.phone)}
            onPress={() => this.props.navigation.navigate('EditPhone')}
          />
          {this.props.mode === 'master' && [
            <TextInputField
              key='specialization'
              label='#label.specialization'
              value={profile.specialization}
              multiline
              onValueChange={specialization => this.updateProfile({ specialization })}
            />,
            <TextInputField
              key='address'
              label='#label.address'
              value={profile.address}
              multiline
              onValueChange={address => this.updateProfile({ address })}
            />,
            <TextInputField
              key='instagram'
              label='#label.instagram'
              value={profile.instagram}
              onValueChange={instagram => this.updateProfile({ instagram })}
              prepend='@'
            />
          ]}
        </Form>

        {Platform.OS === 'android' && (
          <MainButton enabled={this.isFormValid} label='#common.save' onPress={this.saveForm} />
        )}
      </KeyboardAvoidScrollView>
    );
  }
}

export default connect(
  state => ({
    profile: state.profile,
    mode: state.system.mode,
    querying: state.querying
  }),
  {
    saveProfile,
    uploadPhoto
  }
)(ProfileEditScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  photoContainer: {
    width: '100%',
    aspectRatio: 1.25
  },
  photoCover: {
    position: 'absolute',
    zIndex: 2,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00000060'
  },
  photo: {
    position: 'absolute',
    zIndex: 1,
    width: '100%',
    height: '100%'
  }
});
