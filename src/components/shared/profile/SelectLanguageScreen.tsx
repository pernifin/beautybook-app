import React, { Component } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';

import t, { Lang } from 'src/Lang';
import * as theme from 'src/theme';
import { selectLanguage } from 'src/actions';

import Form from '@/common/containers/Form';
import { SelectableField } from '@/common/formFields';

export type Props = {
  navigation: NavigationStackProp;
  selectLanguage: (lang: string) => Promise<any>;
}

export class SelectLanguageScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: t('#profile.selectLanguage')
  });

  selectLang(lang: string) {
    Lang.update(lang);
    this.props.selectLanguage(lang);
    this.props.navigation.pop();
  }

  render() {
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <Form>
          {Lang.availableLangs.map(lang => (
            <SelectableField
              key={lang}
              label={`#langs.${lang}`}
              selected={Lang.activeLang === lang}
              onPress={() => this.selectLang(lang)}
            />
          ))}
        </Form>
      </ScrollView>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    paddingTop: 20
  }
});

export default connect(
  null,
  {
    selectLanguage
  }
)(SelectLanguageScreen);