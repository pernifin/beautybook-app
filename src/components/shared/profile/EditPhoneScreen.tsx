import React, { Component } from 'react';
import { ScrollView, Platform, StyleSheet } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';

import t from 'src/Lang';
import * as theme from 'src/theme';
import { savePhone, verifyPhone, repeatCall } from 'src/actions';
import { checkPhone } from 'src/utils';

import { SingleHeaderButton, MainButton, SecondaryButton } from '@/common/buttons';
import Form from '@/common/containers/Form';
import { PhoneField } from '@/common/formFields';
import CodeInput from '@/common/elements/CodeInput';

export type Props = {
  navigation: NavigationStackProp;
  phone: string;
  savePhone: (phone: string) => Promise<any>;
  verifyPhone: (phone: string, code: string) => Promise<any>;
  repeatCall: () => Promise<any>;
}

export type State = {
  phone: string;
  submitted: boolean;
  accepted: boolean;
  invalid: boolean;
}

export class EditPhoneScreen extends Component<Props, State> {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: t('#profile.phoneNumber'),
    headerRight: Platform.OS === 'ios' && (
      <SingleHeaderButton
        label='#common.next'
        enabled={navigation.getParam('saveEnabled', false)}
        onPress={navigation.getParam('saveOnPress')}
      />
    )
  });

  constructor(props: Props) {
    super(props);
    this.submitPhone = this.submitPhone.bind(this);
    this.onCodeInput = this.onCodeInput.bind(this);
    
    this.state = {
      phone: props.phone,
      submitted: false,
      accepted: false,
      invalid: false
    };

    props.navigation.setParams({
      saveEnabled: this.isFormValid,
      saveOnPress: this.submitPhone
    });
  }

  get isFormValid() {
    const { phone, submitted } = this.state;

    return !submitted && checkPhone(phone) && phone !== this.props.phone;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.phone !== this.state.phone || this.state !== prevState) {
      this.props.navigation.setParams({
        saveEnabled: this.isFormValid
      });
    }
  }

  async submitPhone() {
    this.setState({ submitted: true });

    const result = await this.props.savePhone(this.state.phone);
    if (result.success) {
      this.setState({ accepted: true });
      this.props.navigation.setParams({ saveOnPress: null });
    } else {
      this.setState({ submitted: false });
    }
  }

  async onCodeInput(code) {
    this.setState({ invalid: false });

    const result = await this.props.verifyPhone(this.state.phone, code);
    if (result.success) {
      this.props.navigation.goBack();
    } else {
      this.setState({ invalid: true });
    }
  }

  render() {
    const { phone, submitted } = this.state;

    return (
      <ScrollView keyboardShouldPersistTaps='handled' style={styles.container}>
        <Form footer='#auth.smsSendHint' style={styles.form}>
          <PhoneField
            value={phone}
            onValueChange={phone => !this.state.submitted && this.setState({ phone })}
            fontSize={20}
          />
        </Form>

        {Platform.OS === 'android' && !submitted && (
          <MainButton enabled={this.isFormValid} label='#common.next' onPress={this.submitPhone} />
        )}

        {this.state.accepted && (
          <CodeInput invalid={this.state.invalid} onCodeInput={this.onCodeInput} onRepeatCall={this.props.repeatCall} />
        )}
      </ScrollView>
    );
  }
}

export default connect(
  state => ({
    phone: state.profile.phone
  }),
  {
    savePhone,
    verifyPhone,
    repeatCall
  }
)(EditPhoneScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30
  },
  form: {
    marginBottom: 20
  },
  hint: {
    marginTop: 5,
    marginHorizontal: 15,
    fontSize: 11
  },
  codeField: {
    width: '50%',
    alignSelf: 'center',
    marginTop: 20,
    paddingLeft: 10,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#ddd',
    fontSize: 48,
    letterSpacing: 10
  },
});