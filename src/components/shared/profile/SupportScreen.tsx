import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import t, { Lang } from 'src/Lang';
import * as theme from 'src/theme';

import Span from '@/common/Span';

export class SupportScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    ...theme.header,
    title: t('#profile.support')
  });

  render() {
    return (
      <View>
        <Span> SupportScreen </Span>
      </View>
    )
  }
}

export default connect(
  null,
  {}
)(SupportScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});