import React from 'react';
import { View, StyleSheet } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';
import { Apptentive } from 'apptentive-react-native';

import t from 'src/Lang';
import { switchMode, logoutUser, setError } from 'src/actions';

import { IProfile } from '@/common/DataTypes';
import { SecondaryButton } from '@/common/buttons';
import Form from '@/common/containers/Form';
import { LinkField } from '@/common/formFields';
import ProfileView from '@/common/elements/ProfileView';

export type Props = {
  navigation: NavigationStackProp;
  profile: IProfile;
  userId: string;
  mode: string;
  switchMode: () => Promise<any>;
  logoutUser: () => Promise<any>;
  setError: (message: string) => Promise<any>;
}

export class ProfileViewScreen extends React.Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    title: t('#profile.view'),
    header: null
  });

  constructor(props) {
    super(props);
    this.openSupport = this.openSupport.bind(this);
    this.switchMode = this.switchMode.bind(this);
    this.logout = this.logout.bind(this);
  }

  async openSupport() {
    Apptentive.setPersonName(this.props.profile.name);
    Apptentive.addCustomPersonData('userId', this.props.userId);

    if (await Apptentive.canShowMessageCenter() === true) {
      await Apptentive.presentMessageCenter();
    } else {
      this.props.setError('#error.openSupportCenter');
    }
  }

  switchMode() {
    this.props.switchMode();
  }

  async logout() {
    await this.props.logoutUser();
  }

  render() {
    const { profile, mode, navigation } = this.props;
    
    return (
      <ProfileView profile={profile} full={mode === 'master'}>
        <View>
          <Form style={styles.navigation}>
            {/* <LinkField label='Settings' onPress={() => null} /> */}
            <LinkField label='#profile.editProfile' onPress={() => navigation.navigate('EditProfile')} />
            <LinkField label='#profile.language' onPress={() => navigation.navigate('SelectLanguage')} />
            <LinkField label='#profile.support' onPress={this.openSupport} />
          </Form>

          {profile.name && profile.phone ? (
            <SecondaryButton
              label={profile.mode === 'master' ? '#profile.switchClient' : '#profile.switchMaster'}
              onPress={this.switchMode}
            />
          ) : null}
          
          <SecondaryButton label='#profile.logout' onPress={this.logout} />
        </View>
      </ProfileView>
    );
  }
}

export default connect(
  state => ({
    profile: state.profile,
    userId: state.system.userId,
    mode: state.system.mode,
  }),
  {
    switchMode,
    logoutUser,
    setError
  }
)(ProfileViewScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#efefef'
  },
  navigation: {
    marginBottom: 25
  }
});