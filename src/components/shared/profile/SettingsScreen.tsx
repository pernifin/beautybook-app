import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import Span from '@/common/Span';

export class SettingsScreen extends Component {
  render() {
    return (
      <View>
        <Span> SettingsScreen </Span>
      </View>
    )
  }
}

export default connect(
  null,
  {}
)(SettingsScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});