import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import t from '../../../Lang';
import { color } from '../../../theme';

import ProfileViewScreen from './ProfileViewScreen';
import ProfileEditScreen from './ProfileEditScreen';
import EditPhoneScreen from './EditPhoneScreen';
import SettingsScreen from './SettingsScreen';
import SelectLanguageScreen from './SelectLanguageScreen';
import SupportScreen from './SupportScreen';

export default createStackNavigator({
  ViewProfile: createStackNavigator({
    ViewProfile: ProfileViewScreen,
    Settings: SettingsScreen,
    SelectLanguage: SelectLanguageScreen,
    Support: SupportScreen
  }, {
    navigationOptions: {
      header: null
    },
    defaultNavigationOptions: () => ({
      headerTintColor: Platform.OS === 'ios' ? color.primary : color.white,
      headerTitleStyle: { color: color.black },
    })
  }),
  EditProfile: createStackNavigator({
    EditProfile: ProfileEditScreen,
    EditPhone: EditPhoneScreen
  }, {
    navigationOptions: {
      header: null
    },
    defaultNavigationOptions: () => ({
      headerTintColor: Platform.OS === 'ios' ? color.primary : color.white,
      headerTitleStyle: { color: color.black },
    })
  })
}, {
  mode: 'modal',
  navigationOptions: ({ navigation }) => ({
    tabBarVisible: navigation.state.index === 0,
    tabBarLabel: t('#nav.profile')
  })
});
