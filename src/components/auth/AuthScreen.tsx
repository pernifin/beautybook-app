import React, { Component } from 'react';
import { ScrollView, SafeAreaView, Image, StyleSheet, Keyboard, Dimensions, Platform } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { connect } from 'react-redux';

import { color } from 'src/theme';
import { signUserIn, verifyUserPhone, repeatUserCall, createUser, skipAuth } from 'src/actions';

import TabView from '@/common/containers/TabView';

import PhoneInput from './views/PhoneInput';
import ModeSelect from './views/ModeSelect';

export type Props = {
  navigation: NavigationStackProp;
  phoneSent: boolean;
  signUserIn: (phone: string) => Promise<any>;
  verifyUserPhone: (phone: string, code: string) => Promise<any>;
  repeatUserCall: (phone: string) => Promise<any>;
  skipAuth: () => Promise<any>;
  createUser: (name: string, mode: string) => Promise<any>;
}

export type State = {
  phone: string;
  screenIndex: number;
}

export class AuthScreen extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.onPhoneSubmit = this.onPhoneSubmit.bind(this);
    this.onCodeInput = this.onCodeInput.bind(this);
    this.onSignUp = this.onSignUp.bind(this);

    this.state = {
      phone: '',
      screenIndex: 0
    };
  }

  async onPhoneSubmit(phone: string) {
    this.setState({ phone });

    const result = await this.props.signUserIn(phone);

    return result.success; //Boolean(this.props.phoneSent);
  }

  async onCodeInput(code) {
    const result = await this.props.verifyUserPhone(this.state.phone, code);
    if (result.success && !result.mode) {
      Keyboard.dismiss();
      this.setState({ screenIndex: 1 });
    }

    return result.success;
  }

  async onSignUp(name: string, mode: string) {
    await this.props.createUser(name, mode);
  }

  render() {
    return (
      <ScrollView bounces={false} keyboardShouldPersistTaps='handled' contentContainerStyle={styles.container}>
        <SafeAreaView style={styles.logo}>
          <Image source={require('assets/images/init-label.png')} style={styles.logoImage} />
          {/* <Span style={styles.logoText}>BeautyBook</Span> */}
        </SafeAreaView>

        <TabView tabs={['phone', 'mode']} hideTabs enabled={false} tabIndex={this.state.screenIndex}>
          <PhoneInput
            onPhoneSubmit={this.onPhoneSubmit}
            onCodeInput={this.onCodeInput}
            onRepeatCall={() => this.props.repeatUserCall(this.state.phone)}
            onSkipAuth={this.props.skipAuth}
          />

          <ModeSelect onSignUp={this.onSignUp} />
        </TabView>
      </ScrollView>
    );
  }
}

export default connect(
  state => ({
    phoneSent: !!state.system.cid
  }),
  {
    signUserIn,
    verifyUserPhone,
    repeatUserCall,
    skipAuth,
    createUser
  }
)(AuthScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //height: Dimensions.get('window').height - (Platform.OS === 'android' ? 24 : 0),
    backgroundColor: color.white
  },
  logo: {
    flex: 0.35,
    justifyContent: 'center',
    alignItems: 'center'
  },
  logoText: {
    fontSize: 30,
    fontWeight: '600',
    color: color.primary
  },
  logoImage: {
    width: 180,
    height: 30,
    resizeMode: 'contain'
  }
});