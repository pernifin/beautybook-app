import React, { Component } from 'react';
import { View, TextInput, TouchableOpacity, StyleSheet, Keyboard } from 'react-native';
import { capitalize } from 'lodash';

import { color } from 'src/theme';

import { MainButton } from '@/common/buttons';
import Span from '@/common/Span';
import Touchable from '@/common/shims/Touchable';

export type Props = {
  onSignUp: (name: string, mode: string) => Promise<any>;
}

export type State = {
  name: string;
  mode: string;
}

export default class ModeSelect extends Component<Props, State> {
  readonly state = {
    name: '',
    mode: ''
  };

  selectMode(mode: string) {
    Keyboard.dismiss();
    this.setState({ mode });
  }

  render() {
    return (
      <View style={styles.container}>
        <Span style={styles.header}>#auth.userName</Span>

        <TextInput
          value={this.state.name}
          onChangeText={name => this.setState({ name })}
          style={styles.nameInput}
        />

        {['master', 'client'].map(mode => (
          <Touchable
            key={mode}
            onPress={() => this.selectMode(mode)}
            style={[styles.mode, this.state.mode === mode && styles.modeActive]}
          >
            <Span style={[styles.modeText, this.state.mode === mode && styles.modeActiveText]}>
              {`#auth.select${capitalize(mode)}`}
            </Span>
          </Touchable>
        ))}

        <MainButton
          enabled={Boolean(this.state.name && this.state.mode)}
          label='#auth.register'
          onPress={() => this.props.onSignUp(this.state.name, this.state.mode)}
          style={styles.signUp}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    textAlign: 'center',
    fontSize: 20
  },
  nameInput: {
    margin: 40,
    marginTop: 20,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: color.accentAlpha(0.2),
    fontSize: 20,
    fontWeight: '600',
    textAlign: 'center'
  },
  mode: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 10,
    width: 200,
    height: 60,
    borderRadius: 10,
    backgroundColor: color.accentAlpha(0.1)
  },
  modeActive: {
    borderWidth: 1,
    borderColor: color.primary,
    backgroundColor: color.white
  },
  modeText: {
    fontSize: 16,
    fontWeight: '600',
    color: color.accentAlpha(0.6)
  },
  modeActiveText: {
    color: color.primary
  },
  signUp: {
    marginTop: 30
  }
});