import React from 'react';
import { View, TouchableOpacity, SafeAreaView, StyleSheet } from 'react-native';

import { color } from 'src/theme';

import KeyboardAvoidContainer from '@/common/shims/KeyboardAvoidContainer';
import Span from '@/common/Span';

export type Props = {
  onPress(): void;
}

export default function SkipAuth(props: Props) {
  return (
    <KeyboardAvoidContainer>
      <SafeAreaView style={styles.container}>
        <TouchableOpacity activeOpacity={0.5} onPress={props.onPress}>
          <View style={styles.touchable}>
            <Span>
              <Span style={styles.skipText}>#auth.iAmClient</Span>
              <Span style={styles.skipLink}>#auth.skipAuth</Span>
            </Span>
          </View>
        </TouchableOpacity>
      </SafeAreaView>
    </KeyboardAvoidContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    alignItems: 'center',
    borderTopWidth: StyleSheet.hairlineWidth,
    borderColor: color.accentLight
  },
  touchable: {
    justifyContent: 'center',
    height: 60
  },
  skipText: {
    fontSize: 15,
    fontWeight: '500',
    color: color.accentLight
  },
  skipLink: {
    fontWeight: '600',
    color: color.black
  }
});