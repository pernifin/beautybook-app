import React, { Component } from 'react';
import { View, StyleSheet, Platform } from 'react-native';

import { color } from 'src/theme';
import { checkPhone } from 'src/utils';

import { MainButton } from '@/common/buttons';
import { PhoneField } from '@/common/formFields';
import Span from '@/common/Span';
import CodeInput from '@/common/elements/CodeInput';

import SkipAuth from './SkipAuth';

export type Props = {
  onPhoneSubmit: (phone: string) => Promise<boolean>;
  onCodeInput: (code: string) => Promise<boolean>;
  onRepeatCall: () => void;
  onSkipAuth: () => void;
}

export type State = {
  phone: string;
  submitted: boolean;
  accepted: boolean;
  invalid: boolean;
}

export default class PhoneInput extends Component<Props, State> {
  readonly state = {
    phone: '',
    submitted: false,
    accepted: false,
    invalid: false
  };

  async onPhoneSubmit() {
    this.setState({ submitted: true });

    const accepted = await this.props.onPhoneSubmit(this.state.phone);
    this.setState({ accepted, submitted: accepted });
  }

  async onCodeInput(code) {
    this.setState({ invalid: false });

    const result = await this.props.onCodeInput(code);
    if (!result) {
      this.setState({ invalid: true });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Span style={styles.header}>#auth.phoneNumber</Span>
        <PhoneField
          value={this.state.phone}
          onValueChange={phone => this.setState({ phone })}
          touchable={false}
          fontSize={20}
          style={styles.phone}
          inputStyle={styles.phoneInput}
        />
        {/* {!this.state.submitted && <Span style={styles.phoneHint}>#auth.phoneHint</Span>} */}
        {!this.state.submitted && <Span style={styles.phoneHint}>#auth.smsSendHint</Span>}

        {!this.state.submitted && (
          <MainButton
            enabled={checkPhone(this.state.phone)}
            label='#common.next'
            onPress={() => this.onPhoneSubmit()}
          />
        )}

        {this.state.accepted && (
          <CodeInput
            invalid={this.state.invalid}
            onCodeInput={code => this.onCodeInput(code)}
            onRepeatCall={this.props.onRepeatCall}
          />
        )}

        <SkipAuth onPress={this.props.onSkipAuth} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    textAlign: 'center',
    fontSize: 20
  },
  phone: {
    alignSelf: 'center',
    width: Platform.OS === 'ios' ? 265 : 245
  },
  phoneInput: {
    fontWeight: '600'
  },
  phoneHint: {
    marginBottom: 20,
    marginHorizontal: 30,
    textAlign: 'center',
    lineHeight: 24,
    color: color.accentLight
  }
});