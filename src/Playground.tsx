import React, { Component } from 'react';
import { View, Text, ImageBackground, Platform, StyleSheet, Dimensions, Button } from 'react-native';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import SplashScreen from 'react-native-splash-screen';

import * as theme from 'src/theme';

import Gradient from '@/common/containers/Gradient';
import { FAB } from '@/common/buttons';

type State = {

}

export default class Playground extends Component<{}, State> {
  
  constructor(props) {
    super(props);

    SplashScreen.hide();

    this.state = {

    };
  }

  render() {
    
    const icon = (
      // <ImageBackground source={require('assets/images/fab-bg.png')} resizeMode='contain' style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
      //   <Icon name='ios-add' size={30} color='#fff' />
      // </ImageBackground>
      <Gradient colors={theme.color.primaryRange} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
        <Icon name='ios-add' size={30} color='#fff' />
      </Gradient>
    );

    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Basic Example
        </Text>
        {/* <ActionButton fixNativeFeedbackRadius nativeFeedbackRippleColor={theme.color.accentAlpha(0.2)} renderIcon={active => icon} degrees={135} offsetX={16} offsetY={16}>
          <ActionButton.Item size={40} buttonColor={theme.color.accent} title="Time block" onPress={() => null}>
            <Icon name="md-create" style={styles.actionButtonIcon} />
          </ActionButton.Item>
          <ActionButton.Item size={40} buttonColor={theme.color.accent} title="Appointment" onPress={() => null}>
            <Icon name="md-notifications-off" style={styles.actionButtonIcon} />
          </ActionButton.Item>
        </ActionButton> */}

        <FAB iconName='add' onPress={() => null}>
          <FAB.Item iconName='lock' label='Add time break' onPress={() => null} />
          <FAB.Item iconName='edit-clean' label='Make an appointment' onPress={() => null} />
        </FAB>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  }
});