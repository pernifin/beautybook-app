import Reactotron from 'reactotron-react-native';
import { NativeModules } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { reactotronRedux } from 'reactotron-redux';

export default function connectReactotron() {
  const host = NativeModules.SourceCode.scriptURL.split('://')[1].split(':')[0];

  Reactotron
    .setAsyncStorageHandler(AsyncStorage)
    .configure({ host: '192.168.31.141' })
    .useReactNative()
    .use(reactotronRedux())
    .connect();
}
